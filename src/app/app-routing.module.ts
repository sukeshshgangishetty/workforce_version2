import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseLayoutComponent } from './Layout/base-layout/base-layout.component';
import { PagesLayoutComponent } from './Layout/pages-layout/pages-layout.component';

// import { ForgotPasswordComponent } from './DemoPages/UserPages/forgot-password/forgot-password.component';
// import { ForgotPasswordBoxedComponent } from './DemoPages/UserPages/forgot-password-boxed/forgot-password-boxed.component';
// import { LoginBoxedComponent } from './DemoPages/UserPages/login-boxed/login-boxed.component';
import { LoginComponent } from './modules/login/login.component';
// import { RegisterBoxedComponent } from './DemoPages/UserPages/register-boxed/register-boxed.component';
import { RegisterComponent } from './modules/register/register.component';
import { ActivationComponent } from './modules/activation/activation/activation.component';
import { AuthGuard } from './auth.guard';
import { ForgetPasswordComponent } from './modules/forget-password/forget-password.component';

const routes: Routes = [
  {
    path: "",
    component: BaseLayoutComponent,
    children: [
      {
        path: "dashboard",
        loadChildren: () =>
          import("./modules/dashboard/dashboard.module").then(
            (m) => m.DashboardModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "product",
        loadChildren: () =>
          import("./modules/product/product.module").then(
            (m) => m.ProductModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "services",
        loadChildren: () =>
          import("./modules/services/services.module").then(
            (m) => m.ServicesModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "marketing",
        loadChildren: () =>
          import("./modules/marketing/marketing.module").then(
            (m) => m.MarketingModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "stock",
        loadChildren: () =>
          import("./modules/stock/stock.module").then(
            (m) => m.StockModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "warehouse",
        loadChildren: () =>
          import("./modules/warehouse/warehouse.module").then(
            (m) => m.WarehouseModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "payments",
        loadChildren: () =>
          import("./modules/payments/payments.module").then(
            (m) => m.PaymentsModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "accountings",
        loadChildren: () =>
          import("./modules/accountings/accountings.module").then(
            (m) => m.AccountingsModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "employees",
        loadChildren: () =>
          import("./modules/employees/employees.module").then(
            (m) => m.EmployeesModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "communication",
        loadChildren: () =>
          import("./modules/communication/communication.module").then(
            (m) => m.CommunicationModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "merge",
        loadChildren: () =>
          import("./modules/merge/merge.module").then(
            (m) => m.MergeModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "manageusers",
        loadChildren: () =>
          import("./modules/manageusers/manageusers.module").then(
            (m) => m.ManageusersModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "goodsassets",
        loadChildren: () =>
          import("./modules/goodsassets/goodsassets.module").then(
            (m) => m.GoodsassetsModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "hra",
        loadChildren: () =>
          import("./modules/hra/hra.module").then(
            (m) => m.HraModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "settings",
        loadChildren: () =>
          import("./modules/settings/settings.module").then(
            (m) => m.SettingsModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "discount",
        loadChildren: () =>
          import("./modules/discount/discount.module").then(
            (m) => m.DiscountModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "discounts",
        loadChildren: () =>
          import("./modules/offers/offers.module").then(
            (m) => m.OffersModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "vendors",
        loadChildren: () =>
          import("./modules/vendors/vendors.module").then(
            (m) => m.VendorsModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "client",
        loadChildren: () =>
          import("./modules/client/client.module").then((m) => m.ClientModule),
        canActivate: [AuthGuard],
      },
      {
        path: "executive",
        loadChildren: () =>
          import("./modules/executive/executive.module").then(
            (m) => m.ExecutiveModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "report",
        loadChildren: () =>
          import("./modules/report/report.module").then((m) => m.ReportModule),
        canActivate: [AuthGuard],
      },
      {
        path: "task",
        loadChildren: () =>
          import("./modules/task/task.module").then((m) => m.TaskModule),
        canActivate: [AuthGuard],
      },
      {
        path: "track",
        loadChildren: () =>
          import("./modules/track/track.module").then((m) => m.TrackModule),
        canActivate: [AuthGuard],
      },
      {
        path: "trips",
        loadChildren: () =>
          import("./modules/trips/trips.module").then((m) => m.TripsModule),
        canActivate: [AuthGuard],
      },
      {
        path: "device",
        loadChildren: () =>
          import("./modules/device/device.module").then((m) => m.DeviceModule),
        canActivate: [AuthGuard],
      },

      {
        path: "pricing",
        loadChildren: () =>
          import("./modules/pricing/pricing.module").then(
            (m) => m.PricingModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "sales",
        loadChildren: () =>
          import("./modules/sales/sales.module").then((m) => m.SalesModule),
        canActivate: [AuthGuard],
      },

      {
        path: "order",
        loadChildren: () =>
          import("./modules/orders/orders.module").then((m) => m.OrdersModule),
        canActivate: [AuthGuard],
      },

      {
        path: "orderList/:id",
        loadChildren: () =>
          import("./modules/orders/orders-list/orders-list.module").then(
            (m) => m.OrdersListModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "dispatch",
        loadChildren: () =>
          import("./modules/dispatch-list/dispatch-list.module").then(
            (m) => m.DispatchListModule
          ),
      },
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full",
      },
    ],
  },
  {
    path: "",
    component: PagesLayoutComponent,
    children: [
      // User Pages
      {
        path: "login",
        component: LoginComponent,
        data: { extraParameter: "" },
      },
      {
        path: "register",
        component: RegisterComponent,
        data: { extraParameter: "" },
      },
      {
        path: "activation/:key",
        component: ActivationComponent,
        data: { extraParameter: "" },
      },
      {
        path: "forget",
        component: ForgetPasswordComponent,
        data: { extraParameter: "" },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
