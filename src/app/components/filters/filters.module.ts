import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicComponent } from './basic/basic.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import{ CommonComponent } from './common/common.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


@NgModule({
  declarations: [BasicComponent, CommonComponent],
  imports: [CommonModule, NgbModule, AngularFontAwesomeModule],
  exports: [CommonComponent],
})
export class FiltersModule {}
