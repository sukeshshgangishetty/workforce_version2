import { Component, OnInit,Directive, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'file-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.sass']
})

export class ViewerComponent implements OnInit {

  constructor(public viewContainerRef: ViewContainerRef) {

   }

  ngOnInit() {
  }

}
