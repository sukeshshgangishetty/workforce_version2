import { Injectable } from '@angular/core';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root'
})
export class DateConverter {
 
    constructor() { }

  format(date: Date ,format = 'DD-MM-YYYY'){
    let data = moment(date);
    // var dd = String(date.getDate()).padStart(2, '0');
    // var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy = date.getFullYear();
    
    return data.format(format);
  }
  parse(date: string,format = "DD-MM-YYYY"){
     let data = moment(date,format);
     return data.toDate();
  }
  io(date: string,informat = "DD-MM-YYYY",toformat="DD MMM, YYYY"){
    let data = moment(date,informat);
    return data.format(toformat);
  }
  diff(date2: string,date1: string,format1 = "DD-MM-YYYY",format2 = "DD-MM-YYYY"){
      let d1 = moment(date1,format1);
      let d2 = moment(date2,format2);
      return d1.diff(d2, 'days')+1;
  }
}