import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JsonPipe } from '@angular/common';
import {Router} from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private _http: HttpClient, private route: Router) { }

  getToken(){
    return localStorage.getItem('token');
  }
  isLoggedIn() : boolean{
    return localStorage.getItem('token') !== null;
  }
  logout(){
    localStorage.clear();
    this.route.navigate(['login']);

  }
  login(data){
    return this._http.post<any>("/workforce/api/v1/login",data);
  }

  getUser(){
    return JSON.parse(localStorage.getItem('user'));
  }


}
