import { Directive, ElementRef, EventEmitter, HostListener, Input, Output,AfterViewInit} from '@angular/core';
import { NgModel } from '@angular/forms';
import * as moment from 'moment';
@Directive({
  selector: '[ngModel][appDate]',
  providers: [NgModel],
  host: {
    '(ngModelChange)': 'onInputChange($event)'
  }
})
export class DateDirective implements AfterViewInit{
  @Input() appDate: any;
  @Input() appDateFormat: string = 'YYYY-MM-DD';
  @Output() appDateChange: EventEmitter<number> = new EventEmitter();
  constructor(private el: ElementRef,private model:NgModel) {}
  ngAfterViewInit(){
    setTimeout(() => {
     this.model.valueAccessor.writeValue(moment(this.appDate).format('YYYY-MM-DD'));
    },500);
   }

  onInputChange(value:any){
    this.model.valueAccessor.writeValue(value);
    var res = moment(value, this.appDateFormat).toDate().getTime();
    this.appDateChange.emit(res);
  }
  
}
