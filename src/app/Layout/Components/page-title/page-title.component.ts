import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
})
export class PageTitleComponent {
  @Output() public callback: EventEmitter<any> = new EventEmitter();

  @Input() heading :string = '';
  @Input() subheading :string = '';
  @Input() icon :string = '';
  @Input() btntext: string = 'Create New';
  @Input() btnstatus: boolean = true;
  @Input() btnLink: string = '';
  @Input() btnIcon: string = 'plus';
  @Input() btntype: string = 'link';
  @Input() params: any;
  
  call(){
    this.callback.emit(this.params);
  }
}
