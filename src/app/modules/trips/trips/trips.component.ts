import { Component, OnInit } from '@angular/core';
import { TripService } from '../trip.service'; 
@Component({
  selector: "app-trips",
  templateUrl: "./trips.component.html",
  styleUrls: ["./trips.component.sass"],
})
export class TripsComponent implements OnInit {
  heading = "Trips";
  subheading = "Welcome to Trips Section";
  icon = "fa fa-line-chart";
  btntext = "Add Trips";
  btnLink = "trips/addtrip";
  trips: any=[];
  track:any;

  constructor(private _trip: TripService) {}

  ngOnInit() {
    this._trip.getTrips().subscribe((res:any) => {
      this.trips = res.body.tripResponses;
      console.log("Trips", this.trips);
    });

    // track
    // this._trip.getTrack().subscribe((res) => {
    //   this.track = res;
    //   console.log("Track Data:", this.track);
    // });
  }
}
