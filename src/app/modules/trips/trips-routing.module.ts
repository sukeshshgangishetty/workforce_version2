import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddtripComponent } from './addtrip/addtrip.component';
import { GrouptripComponent } from './grouptrip/grouptrip.component';
import { RoundsComponent } from './rounds/rounds.component';
import { TripComponent } from './trip/trip.component';
import { TripsComponent } from './trips/trips.component';


const routes: Routes = [
  {
    path: "",

    children: [
      {
        path: "trips",
        component: TripsComponent,
      },
      {
        path: "trip",
        component: TripComponent,
      },
      {
        path: "rounds/:id",
        component: RoundsComponent,
      },
      {
        path: "addtrip/:id",
        component: AddtripComponent,
      }, {
        path: "addtrip",
        component: AddtripComponent,
      },
      {
        path: 'grouptrip',
        component: GrouptripComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TripsRoutingModule { }
