import { CdkDragDrop, CdkDragRelease, moveItemInArray } from "@angular/cdk/drag-drop";
import {
  Component,
  OnInit,
  AfterViewInit,
  EventEmitter,
  Input,
  Output,
  Renderer2,
} from "@angular/core";
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import * as L from "leaflet";
import "mapbox-gl-leaflet";
import * as moment from "moment";
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { ClientService } from "../../client/client.service";
import { TripService } from "../trip.service";
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ScheduleComponent } from "../schedule/schedule.component";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-addtrip",
  templateUrl: "./addtrip.component.html",
  styleUrls: ["./addtrip.component.sass"],
})
export class AddtripComponent implements OnInit, AfterViewInit {
  heading = "Trips";
  subheading = "Welcome to Trips Section";
  icon = "fa fa-line-chart";
  btntext = "Back to Trips";
  btnLink = "trips/trips";
  btnIcon = "angle-left";
  queryParams : any = {};
  private map: L.Map;
  constructor(public dialog: MatDialog, private _trip: TripService, public renderer2: Renderer2, private _client: ClientService, private snackBar: MatSnackBar, private _route: ActivatedRoute) {
    this.req = this._route.snapshot.params;
    this.queryParams = this._route.snapshot.queryParams
  }
  
  pointIndex = 0;
  trip: any = {
    id: 0,
    description: "",
    period: "DAILY",
    tripPoints: [
      {
        "expTym": "01:00",
        "purpose": "",
        "sequence": 0,
        "point":{
          "clientId": 0,
          "locCurrent": {
            "address": "",
            "lat": null,
           "lng": null
          },
          "name": "",
          "type": "VISIT"
        }
      },
      {
        "expTym": "01:00",
        "purpose": "",
        "sequence": 0,
        "point": {
          "clientId": 0,
          "locCurrent": {
            "address": "",
            "lat": null,
            "lng": null
          },
          "name": "",
          "type": "VISIT"
        }
      }
    ],
    priorityType: "LOW",
    title: ""
  };
  tripCopy = JSON.parse(JSON.stringify(this.trip));

  point = {
    "expTym": "01:00",
    "purpose": "",
    "sequence": 0,
    "point":{
    "clientId": 0,
      "locCurrent": {
        "address": "",
        "lat": 0,
        "lng": 0
      },
      "name": "",
      "type": "VISIT"
    }
  };
  pointName = new FormControl();
  searchpoints:any = [{"name":"none"}];
  filteredPoints: any;

  priority = [{ name: "HIGH" }, { name: "LOW" }, { name: "MEDIUM" }];
  periods = [{ name: "DAILY" }, { name: "WEEKLY" }, { name: "MONTHLY" }, { name: "YEARLY" }];
  visittypes = [{ name: "VISIT" }, { name: "CLIENT" }];
  clients: any;
  req: any;
  marker : any;
  markers : any = [];
  columns: any[];
  paginateData: any[];
  pos: any;
  release: boolean = true;
  polylines: any = [];
  polyline : any;
  updateType : string = 'remove';
  route = [];
  scheduleData: any = {};
  ngOnInit() {
    
    this._client.get().subscribe(res => {
      this.clients = res;
    });
    
    this._trip.points().subscribe(res => {
      this.searchpoints = res;
      // this._filterName();
    });
    if (this.req.id) {
      this._trip.getschduleAlloc(this.req.id).subscribe(res => {
        this.trip = res;
        if(this.queryParams.type == 'duplicate'){
          this.trip.id = 0;
        }
        this.trip.tripPoints.forEach((element:any,index:number) => {
          this.addMarker(index);
          this.addPolyline();
        });
      });
    }
    
  }
  ngAfterViewInit() {
    
    this.map = L.map("map").setView([17.39062, 78.55883], 11);
    this.map.options.minZoom = 5;
    this.map.options.maxZoom = 15;
    L.tileLayer("http://49.156.148.124/map/{z}/{x}/{y}.png").addTo(this.map);
    // this.marker = L.marker([17.39062, 78.55883]).addTo(this.map);
    // this.map.on('mousemove', function (e:any) {
    //   this.marker.setLatLng(e.latlng);
      
    // },this);
    this.map.on('click',function(e:any) {
      
      this.trip.tripPoints[this.pointIndex].point.locCurrent.lat = e.latlng.lat;
      this.trip.tripPoints[this.pointIndex].point.locCurrent.lng = e.latlng.lng;
      
      this.updateType = "update";
      this._trip.getAddress(e.latlng).subscribe(res =>{
        this.trip.tripPoints[this.pointIndex].point.locCurrent.address = res.display_name;
      });
      this.updateMapData();
      // var tr = new TripService();
      
    },this);
    
  }
  private _filterName(i:any){
    console.log(i);
    this.filteredPoints = this._filter(this.trip.tripPoints[i].point.name);
  }
  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.searchpoints.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }
  // validateAllFormFields(formGroup: any) {
    // console.log(formGroup);       
    // Object.keys(formGroup.controls).forEach(field => { 
      // const control = formGroup.get(field);          
      // console.log(field);
    // });
  // }
  addMarker(i:any){
    this.marker = L.marker(this.trip.tripPoints[i].point.locCurrent).addTo(this.map);
    this.markers.push({ index: i, marker: this.marker });
    this.polylines.push(L.latLng(this.trip.tripPoints[i].point.locCurrent));
  }
  addPolyline(){
    if (this.polylines.length > 1) {
      if (this.polyline) {
        this.map.removeLayer(this.polyline);
      }
      this.polyline = L.polyline(this.polylines, { color: 'red' }).addTo(this.map);
      // this.route = [];
      // this.polylines.forEach(element => {
      //   this.route.push([element.lat,element.lng])

      // });
      // console.log(JSON.stringify(this.route));

    }
  }
  updateMapData(){
    // debugger;
    var mi = this.markers.findIndex(m => m.index == this.pointIndex);
    if (mi > -1) {
      if(this.updateType != 'shift'){
        this.markers[mi].marker.setLatLng(this.trip.tripPoints[this.pointIndex].point.locCurrent);
      }
      this.polylines[mi] = L.latLng(this.trip.tripPoints[this.pointIndex].point.locCurrent);
    } else {
      if (this.trip.tripPoints[this.pointIndex] ){
        this.addMarker(this.pointIndex);
      }      
    }
    this.addPolyline();
    
  }
  // add trip function
  add(f: any) {  
    console.log(f);
    if (f.valid) {
      if(this.trip.id==0 || this.trip.id == undefined){
        this._trip.saveTrip(this.trip).subscribe((res: any) => {
          if (document.activeElement.id == 'savens') {
            this.trip = res;
            this.schedule();
          }else{
            this.successMsg(res);
          }
          
        });
      }else{
        this._trip.updateTrip(this.trip).subscribe((res: any) => {
          if (document.activeElement.id == 'savens') {
            this.trip = res;
            this.schedule();
          } else {
            this.successMsg(res);
          }
        });
      }

    }
    
  }

  successMsg(res:any){
    Swal.fire({
      icon: 'success',
      title: 'Trip Successfully Saved',
      showConfirmButton: true,
      confirmButtonText: 'Continue Editing',
      showCancelButton: true,
      cancelButtonColor: '#25b54a',
      cancelButtonText: 'Add New'
    }).then((result) => {
      if (result.isConfirmed) {
        // window.location.href = '/trips/addtrip/'+rid;
        this.trip = res;
      } else {
        window.location.href = '/trips/addtrip';
      }
    });
  }
  schedule() {
    var trip:any = {};
    trip.tripId = this.trip.id;
    trip.tripPoints = this.trip.tripPoints;
    trip.changeTripSeq = false;
    trip.add = true;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "70%";
    dialogConfig.height = "80%";
    dialogConfig.data = trip;

    // console.log(this.trip);
    let dialogRef = this.dialog.open(ScheduleComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.scheduleData = result.data;
        if (result.status == 'ok') {
          if (this.req.id) {

          } else {
            window.location.href = '/trips/addtrip';
          }

        }
      }
      
    });
  }

  // table contant drag drop
  drop(event: CdkDragDrop<Object[]>) {
    // console.log(event);
    // debugger;
    moveItemInArray(this.trip.tripPoints, event.previousIndex, event.currentIndex);
    moveItemInArray(this.polylines, event.previousIndex, event.currentIndex);
    // moveItemInArray(this.markers, event.previousIndex, event.currentIndex);
    var temp = {index:event.previousIndex,marker:this.markers[event.currentIndex].marker};
    this.markers[event.currentIndex] = { index: event.currentIndex, marker: this.markers[event.previousIndex].marker };
    this.markers[event.previousIndex] = temp;
    this.pointIndex = event.previousIndex;
    this.updateType ="shift";
    this.updateMapData();
    this.changeActiveIndex(event.previousIndex, event.currentIndex);
  }

  newPoint(){
    var point = JSON.parse(JSON.stringify(this.point));
    this.trip.tripPoints.push(point);
  }
  removePoint(index:any){
    this.trip.tripPoints.splice(index,1);
    var mi = this.markers.findIndex(m => m.index == this.pointIndex);
    this.map.removeLayer(this.markers[mi].marker);
    this.markers.splice(mi,1);
    this.polylines.splice(index,1);
    this.updateType = "remove";
    this.updateMapData();
  }
  shiftUp(index: any){
    var temp = this.trip.tripPoints[index-1];
    this.trip.tripPoints[index-1] = this.trip.tripPoints[index];
    this.trip.tripPoints[index] = temp;
    this.updateType = "shift";
    // moveItemInArray(this.markers, index, index-1);
    moveItemInArray(this.polylines, index, index-1);
    this.updateMapData();
    this.changeActiveIndex(index,index-1);
  }
  shiftDown(index: any) {

    var temp = this.trip.tripPoints[index + 1];
    this.trip.tripPoints[index + 1] = this.trip.tripPoints[index];
    this.trip.tripPoints[index] = temp;
    this.updateType = "shift";
    // moveItemInArray(this.markers, index, index + 1);
    moveItemInArray(this.polylines, index, index + 1);
    this.updateMapData();
    this.changeActiveIndex(index, index + 1);
  }
  changeActiveIndex(i:any,j:any){
    if(i == this.pointIndex){
      this.pointIndex = j;
    }
  }
  selectPoint(pi){
    // this.pointName.reset();
    this._filterName(pi);
    this.pointIndex = pi;
  }
  clientSelected(clientIId:any,index: any){
    var clientIndex = this.clients.findIndex((c:any)=>{ c.id == clientIId });
    this.trip.tripPoints[index].clientId = this.clients[clientIndex].id;
    this.trip.tripPoints[index].point.locCurrent = this.clients[clientIndex].location;
    this.trip.tripPoints[index].name = this.clients[clientIndex].name;
    this.selectPoint(index);
    
  }

  typeSelected(index:any){
    this.selectPoint(index);
    if (this.trip.tripPoints[index].type =='VISIT') {
      this.trip.tripPoints[index].clientId = 0;
      this.trip.tripPoints[index].point.locCurrent = {
        "address": "",
        "lat": null,
        "lng": null
      };
      
    }
  }

  selectedName(i: any){
    var searchindex =  this.searchpoints.findIndex(p=> p.name == i.option.value);
    this.trip.tripPoints[this.pointIndex].point = JSON.parse(JSON.stringify(this.searchpoints[searchindex]));
    console.log(this.searchpoints[searchindex]);
    this.updateMapData();

  }

  ontextInLoc(){
    if (this.trip.tripPoints[this.pointIndex].point.locCurrent.lat == null) {
        this.snakbar("Please Select Location Before Adding Address");
      this.trip.tripPoints[this.pointIndex].point.locCurrent.address = "";
    }
  }
sequence(index: any){
  this.trip.tripPoints[index].sequence = index;
  return index;
}
snakbar(msg: any){
  this.snackBar.open(msg, '', {
    duration: 5000
  });

}


notify(data : string){
  let status = Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 1500
  });
  console.log(status);
}

expTimeValidate(time:any){
  var str = time;
  var patt = new RegExp("^[0-9]{1,2}[:][0-9]{1,2}$");
  var res = patt.test(str);
  // console.log("pt: ",res);
  return res;
}
}

