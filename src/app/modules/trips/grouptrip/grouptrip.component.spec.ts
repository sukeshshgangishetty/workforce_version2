import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrouptripComponent } from './grouptrip.component';

describe('GrouptripComponent', () => {
  let component: GrouptripComponent;
  let fixture: ComponentFixture<GrouptripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrouptripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrouptripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
