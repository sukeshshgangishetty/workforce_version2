import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TripService } from '../trip.service';

@Component({
  selector: "app-rounds",
  templateUrl: "./rounds.component.html",
  styleUrls: ["./rounds.component.sass"],
})
export class RoundsComponent implements OnInit {
  req: Params;
  heading = "Trip";
  subheading = "Welcome to Trip Section";
  icon = "fa fa-line-chart";
  btntext = "Back to Trips";
  btnLink = "trips/trips";
  btnIcon = "angle-left";

  schedules: any = { schedules: [], executiveResponses:[]};
  visit:any = [];
  scheduleData: any;

  tripByExeId: any = 0;
  roundByExeId: any = 0;

  constructor(private _trip: TripService, private _route: ActivatedRoute) {
    this.req = this._route.snapshot.params;
    console.log("REQ:", this.req);
  }
  columns: any[];
  paginateData: any[];
  pos: any;
  release: boolean = true;
  ngOnInit() {
    this.columns = [
      { field: "_id", header: "Client/Id" },
      { field: "purpose", header: "Purpose" },
      { field: "checkin", header: "Check In" },
      { field: "checkout", header: "Check Out" },
      { field: "spenttime", header: "Spent Time" },
      { field: "distance", header: "Distance" },
      { field: "status", header: "Status" },
    ];

    // Get-Schedule
    this.getSchedules();
    
  }

  selectExecutive(data:any) {
    this.tripByExeId = data;
    this.getSchedulesByExeID(data);
  }
  getVisits(index: number) {
    if (this.schedules.schedules[index].visits == undefined) {
      var sid = this.schedules.schedules[index].id;
      this._trip.getVisits(sid).subscribe((res: any) => {
        console.log("Res", res);
        this.schedules.schedules[index].visits = res.body;
        this.visit = this.schedules.schedules[index].visits;
        console.log("visits", this.visit);
      });
    }
  }
  getSchedules(){
    this._trip.getschduleAlloc(this.req.id).subscribe((res: any) => {
      this.schedules = res;
      // console.log("schdules", this.schedules);
    });
  }
  getSchedulesByExeID(exeID:any){
    if(this.req.id){
      if(exeID == 0){
        this.getSchedules();
      }else{
        this._trip.getSchedulesOfTripByExeID(this.req.id, exeID).subscribe((res: any) => {
          this.schedules.schedules = res;
        });
      }
    }
  }
}
