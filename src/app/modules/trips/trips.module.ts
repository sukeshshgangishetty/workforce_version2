import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TripsRoutingModule } from './trips-routing.module';
import { TripsComponent } from './trips/trips.component';

import { PageTitleModule } from "../../Layout/Components/page-title/page-title.module";
import { AgmCoreModule } from "@agm/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatTooltipModule,
  MatTreeModule,
  MatExpansionModule
} from "@angular/material";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { AddtripComponent } from './addtrip/addtrip.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { PerfectScrollbarConfigInterface,PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { DropzoneConfigInterface, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DragDropModule } from "@angular/cdk/drag-drop";
import { TripComponent } from './trip/trip.component';
import { RoundsComponent } from './rounds/rounds.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { CommonModule as Common } from '../common/common.module';
import { GrouptripComponent } from './grouptrip/grouptrip.component';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: "https://httpbin.org/post",
  maxFilesize: 50,
  acceptedFiles: "image/*",
};

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [TripsComponent, AddtripComponent, TripComponent, RoundsComponent,ScheduleComponent, GrouptripComponent],
  imports: [
    CommonModule,
    TripsRoutingModule,
    PageTitleModule,
    AgmCoreModule,
    NgbModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatIconModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatIconModule,
    MatTabsModule,
    MatTooltipModule,
    MatTreeModule,
    MatRadioModule,
    AngularFontAwesomeModule,
    NgxDaterangepickerMd.forRoot(),
    MatSlideToggleModule,
    PerfectScrollbarModule,
    MatExpansionModule,
    DragDropModule,
    Common
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      // DROPZONE_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
      // DEFAULT_DROPZONE_CONFIG,
    },
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG,
    },
  ],
  entryComponents: [
    ScheduleComponent
  ],
})
export class TripsModule {}
