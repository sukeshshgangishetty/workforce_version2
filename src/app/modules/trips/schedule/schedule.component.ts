import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { ExecutiveService } from '../../executive/executive.service';
import { TripService } from "../trip.service";
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.sass']
})
export class ScheduleComponent implements OnInit {

   msg: string;
  schedule: any = { changeTripSeq: false, expStrTym: { startDate: moment(), endDate: moment() }, expEndTym: { startDate: moment(), endDate: moment() }};
  executives: any = [];
  schedules:any = [];
  schedulesCopy: any =[];
  scheduleCopy: any = { changeTripSeq: false, expStrTym: { startDate: moment(), endDate: moment() }, expEndTym: { startDate: moment(), endDate: moment() } };
  constructor(private snackBar: MatSnackBar,private _trip: TripService,public dialogRef: MatDialogRef<ScheduleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private _executive: ExecutiveService) { 
      // this.schedule = data;
    this.schedule = data;
    console.log(this.schedule);
    this.scheduleCopy = JSON.parse(JSON.stringify(this.schedule));
    this.schedules.push(JSON.parse(JSON.stringify(this.schedule)));
    this.schedulesCopy.push(JSON.parse(JSON.stringify(this.schedule)));
    }

  ngOnInit() {
    console.warn(this.schedule.assignedTo);
    this._executive.get().subscribe((res: any) => {
      this.executives = res.body;
      console.log("executives:",this.executives);
    });
  }
  sadd(f:any,i:number){
    if(f.valid){
      this.schedule = this.schedules[i];
      console.log(this.schedule);

      this.schedule.expStrTym = Number.isInteger(this.schedule.expStrTym) ? this.schedule.expStrTym:this.schedule.expStrTym.startDate.toDate().getTime();
      this.schedule.expEndTym = Number.isInteger(this.schedule.expEndTym) ? this.schedule.expEndTym:this.schedule.expEndTym.startDate.toDate().getTime();
      this._trip.saveSchedule(this.schedule).subscribe((res) => {
      // this.dialogRef.close({ status: "ok", data: res });
        var add = this.schedule.add;
        this.schedules[i] = res;
        this.schedules[i].add = add;
        this.schedulesCopy[i] = JSON.parse(JSON.stringify(this.schedules[i]));
      this.msg = "Scheduled Sucessfully!";
        this.snackBar.open(this.msg, "", {
          duration: 5000,
        });
      },
        (error) => {
          this.snackBar.open(error.error.message, "", {
            duration: 5000,
          });
        });
      
    }
  }
  exe(i:number){
    // console.log(this.executives.findIndex(e => e.exeId == this.schedules[i].assignedTo[0]));
    return this.executives[this.executives.findIndex(e => e.exeId == this.schedules[i].assignedTo[0])].name;
  }

  addS(){
    this.schedules.push(JSON.parse(JSON.stringify(this.scheduleCopy)));
  }

  isSaved(i:number): boolean{
    if (JSON.stringify(this.schedules[i]) == JSON.stringify(this.schedulesCopy[i]) && (this.schedules[i].id != 0 && this.schedules[i].id)){
      return true;
    }else{
      return false;
    }
  }
  
 notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
