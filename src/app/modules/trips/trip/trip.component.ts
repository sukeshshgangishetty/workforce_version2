import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as L from "leaflet";
import "mapbox-gl-leaflet";
import * as moment from "moment";

@Component({
  selector: "app-trip",
  templateUrl: "./trip.component.html",
  styleUrls: ["./trip.component.sass"],
})
export class TripComponent implements OnInit, AfterViewInit {
  // pagetitle Code
  @Output() public callback: EventEmitter<any> = new EventEmitter();

  date = moment();
  end = moment().endOf("month").format("D");

  heading = "Trip";
  subheading = "Welcome to Trip Section";
  icon = "fa fa-line-chart";
  btntext = "Back to Trips";
  btnLink = "trips/trips";
  btnIcon = "angle-left";

  private map: L.Map;

  ngAfterViewInit() {
    this.map = L.map("map").setView([17.39062, 78.55883], 11);
    this.map.options.minZoom = 5;
    this.map.options.maxZoom = 15;

    L.tileLayer("http://49.156.148.124/map/{z}/{x}/{y}.png").addTo(this.map);
    var marker = L.marker([17.39062, 78.55883]).addTo(this.map);
    var marker = L.marker([17.4007, 78.8089]).addTo(this.map);
  }

  constructor() {}

  ngOnInit() {}

  changeMonth(m) {
    if (m == "add") {
      this.date.add(1, "month");
    } else {
      this.date.subtract(1, "month");
    }
    this.end = this.date.endOf("month").format("D");
  }
  scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: "smooth" });
  }
}
