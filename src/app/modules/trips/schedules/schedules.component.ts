import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TripService } from '../trip.service';
import { ScheduleComponent } from '../schedule/schedule.component';

@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.sass']
})
export class SchedulesComponent implements OnInit {

  @Input() schedules:any = [];
  @Input() TripId:any = '';
  visit : any = {};
  @Input() isTrip: any = false;
  @Input() exeID: any = 0;
  scheduleData: any;
  constructor(public dialog: MatDialog,private _trip: TripService) { }
  ngOnInit() {
    console.log(this.exeID);
  }
  
  getVisits(index: number) {
    // console.log(this.schedules);
    // debugger;
    if (this.schedules[index].visits == undefined) {
      var sid = this.schedules[index].id;
      this._trip.getVisits(sid).subscribe((res: any) => {
        // console.log("Res", res);
        this.schedules[index].visits = res.body;
        this.visit = this.schedules[index].visits;
        this.schedules[index].selectExecutive = this.schedules[index].visits.schAllocId;
        // console.log("visits", this.visit);
      });
    }
  }
  scheduleAlloc(allocID:any,index:any){
    this._trip.getVisitsByAllocID(allocID).subscribe((res:any)=>{
      this.schedules[index].visits.scheduleVisits = res.body;
    });
  }
  schedule(i: any) {
    
    var trip:any = {};
    trip.expStrTym = this.schedules[i].expStrTym;
    trip.expEndTym = this.schedules[i].expEndTym;
    trip.name = this.schedules[i].name;
    trip.description = this.schedules[i].description;
    trip.assignedType = this.schedules[i].assignedType;
    trip.changeTripSeq = false;
    trip.tripId = this.TripId;
    trip.id = this.schedules[i].id;
    trip.tripPoints =[];
    trip.add = false;
    this.schedules[i].visits.scheduleVisits.map((res:any)=>{
      trip.tripPoints.push(res.tripPoint);
    });
    trip.assignedTo = [];
    this.schedules[i].visits.executives.map((res: any)=>{
      trip.assignedTo.push(res.exeId);
    });
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "70%";
    dialogConfig.height = "80%";
    dialogConfig.data = trip;
    let dialogRef = this.dialog.open(ScheduleComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.scheduleData = result.data;
        if (result.status == 'ok') {
          console.log(result);
          
        }
      }

    });
  }
}
