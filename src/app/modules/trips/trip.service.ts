import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: "root",
})
export class TripService {
  constructor(private _http: HttpClient) {}

  getAddress(loc: any) {
    return this._http.get(
      `/search?format=json&lat=${loc["lat"]}&lon=${loc["lng"]}&zoom=18`
    );
  }
  saveTrip(trip: any) {
    return this._http.post("/workforce/api/v1/trip", trip);
  }
  saveSchedule(schedule: any) {
    if(schedule.id !=0 && schedule.id){
      return this._http.put("/workforce/api/v1/trip/schedule/"+schedule.id, schedule);
    }else{
      return this._http.post("/workforce/api/v1/trip/schedule", schedule);
    }
  }
  updateTrip(trip: any) {
    return this._http.put("/workforce/api/v1/trip/" + trip.id, trip);
  }
  points() {
    return this._http.get("/workforce/api/v1/trip/points");
  }
  getTrips() {
    return this._http.get("/workforce/api/v1/trip");
  }
  // getTrack(){
  //   return this._http.get('/workforce/track/td');
  // }
  getschduleAlloc(id: string) {
    return this._http.get("/workforce/api/v1/trip/" + id);
  }
  getVisits(id: string) {
    return this._http.get("/workforce/api/v1/trip/schedule/" + id);
  }
  getTripsByExeId(id: any) {
    return this._http.get("/workforce/api/v1/trip/schedule/exec/" + id);
  }
  getSchedulesOfTripByExeID(tripID:any,exeID:any){
    return this._http.get(`/workforce/api/v1/trip/${ tripID }/scheduleAlloc/${ exeID }`);
  }
  getVisitsByAllocID(allocID:any){
    return this._http.get(`/workforce/api/v1/trip/scheduleAlloc/${allocID}/visits`);
  }
}
