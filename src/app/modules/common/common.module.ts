import { NgModule } from '@angular/core';
import { CommonModule as AngularCommon } from '@angular/common';
import { SchedulesComponent } from '../trips/schedules/schedules.component';
import { DateDirective } from '../../directives/date.directive';
import {
  MatIconModule, MatExpansionModule, MatSelectModule, MatFormFieldModule,MatInputModule } from "@angular/material";
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [DateDirective, SchedulesComponent],
  imports: [
    AngularCommon, MatIconModule, MatExpansionModule, MatSelectModule, MatFormFieldModule, MatInputModule, FormsModule
  ],
  exports: [DateDirective, SchedulesComponent]
})
export class CommonModule { }
