import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageusersRoutingModule } from './manageusers-routing.module';
import { ViewusersComponent } from './viewusers/viewusers.component';
import { ViewrolesComponent } from './viewroles/viewroles.component';
import { CreaterolesComponent } from './createroles/createroles.component';
import { UserpermissionComponent } from './userpermission/userpermission.component';
import { MerchantpermissionsComponent } from './merchantpermissions/merchantpermissions.component';
import { UserstatusComponent } from './userstatus/userstatus.component';
import { DistributoraccessComponent } from './distributoraccess/distributoraccess.component';


@NgModule({
  declarations: [ViewusersComponent, ViewrolesComponent, CreaterolesComponent, UserpermissionComponent, MerchantpermissionsComponent, UserstatusComponent, DistributoraccessComponent],
  imports: [
    CommonModule,
    ManageusersRoutingModule
  ]
})
export class ManageusersModule { }
