import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreaterolesComponent } from './createroles/createroles.component';
import { DistributoraccessComponent } from './distributoraccess/distributoraccess.component';
import { MerchantpermissionsComponent } from './merchantpermissions/merchantpermissions.component';
import { UserpermissionComponent } from './userpermission/userpermission.component';
import { UserstatusComponent } from './userstatus/userstatus.component';
import { ViewrolesComponent } from './viewroles/viewroles.component';
import { ViewusersComponent } from './viewusers/viewusers.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'viewusers',
        component: ViewusersComponent,
      },
      {
        path: 'viewroles',
        component: ViewrolesComponent,
      },
      {
        path: 'createroles',
        component: CreaterolesComponent,
      },
      {
        path: 'userpermission',
        component: UserpermissionComponent,
      },
      {
        path: 'merchantpermission',
        component: MerchantpermissionsComponent,
      },
      {
        path: 'userstatus',
        component: UserstatusComponent,
      },
      {
        path: 'distributoracess',
        component: DistributoraccessComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageusersRoutingModule { }
