import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Params, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ClientService } from '../../client.service';

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.sass"],
})
export class AddComponent implements OnInit {
  req: Params;
  heading = "Add Client Category";
  mainheading = "Add"
  subheading = "Welcome to Category";
  icon = "pe-7s-light icon-gradient bg-info";
  btntext = "Back Category";
  btnIcon = "angle-left";
  btnLink = "client/category";
  cat: any = { id: 0, isActive: true };

  // cat: any = [];
  msg: string;
  constructor(
    private _client: ClientService,
    private snackBar: MatSnackBar,
    private _route: ActivatedRoute,
    private router: Router
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    // Get-Client Category's
    if (this.req.id != 0 && this.req.id != undefined) {
      console.log("am in edit ---> ");
      this.heading = "Edit Client Category";
      this.mainheading = "Edit"
      this._client.getcatById(this.req.id).subscribe((res: any) => {
        this.cat = res.body;
        console.log("CatById:", res);
      });
    }
  }
  add(f) {
    if (f.valid) {
      console.log("Clent Category  Data:", this.cat);
      if (this.cat.id <= 0 || this.cat.id == undefined) {
        this._client.addcat(this.cat).subscribe(
          (res) => {
            this.msg = "Client Category Added";
            this.cat = { id: 0, isActive: true };
            this.notify(this.msg);
            this.router.navigate(['/client/category']);
          },
          (error) => {
            this.snackBar.open(this.msg, "", {
              duration: 5000,
            });
          }
        );
      } else {
        // update
        this._client.updatecat(this.cat).subscribe(
          (res) => {
            this.msg = "Client Category Updated";
            this.notify(this.msg);
            this.router.navigate(['/client/category']);
          },
          (error) => {
            this.snackBar.open(this.msg, "", {
              duration: 5000,
            });
          }
        );
      }
    }
  }



  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
