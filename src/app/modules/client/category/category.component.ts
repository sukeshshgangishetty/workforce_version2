import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { MatSnackBar } from "@angular/material";
import Swal from "sweetalert2";

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.scss"],
})
export class CategoryComponent implements OnInit {
  heading = "Client Category";
  subheading = "Welcome to Category";
  icon = "pe-7s-light icon-gradient bg-info";
  btntext = "Add  Category";
  btnIcon = "plus";
  btnLink = "client/category/0";

  cat = { catid: 0 };

  categories = [];

  constructor(private _client: ClientService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this._client.getcat().subscribe((res: any) => {
      this.categories = res.body;
    });
  }

  deleteCat(id: string) {
    console.log("ID:", id);
    this.DeleteMsg(id);
  }

  DeleteMsg(id: any) {
    Swal.fire({
      icon: "warning",
      title: "Do You really Want To Delete ?",
      showConfirmButton: true,
      confirmButtonText: "Yes",
      showCancelButton: true,
      cancelButtonColor: "#25b54a",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this._client.delete(id).subscribe(
          (res) => {
            var index = this.categories.findIndex((c) => c.id == id);
            this.categories.splice(index, 1);
            this.notify("Category Successfully Deleted");

          },
          (error) => {

            this.notify("Sorry,Somthing Went Wrong, Please Try Again Later");

          }
        );
      }
    });
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
