import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.sass']
})
export class ClientComponent implements OnInit {
  heading = 'Clients list';
  subheading = 'Welcome To Client Management';
  icon = 'pe-7s-user icon-gradient btn-primary';
  btntext = 'Add Client';
  btnLink = 'client/0';
  viewType: string = 'list';
  clients : any=[];

  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  slides = [
    { img: '1' },
    { img: '2' },
    { img: '3' },
    

  ];
  slideConfig = {
    slidesToShow: 1,
    dots: true,
  };

  constructor(private _client: ClientService) { }

  ngOnInit() {
    this._client.get().subscribe((res: any)=>{
        this.clients = res.body;
        console.log("Clients:",this.clients);
    });
  //   this._client.getclients(1,100).subscribe((res: any)=>{
  //     this.clients = res.body;
  //     console.log("Clients:",this.clients);
  // });
  }

  viewTypeChange() {
    if( this.viewType == 'list'){
      this.viewType = 'card';
    }else{
      this.viewType = 'list';
    }
    
	}


}
