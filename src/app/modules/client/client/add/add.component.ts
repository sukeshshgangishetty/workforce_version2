import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientService } from '../../client.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import Swal from 'sweetalert2';
import { MatSnackBar, MatVerticalStepper } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DropzoneConfigInterface, DropzoneDirective } from 'ngx-dropzone-wrapper';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { ExecutiveService } from 'src/app/modules/executive/executive.service';
import { LocalDetailsService } from 'src/app/modules/common-Services/local-details.service';





@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.sass"],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true, }
  }]
})
export class AddComponent implements OnInit {
  heading = "Add Client";
  subheading = "Welcome To Add Client Section";
  icon = "pe-7s-user icon-gradient btn-primary";
  btntext = "Back to Clients";
  btnLink = "client";
  btnIcon = "angle-left";
  clients: any;

  client: any = {

    location: { "address": "", "lat": 0, "lng": 0 }, category: { id: null }, route: { id: null },
    repayPeriod: 0,
    assignedTo: 0,
    ownerDetails: [{
      "department": "",
      "description": "",
      "designation": "",
      "dob": 0,
      "image": "",
      "emailId": "",
      "isPrimary": true,
      "name": "",
      "phoneNo": ""
    }],
    addresses: [{ "line1": "", "line2": "", "city": "", "state": "", "zipcode": "", "longlatitude": "", "isBilling": true, "country": "", "name": "" }],
    accountDetails: [{ "name": "", "accNo": "", "accType": "", "bankName": "", "branch": "", "ifsc": "" }],
    kycDetail: {},
    contactDetail: {}//social links
  };
  clientCopy: any = {
    location: { "address": "", "lat": 200, "lng": 200 }, category: { id: null }, route: { id: null },
    repayPeriod: 0,
    assignedTo: 0,
    ownerDetails: [{
      "department": "",
      "description": "",
      "designation": "",
      "dob": 0,
      "image": "",
      "emailId": "",
      "isPrimary": true,
      "name": "",
      "phoneNo": ""
    }],
    contactDetail: {},
    kycDetail: {},
    addresses: [{ "line1": "", "line2": "", "city": "", "state": "", "zipcode": "", "longlatitude": "", "isBilling": true.valueOf, "country": "", "name": "" }],
    accountDetails: [{ "name": "", "accNo": "", "accType": "", "bankName": "", "branch": "", "ifsc": "" }]
  };
  CCs: any = {
    ownerDetails: {
      "department": "",
      "description": "",
      "designation": "",
      "dob": 0,
      "image": "",
      "emailId": "",
      "isPrimary": false,
      "name": "",
      "phoneNo": ""
    },
    addresses: { "line1": "", "line2": "", "city": "", "state": "", "zipcode": "", "longlatitude": "", "isBilling": false, "country": "", "name": "" },
    accountDetails: { "name": "", "accNo": "", "accType": "", "bankName": "", "branch": "", "ifsc": "" }
  };
  req: Params;
  msg: string;
  secondFormGroup: FormGroup;
  cat: any = [];
  route: any = [];
  exe: any = [];
  root: any = [];
  arealist: any = ["ameerpet", "uppal", "hightech", "madhupur"];
  userDetails: any;
  // @ViewChild('stepper') public stepper: MatVerticalStepper;

  constructor(
    private formBuilder: FormBuilder,
    private _client: ClientService,
    private _route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _executive: ExecutiveService,
    private localDetailsService: LocalDetailsService,
    private router: Router,
  ) {
    this.req = this._route.snapshot.params;
  }
  ngOnInit() {
    this.userDetails = this.localDetailsService.userLoginDetails || null;
    this.userDetails = JSON.parse(this.userDetails);
    if (this.req.id != 0 && this.req.id != undefined && this.req.id != 'addnewclient') {
      this.heading = "Edit Client ";
      this.subheading = "Welcome To Edit Client Section";
      this._client.view(this.req.id).subscribe((res: any) => {
        if (res) {
          this.client = res.body;
          console.log(this.client)
        }
        this.checkObjs();
      });
    }
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ["", Validators.required],
    });
    this._client.getcat().subscribe((res: any) => {
      this.cat = res.body;
      console.log("cat:", this.cat);
    });
    this.cat = this.cat.filter((res) => {
      return res.title
        .toLocaleLowerCase()
        .match(this.client.category.id.toLocaleLowerCase());
    });
    this._client.getRouteNo().subscribe((res: any) => {
      this.route = res.body;
    });

    this.route = this.route.filter((res) => {
      return res.title
        .toLocaleLowerCase()
        .match(this.client.route.id.toLocaleLowerCase());
    });

    this._executive.get().subscribe((res: any) => {
      this.exe = res.body;
      console.log(this.exe);
    });
  }
  handleFileInputChange() {

  }
  addCC(ref: string) {
    this.client[ref].push(JSON.parse(JSON.stringify(this.CCs[ref])));
  }
  removeCC(ref: string, i: number) {
    this.client[ref].splice(i, 1);
  }
  changeStatus(ref: string, attr: string, i: number) {
    this.client[ref].map((d: any, index: number) => {
      if (i != index) {
        this.client[ref][index][attr] = false;
      }
    });
  }
  save(f: any) {
    const body = {
      "name": this.client.name || "",
      "description": this.client.description || "",
      "phoneNo": this.client.phoneNo || "",
      "landlineNo": this.client.landlineNo || "",
      "email": this.client.email || "",
      "incorporatedAt": this.client.incorporatedAt || 0,
      "gstNo": this.client.gstNo || "",
      "assignedTo": this.client.assignedTo || "",
      "openingBalance": this.client.openingBalance || 0,
      "outstanding": this.client.outstanding || 0,
      "creditLimit": this.client.creditLimit || 0,
      "repayPeriod": this.client.repayPeriod || 0,
      "noOfEmp": this.client.noOfEmp || 0,
      "location": this.client.location,
      "clientRequest": {
        "title": "",
        "description": "",
        "requestType": "UPDATE",
      },
      "ownerDetails": this.client.ownerDetails,
      "addresses": this.client.addresses,
      "category": {
        "additionalProp1": {},
        "additionalProp2": {},
        "additionalProp3": {}
      },
      "categoryId": this.client.categoryId,
      "routenoId": this.client.route.id
    }
    if (f.valid) {
      this._client.save(body).subscribe(
        (res: any) => {
          this.msg = "Client Updated";
          if (!this.client.id) {
            this.client = JSON.parse(JSON.stringify(this.clientCopy));
            this.msg = "Client Added";
            localStorage.setItem("client", JSON.stringify(this.client));
          }
          this.notify(this.msg);
          this.client = res.body;
          this.checkObjs();

        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }
  clientaddId: any
  saveAddress(f: any, i: any) {
    this.client.addresses[i].name = ""
    if (!this.client.addresses[i].id) {
      const clients = localStorage.getItem('client')
      const clientId = JSON.parse(clients)
      this.clientaddId = clientId.id
    } else {
      this.clientaddId = this.client.id
    }
    console.log(this.clientaddId)
    if (f.valid) {
      this._client.saveAddress(this.clientaddId, this.client.addresses[i]).subscribe(
        (res: any) => {
          this.msg = "Client Address Updated";
          if (!this.client.addresses[i].id) {
            this.msg = "Client Address Added";
          }
          this.notify(this.msg);
          this.client.addresses[i] = res.body;
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }
  saveAccount(f: any, i: any) {
    if (!this.client.accountDetails[i].id) {
      this.clientaddId = this.clientaddId
    } else {
      this.clientaddId = this.client.id
    }
    console.log(this.clientaddId)
    if (f.valid) {
      this._client.saveAccount(this.clientaddId, this.client.accountDetails[i]).subscribe(
        (res: any) => {
          this.msg = "Client Account Details Updated";
          if (!this.client.accountDetails[i].id) {
            this.msg = "Client Account Details Added";
          }
          this.notify(this.msg);
          this.client.accountDetails[i] = res.body;
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }
  saveKyc(f: any) {
    if (!this.client.kycDetail.id) {
      this.clientaddId = this.clientaddId
    } else {
      this.clientaddId = this.client.id
    }
    if (f.valid) {
      this._client.saveKyc(this.clientaddId, this.client.kycDetail).subscribe(
        (res: any) => {
          this.msg = "Client KYC Details Updated";
          if (!this.client.kycDetail.id) {
            this.msg = "Client KYC Details Added";
          }
          this.notify(this.msg);
          this.client.kycDetail = res.body;
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }
  saveSocial(f: any) {
    if (!this.client.contactDetail.id) {
      this.clientaddId = this.clientaddId
    } else {
      this.clientaddId = this.client.id
    }

    if (f.valid) {
      this._client.saveSocial(this.clientaddId, this.client.contactDetail).subscribe(
        (res: any) => {
          this.msg = "Client Social Links Updated";
          if (!this.client.contactDetail.id) {
            // this.client = JSON.parse(JSON.stringify(this.clientCopy));
            this.msg = "Client Social Links Added";
          }
          this.notify(this.msg);
          this.client.contactDetail = res.body;
          // this.checkObjs();
          if (res.statusCode == "OK") {
            this.router.navigate(['/client'])
          }
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }
  updateStatus(obj: string, key: string, i: number) {
    var keys = { "addresses": 'address', "accountDetails": 'accDetail' };
    var id = this.client[obj][i].id;
    this._client.updatePrimaryStatus(keys[obj], id).subscribe(res => {
      this.client[obj][i][key] = true;
      this.changeStatus(obj, key, i);
      this.snackBar.open("Primary status Update Successful", "", {
        duration: 5000,
      });
    }, error => {
      this.snackBar.open(this.msg, "", {
        duration: 5000,
      });
    });
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

  checkObjs() {
    if (!this.client.category) {
      this.client.category = {};
    }
    if (!this.client.route) {
      this.client.route = {};
    }
    if (!this.client.contactDetail) {
      this.client.contactDetail = {};
    }
    if (!this.client.kycDetail) {
      this.client.kycDetail = {}
    }
    if (this.client.addresses.length < 1) {
      this.client.addresses = JSON.parse(JSON.stringify(this.clientCopy.addresses));
    }
    if (this.client.accountDetails.length < 1) {
      this.client.accountDetails = JSON.parse(JSON.stringify(this.clientCopy.accountDetails));
    }
  }
  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
  };
  public onUploadInit(args: any): void {
    // console.log("onUploadInit:", args);
  }

  public onUploadError(args: any): void {
    console.log("onUploadError:", args);
  }

  public onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
  }
}
