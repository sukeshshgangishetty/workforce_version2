import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './client/client.component';
import { CategoryComponent } from './category/category.component';
import { AddComponent as AddClient } from './client/add/add.component';
import { EditclientComponent } from './editclient/editclient.component';
import { ViewclientComponent } from './viewclient/viewclient.component';
import { AddnewclientComponent } from './addnewclient/addnewclient.component';
import { AddComponent } from './category/add/add.component';
import { RootnumberComponent } from './rootnumber/rootnumber.component';
import { AddrootnoComponent } from './rootnumber/addrootno/addrootno.component';
import { ClientStatusComponent } from './client-status/client-status.component';
import { MerchantsComponent } from './merchants/merchants.component';
import { InactiveClientsComponent } from './inactive-clients/inactive-clients.component';


const routes: Routes = [
  {
    path: "",

    children: [
      {
        path: "category",
        component: CategoryComponent,
      },
      {
        path: "routeNumber",
        component: RootnumberComponent,
      },
      {
        path: "clientstatus",
        component: ClientStatusComponent,
      },
      {
        path: "merchants",
        component: MerchantsComponent,
      },
      {
        path: "inactiveclients",
        component: InactiveClientsComponent,
      },
      // {
      //   path: "viewclient/:id",
      //   component: ViewclientComponent,
      // },
      {
        path: "view/:id",
        component: EditclientComponent,
      },
      {
        path: "category/:id",
        component: AddComponent,
      },
      {
        path: "route/:id",
        component: AddrootnoComponent,
      },
      {
        path: ":id",
        component: AddClient,
      },

      {
        path: "",
        component: ClientComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
