import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Params } from '@angular/router';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { DiscountService } from '../../discount/discount.service';
import { OrdersService } from '../../orders/orders.service';
import { ClientService } from '../client.service';

@Component({
  selector: "app-editclient",
  templateUrl: "./editclient.component.html",
  styleUrls: ["./editclient.component.sass"],
})
export class EditclientComponent implements OnInit {
  heading = "Edit Section";
  subheading = "Welcome to Edit Section";
  icon = "pe-7s-light icon-gradient bg-info";
  btntext = "Back to Clients";
  btnLink = "client";
  btnIcon = "angle-left";
  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
  };
  clientsById: any = {
    location: {},
    kycDetail: {},
    contactDetail: {},
    ownerDetails: [
      { }
    ],
  };
  msg: string;
  req: Params;
  clientOrders:any = [];
  clientDiscounts: any =[];

  constructor(
    private _client: ClientService,
    private _route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _order:OrdersService,
    private _disc: DiscountService
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    this._client.view(this.req.id).subscribe((res: any) => {
      res.location = res.location ? res.location : {};
      res.kycDetail = res.kycDetail ? res.kycDetail : {};
      res.contactDetail = res.contactDetail ? res.contactDetail : {};
      this.clientsById = res.body;

      console.log("clientsById:", this.clientsById);
    });

    // Client Orders by CLientID

    this._order.getOrderBycid(this.req.id).subscribe((res: any) =>{
      this.clientOrders = res.body;
    });

    // Client offer/Discounts by CLientID

    // this._disc.getofferBycid(this.req.id).subscribe((res: any) =>{
    //   this.clientDiscounts = res.body;
    // });

  }

  public onUploadInit(args: any): void {
    console.log("onUploadInit:", args);
  }

  public onUploadError(args: any): void {
    console.log("onUploadError:", args);
  }

  public onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
  }
}