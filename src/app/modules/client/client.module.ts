import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import { NgApexchartsModule } from 'ng-apexcharts';

import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client/client.component';
import { CategoryComponent } from './category/category.component';
import { AddComponent as CataddComponent } from "./category/add/add.component";
import { AddComponent } from './client/add/add.component';
import { ViewclientComponent } from './viewclient/viewclient.component';
import { EditclientComponent } from './editclient/editclient.component';
import { AddnewclientComponent } from './addnewclient/addnewclient.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { NgbModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import {
  MatDividerModule,
  MatCardModule,
  MatSnackBarModule,
  MatAutocompleteModule,
  MatSlideToggleModule,
} from "@angular/material";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatStepperModule } from '@angular/material/stepper';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CommonModule as Common } from '../common/common.module';
import { RootnumberComponent } from './rootnumber/rootnumber.component';
import { AddrootnoComponent } from './rootnumber/addrootno/addrootno.component';
import { ClientStatusComponent } from './client-status/client-status.component';
import { MerchantsComponent } from './merchants/merchants.component';
import { InactiveClientsComponent } from './inactive-clients/inactive-clients.component';
import { NgSelectModule } from '@ng-select/ng-select';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

@NgModule({
  declarations: [
    ClientComponent,
    CataddComponent,
    CategoryComponent,
    AddComponent,
    ViewclientComponent,
    EditclientComponent,
    AddnewclientComponent,
    RootnumberComponent,
    AddrootnoComponent,
    ClientStatusComponent,
    MerchantsComponent,
    InactiveClientsComponent,
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    PageTitleModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatTabsModule,
    MatInputModule,
    NgbModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatIconModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatRadioModule,
    AngularFontAwesomeModule,
    MatStepperModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    DropzoneModule,
    NgApexchartsModule,
    AngularEditorModule,
    CalendarModule,
    MatTooltipModule,
    MatCardModule,
    MatSnackBarModule,
    SlickCarouselModule,
    Common,
    NgSelectModule
  ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG,
    },

  ],
})
export class ClientModule { }
