import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { MatSnackBar } from "@angular/material";
import Swal from "sweetalert2";

@Component({
  selector: 'app-rootnumber',
  templateUrl: './rootnumber.component.html',
  styleUrls: ['./rootnumber.component.sass']
})
export class RootnumberComponent implements OnInit {
  heading = "Client Route No";
  subheading = "Welcome to Route No";
  icon = "pe-7s-light icon-gradient bg-info";
  btntext = "Add  Route No";
  btnIcon = "plus";
  btnLink = "client/route/0";

  root = { rootnoid: 0 };

  routenumbers = [];
  constructor(private _client: ClientService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this._client.getRouteNo().subscribe((res: any) => {
      console.log(res)
      this.routenumbers = res.body;
    });
  }
  deleteRootNo(id: string) {
    console.log("ID:", id);
    this.DeleteMsg(id);
  }

  DeleteMsg(id: any) {
    Swal.fire({
      icon: "warning",
      title: "Do You really Want To Delete ?",
      showConfirmButton: true,
      confirmButtonText: "Yes",
      showCancelButton: true,
      cancelButtonColor: "#25b54a",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this._client.deleteRouteNo(id).subscribe(
          (res) => {
            var index = this.routenumbers.findIndex((c) => c.id == id);
            this.routenumbers.splice(index, 1);
            this.notify("Route No Successfully Deleted");

          },
          (error) => {

            this.notify("Sorry,Somthing Went Wrong, Please Try Again Later");

          }
        );
      }
    });
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
