import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Params, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ClientService } from '../../client.service';

@Component({
  selector: 'app-addrootno',
  templateUrl: './addrootno.component.html',
  styleUrls: ['./addrootno.component.sass']
})
export class AddrootnoComponent implements OnInit {
  req: Params;
  heading = "Add Client Route No";
  mainheading = "Add"
  subheading = "Welcome to Route No";
  icon = "pe-7s-light icon-gradient bg-info";
  btntext = "Back RouteNo";
  btnIcon = "angle-left";
  btnLink = "client/routeNumber";
  root: any = { id: 0, isActive: true };

  // cat: any = [];
  msg: string;
  constructor(
    private _client: ClientService,
    private snackBar: MatSnackBar,
    private _route: ActivatedRoute,
    private router: Router
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    // Get-Client Category's
    if (this.req.id != 0 && this.req.id != undefined) {
      console.log("am in edit ---> ");
      this.heading = "Edit Client Route No";
      this.mainheading = "Edit"
      this._client.getRouteNoById(this.req.id).subscribe((res: any) => {
        this.root = res.body;
        console.log("routeById:", res);
      });
    }
  }
  add(f) {
    if (f.valid) {
      console.log("Clent RouteNo  Data:", this.root);
      if (this.root.id <= 0 || this.root.id == undefined) {
        this._client.addRouteNo(this.root).subscribe(
          (res) => {
            this.msg = "Client Route No Added";
            this.root = { id: 0, isActive: true };
            this.notify(this.msg);
            this.router.navigate(['/client/routeNumber']);
          },
          (error) => {
            this.snackBar.open(this.msg, "", {
              duration: 5000,
            });
          }
        );
      } else {
        // update
        this._client.updateRouteNo(this.root).subscribe(
          (res) => {
            this.msg = "Client Route No Updated";
            this.notify(this.msg);
            this.router.navigate(['/client/routeNumber']);
          },
          (error) => {
            this.snackBar.open(this.msg, "", {
              duration: 5000,
            });
          }
        );
      }
    }
  }



  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

}
