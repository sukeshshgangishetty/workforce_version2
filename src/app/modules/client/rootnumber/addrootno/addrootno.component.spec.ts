import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddrootnoComponent } from './addrootno.component';

describe('AddrootnoComponent', () => {
  let component: AddrootnoComponent;
  let fixture: ComponentFixture<AddrootnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddrootnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddrootnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
