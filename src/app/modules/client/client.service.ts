import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: "root",
})
export class ClientService {
  // "target": "http://localhost:8080",
  constructor(private _http: HttpClient) { }

  get() {
    return this._http.get("/workforce/api/v1/client");
  }
  getclients(pageno: number, pagesize: number) {
    return this._http.get("/workforce/api/v1/client?" + 'pageNo=' + pageno + '&pageSize=' + pagesize);
  }
  view(id: string) {
    return this._http.get("/workforce/api/v1/client/" + id);
  }

  save(data: any) {
    if (data.id) {
      return this._http.put("/workforce/api/v1/client/" + data.id, data);
    } else {
      return this._http.post("/workforce/api/v1/client", data);
    }
  }
  saveAddress(id: string, data: any) {
    if (data.id) {
      return this._http.put("/workforce/api/v1/client/address/" + data.id, data);
    } else {
      return this._http.post("/workforce/api/v1/client/" + id + "/address", data);
    }
  }
  saveAccount(id: string, data: any) {
    if (data.id) {
      return this._http.put("/workforce/api/v1/client/accDetail/" + data.id, data);
    } else {
      return this._http.post("/workforce/api/v1/client/" + id + "/accDetail", data);
    }
  }
  saveKyc(id: string, data: any) {
    if (data.id) {
      return this._http.put("/workforce/api/v1/client/" + id + "/kycDetails", data);
    } else {
      return this._http.post("/workforce/api/v1/client/" + id + "/kycDetails", data);
    }
  }
  saveSocial(id: string, data: any) {
    if (data.id) {
      return this._http.put("/workforce/api/v1/client/" + id + "/contactDetails", data);
    } else {
      return this._http.post("/workforce/api/v1/client/" + id + "/contactDetails", data);
    }
  }
  updatePrimaryStatus(key: string, id: string) {
    return this._http.patch("/workforce/api/v1/client/" + key + "/" + id, {});
  }
  // category
  getcat() {
    return this._http.get("/workforce/api/v1/client/category");
  }
  getcatById(id: string) {
    return this._http.get("/workforce/api/v1/client/category/" + id);
  }

  addcat(data: any) {
    return this._http.post("/workforce/api/v1/client/category", data);
  }
  updatecat(data: any) {
    return this._http.put("/workforce/api/v1/client/category/" + data.id, data);
  }
  delete(id: any) {
    return this._http.delete("/workforce/api/v1/client/category/" + id);
  }
  //Route No 
  getRouteNo() {
    return this._http.get("/workforce/api/v1/client/route");
  }
  getRouteNoById(id: string) {
    return this._http.get("/workforce/api/v1/client/route/" + id);
  }
  addRouteNo(data: any) {
    return this._http.post("/workforce/api/v1/client/route", data);
  }
  updateRouteNo(data: any) {
    return this._http.put("/workforce/api/v1/client/route/" + data.id, data);
  }
  // Delete
  deleteRouteNo(id: any) {
    return this._http.delete("/workforce/api/v1/client/route/" + id);
  }
}
