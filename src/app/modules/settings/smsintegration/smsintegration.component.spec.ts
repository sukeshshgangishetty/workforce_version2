import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsintegrationComponent } from './smsintegration.component';

describe('SmsintegrationComponent', () => {
  let component: SmsintegrationComponent;
  let fixture: ComponentFixture<SmsintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
