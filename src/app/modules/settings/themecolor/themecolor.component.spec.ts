import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemecolorComponent } from './themecolor.component';

describe('ThemecolorComponent', () => {
  let component: ThemecolorComponent;
  let fixture: ComponentFixture<ThemecolorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThemecolorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemecolorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
