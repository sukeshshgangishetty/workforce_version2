import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherapisComponent } from './otherapis.component';

describe('OtherapisComponent', () => {
  let component: OtherapisComponent;
  let fixture: ComponentFixture<OtherapisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherapisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherapisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
