import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatetimeformatComponent } from './datetimeformat.component';

describe('DatetimeformatComponent', () => {
  let component: DatetimeformatComponent;
  let fixture: ComponentFixture<DatetimeformatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatetimeformatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatetimeformatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
