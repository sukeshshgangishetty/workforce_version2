import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailintegrationComponent } from './emailintegration.component';

describe('EmailintegrationComponent', () => {
  let component: EmailintegrationComponent;
  let fixture: ComponentFixture<EmailintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
