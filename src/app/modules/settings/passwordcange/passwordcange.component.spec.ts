import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordcangeComponent } from './passwordcange.component';

describe('PasswordcangeComponent', () => {
  let component: PasswordcangeComponent;
  let fixture: ComponentFixture<PasswordcangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordcangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordcangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
