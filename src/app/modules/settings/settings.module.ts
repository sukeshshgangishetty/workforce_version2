import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { CompanyprofileComponent } from './companyprofile/companyprofile.component';
import { DatetimeformatComponent } from './datetimeformat/datetimeformat.component';
import { SmsintegrationComponent } from './smsintegration/smsintegration.component';
import { EmailintegrationComponent } from './emailintegration/emailintegration.component';
import { SetingsbulkuploadComponent } from './setingsbulkupload/setingsbulkupload.component';
import { PackagedetailsComponent } from './packagedetails/packagedetails.component';
import { VersionComponent } from './version/version.component';
import { ThemecolorComponent } from './themecolor/themecolor.component';
import { PasswordcangeComponent } from './passwordcange/passwordcange.component';
import { HelpComponent } from './help/help.component';
import { OtherapisComponent } from './otherapis/otherapis.component';


@NgModule({
  declarations: [CompanyprofileComponent, DatetimeformatComponent, SmsintegrationComponent, EmailintegrationComponent, SetingsbulkuploadComponent, PackagedetailsComponent, VersionComponent, ThemecolorComponent, PasswordcangeComponent, HelpComponent, OtherapisComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule
  ]
})
export class SettingsModule { }
