import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyprofileComponent } from './companyprofile/companyprofile.component';
import { DatetimeformatComponent } from './datetimeformat/datetimeformat.component';
import { EmailintegrationComponent } from './emailintegration/emailintegration.component';
import { HelpComponent } from './help/help.component';
import { OtherapisComponent } from './otherapis/otherapis.component';
import { PackagedetailsComponent } from './packagedetails/packagedetails.component';
import { PasswordcangeComponent } from './passwordcange/passwordcange.component';
import { SetingsbulkuploadComponent } from './setingsbulkupload/setingsbulkupload.component';
import { SmsintegrationComponent } from './smsintegration/smsintegration.component';
import { ThemecolorComponent } from './themecolor/themecolor.component';
import { VersionComponent } from './version/version.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'companyprofile',
        component: CompanyprofileComponent,
      },
      {
        path: 'datetimeformat',
        component: DatetimeformatComponent,
      },
      {
        path: 'smsintegration',
        component: SmsintegrationComponent,
      },
      {
        path: 'emailintegration',
        component: EmailintegrationComponent,
      },
      {
        path: 'bulkupload',
        component: SetingsbulkuploadComponent,
      },
      {
        path: 'packagedetails',
        component: PackagedetailsComponent,
      },
      {
        path: 'version',
        component: VersionComponent,
      },
      {
        path: 'themecolor',
        component: ThemecolorComponent,
      },
      {
        path: 'passwordchange',
        component: PasswordcangeComponent,
      },
      {
        path: 'help',
        component: HelpComponent,
      },
      {
        path: 'otherapis',
        component: OtherapisComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
