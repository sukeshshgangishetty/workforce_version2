import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetingsbulkuploadComponent } from './setingsbulkupload.component';

describe('SetingsbulkuploadComponent', () => {
  let component: SetingsbulkuploadComponent;
  let fixture: ComponentFixture<SetingsbulkuploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetingsbulkuploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetingsbulkuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
