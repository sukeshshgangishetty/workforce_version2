import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsubtaskComponent } from './addsubtask.component';

describe('AddsubtaskComponent', () => {
  let component: AddsubtaskComponent;
  let fixture: ComponentFixture<AddsubtaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsubtaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsubtaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
