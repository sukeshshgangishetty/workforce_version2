import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsubtaskComponent } from './viewsubtask.component';

describe('ViewsubtaskComponent', () => {
  let component: ViewsubtaskComponent;
  let fixture: ComponentFixture<ViewsubtaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsubtaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsubtaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
