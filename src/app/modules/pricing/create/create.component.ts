import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {
  heading = 'Create Pricing Strategy';
  subheading = '';
  icon = 'fa fa-archive';
  constructor() { }

  ngOnInit() {
  }

}
