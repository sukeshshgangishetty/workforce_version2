import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PricingRoutingModule } from './pricing-routing.module';
import { StrategyComponent } from './strategy/strategy.component';
import { CreateComponent } from './create/create.component';
import { DetailsComponent } from './details/details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule,MatInputModule } from '@angular/material';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { MatCheckboxModule} from '@angular/material/checkbox'; 
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule, MatTabsModule} from '@angular/material';
import { MatChipsModule,MatAutocompleteModule} from '@angular/material'
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio'; 
import { MatTooltipModule } from '@angular/material/tooltip';
import { CalendarModule } from 'angular-calendar';
import { MatStepperModule} from '@angular/material/stepper';
import { MatProgressBarModule} from '@angular/material/progress-bar';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MatSlideToggleModule } from '@angular/material/slide-toggle'; 
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';

@NgModule({
  declarations: [StrategyComponent, CreateComponent, DetailsComponent],
  imports: [
    CommonModule,
    PricingRoutingModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatRadioModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatNativeDateModule,
    MatIconModule,
    MatDividerModule,
    MatTabsModule,
    MatSlideToggleModule,
    AngularEditorModule,
    MatProgressBarModule,
    MatStepperModule,
    CalendarModule,
    MatSelectModule,
    MatCheckboxModule,
    CommonModule,
    PageTitleModule,
    NgbModule,
    FormsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    NgBootstrapFormValidationModule,
    MatInputModule,
    AngularFontAwesomeModule,
    MatDatepickerModule
  
  ]
})
export class PricingModule { }
