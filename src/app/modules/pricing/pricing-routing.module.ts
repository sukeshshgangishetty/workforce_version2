import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { DetailsComponent } from './details/details.component';
import { StrategyComponent } from './strategy/strategy.component';


const routes: Routes = [
  {
    
    path: '',

    children:[    
      {
          
      path:'create',
      component:CreateComponent     
    },
    {
          
      path:'details',
      component:  DetailsComponent   
    },
    {
          
      path:'strategy',
      component: StrategyComponent    
    }
 
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PricingRoutingModule { }
