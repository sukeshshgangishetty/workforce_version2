import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-strategy',
  templateUrl: './strategy.component.html',
  styleUrls: ['./strategy.component.sass']
})
export class StrategyComponent implements OnInit {
  heading = 'Pricing Strategy';
  subheading = '';
  icon = 'fa fa-archive';
  constructor() { }

  ngOnInit() {
  }

}
