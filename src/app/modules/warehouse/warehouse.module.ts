import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WarehouseRoutingModule } from './warehouse-routing.module';
import { AddwarehouseComponent } from './addwarehouse/addwarehouse.component';
import { AreawisegroupComponent } from './areawisegroup/areawisegroup.component';
import { ViewwarehouseComponent } from './viewwarehouse/viewwarehouse.component';


@NgModule({
  declarations: [AddwarehouseComponent, AreawisegroupComponent, ViewwarehouseComponent],
  imports: [
    CommonModule,
    WarehouseRoutingModule
  ]
})
export class WarehouseModule { }
