import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddwarehouseComponent } from './addwarehouse/addwarehouse.component';
import { AreawisegroupComponent } from './areawisegroup/areawisegroup.component';
import { ViewwarehouseComponent } from './viewwarehouse/viewwarehouse.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'addwarehouse',
        component: AddwarehouseComponent,
      },
      {
        path: 'viewwarehouse',
        component: ViewwarehouseComponent
      },
      {
        path: 'areawisegroup',
        component: AreawisegroupComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarehouseRoutingModule { }
