import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreawisegroupComponent } from './areawisegroup.component';

describe('AreawisegroupComponent', () => {
  let component: AreawisegroupComponent;
  let fixture: ComponentFixture<AreawisegroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreawisegroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreawisegroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
