import { Injectable } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class LocalDetailsService {
    constructor() { }

    get loginDetails() {
        let workforceUser = localStorage.getItem('user') || null;
        if (workforceUser) {
            workforceUser = JSON.parse(workforceUser);
            return {
                userId: workforceUser['userid']
            }
        }
    }

    get userLoginDetails() {
        return localStorage.getItem('user') || null;
    }
}