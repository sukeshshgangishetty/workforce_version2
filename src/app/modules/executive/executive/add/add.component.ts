import { Component, OnInit } from '@angular/core';

import { Router,ActivatedRoute,Params } from '@angular/router';
import Swal from 'sweetalert2';
import { ExecutiveService } from '../../executive.service';
import { MatSnackBar } from '@angular/material';
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material/core";
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/date.adapter';
import * as moment from 'moment';
import { DateConverter } from 'src/app/helpers/date-converter';


@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.sass"],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
  ],
})
export class AddComponent implements OnInit {
  req: Params;
  heading = "Add Executive";
  subheading = "";
  icon = "fa fa-user";
  btntext = "Back to Executives";
  btnLink = "executive";
  btnIcon = "angle-left";
  today = moment().add(1, "days").toDate();
  executive: any = { address: {} };
  msg: string;

  constructor(
    private _executive: ExecutiveService,
    private _route: ActivatedRoute,
    private _date: DateConverter,
    private snackBar: MatSnackBar
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    if (this.req.id) {
      // this.heading = "Edit Executive ID#" + this.req.id;
      this._executive.view(this.req.id).subscribe((res) => {
        this.executive = res;
      });
    }
  }

  add(f: any) {
    if (f.valid) {
      this._executive.save(this.executive).subscribe(
        (res) => {
         

          this.msg = "Executive Updated";
          if (this.req.id < 1) {
            this.executive = { address: {} };
            this.msg = "Executive Added";
          }
          this.notify(this.msg);
        },
        (error) => {
          this.snackBar.open(error.error.message, "", {
            duration: 5000,
          });
        }
      );
      // console.log("Executive:", this.executive);
    }
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
