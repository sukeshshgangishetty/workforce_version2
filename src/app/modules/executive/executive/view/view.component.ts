import {
  Component,
  OnInit,
  AfterViewInit,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import * as L from "leaflet";
import "mapbox-gl-leaflet";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { Router, ActivatedRoute, Params } from "@angular/router";
import Swal from "sweetalert2";
import { ExecutiveService } from "../../executive.service";
import { MatSnackBar } from "@angular/material";
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material/core";
import { AppDateAdapter, APP_DATE_FORMATS } from "src/app/helpers/date.adapter";
import * as moment from "moment";
import { DateConverter } from "src/app/helpers/date-converter";
import { TripService } from "src/app/modules/trips/trip.service";
import { TaskService } from "src/app/modules/task/task.service";
import { TrackService } from "src/app/modules/track/track.service";

@Component({
  selector: "app-view",
  templateUrl: "./view.component.html",
  styleUrls: ["./view.component.sass"],
})
export class ViewComponent implements OnInit, AfterViewInit {
  // pagetitle Code
  @Output() public callback: EventEmitter<any> = new EventEmitter();

  req: Params;
  heading = "View Section";
  subheading = "Welcome to View Section";
  icon = "pe-7s-light icon-gradient bg-info";
  btntext = "Back to Executives";
  btnLink = "executive";
  btnIcon = "angle-left";
  today = moment().add(1, "days").toDate();
  executive: any = { address: {} };
  executives: any = [];
  msg: string;

  //trips
  trips: any = [];

  // for Map
  map: any = null;
  markers = [];

  status: any = [
    "CREATED",
    "ACTIVE",
    "REJECTED",
    "ASSIGNED",
    "SCHEDULED",
    "INPROGRESS",
    "COMPLETED",
    "ONHOLD",
    "ACCEPTENCE_PENDING",
    "CANCELLED",
  ];

  Mobileinfo: any = [
    {
      date: "12-01-2021",
      imei: 254145546,
      model: "Sony",
      manufacturer: "Sony Ent.",
      os: "8.1",
    },
    {
      date: "21-02-2021",
      imei: 254145546,
      model: "Nokia",
      manufacturer: "Nokia Pvt",
      os: "8.1",
    },
    {
      date: "23-03-2021",
      imei: 254145546,
      model: "Honor",
      manufacturer: "Honor pvt.",
      os: "9",
    },
    {
      date: "24-03-2021",
      imei: 254145546,
      model: "POCO",
      manufacturer: "POCO X2",
      os: "10",
    },
    {
      date: "30-03-2021",
      imei: 254145546,
      model: "One Plus",
      manufacturer: "Never Setel",
      os: "11",
    },
  ];

  rounds: any = [];
  schedules:any = [];
  attendance:any = [];
  exeTasks:any = [];
  exeTrack:any = [];

  // MAP
  ngAfterViewInit() {}
  mapInit() {
    if (!this.map) {
      this.map = L.map("map").setView([17.39062, 78.55883], 11);
      this.map.options.minZoom = 3;
      this.map.options.maxZoom = 20;
    }

    L.tileLayer("http://49.156.148.124/map/{z}/{x}/{y}.png").addTo(this.map);
  }

  res:any=[];
  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
  };

  constructor(
    private _executive: ExecutiveService,
    private _route: ActivatedRoute,
    private _date: DateConverter,
    private snackBar: MatSnackBar,
    private _trips: TripService,
    private _task: TaskService,
    private _track: TrackService,
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    // GetAllexecutive API
    this._executive.get().subscribe((res) => {
      this.executives = res;
    });

    // GetById
    if (this.req.id) {
      // this.heading = "View Executive ID#" + this.req.id;
      this._executive.view(this.req.id).subscribe((res: any) => {
        this.executive = res;
      });
      //Get Trips By executive ID
      this._trips.getTripsByExeId(this.req.id).subscribe((res: any) => {
        this.trips = res;

        // console.log("tripbyId:", this.trips);
      });
        // Get Attadence By Exe id attendance/employee /api/v1/exe
        this._executive.getAttByExeId(this.req.id).subscribe((res: any) => {
          this.res = res;
          this.attendance = res;

      });

       //Get Tasks By executive ID
       this._task.getTaskExeId(this.req.id).subscribe((res: any) => {
        this.exeTasks = res.body;

        // console.log("tripbyId:", this.trips);
      });

       //Get Tasks By executive ID
       this._track.getTrackExeId(this.req.id).subscribe((res: any) => {
        this.exeTrack = res.body;
      });

    }
  }

  public onUploadInit(args: any): void {
    console.log("onUploadInit:", args);
  }

  public onUploadError(args: any): void {
    console.log("onUploadError:", args);
  }

  public onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
  }

  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

  // For Map Integrations
  date = moment();
  end = moment().endOf("month").format("D");

  changeMonth(m) {
    if (m == "add") {
      this.date.add(1, "month");
    } else {
      this.date.subtract(1, "month");
    }
    this.end = this.date.endOf("month").format("D");
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: "smooth" });
  }

  tabChange(e: any) {
    console.log("e:", e);
    if (e.tab.textLabel == "Track History") {
      this.mapInit();
    }
  }
}