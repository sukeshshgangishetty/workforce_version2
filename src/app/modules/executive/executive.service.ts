import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: "root",
})
export class ExecutiveService {
  constructor(private _http: HttpClient) {}

  get() {
    return this._http.get("/workforce/api/v1/exec");
  }
  view(id: number) {
    return this._http.get("/workforce/api/v1/exe/" + id);
  }
  save(data: any) {
    if (data.exeId != 0 && data.exeId) {
      return this._http.put("/workforce/api/v1/exec/" + data.exeId, data);
    } else {
      return this._http.post("/workforce/api/v1/exe", data);
    }
  }

  getAttByExeId(id: number) {
    return this._http.get("/workforce/attendance/employee/" + id);
  }
  
  
}