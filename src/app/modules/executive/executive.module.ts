import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

import { ExecutiveRoutingModule } from './executive-routing.module';
import { ExecutiveComponent } from './executive/executive.component';
import { AddComponent } from './executive/add/add.component';
import { PageTitleModule} from '../../Layout/Components/page-title/page-title.module';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FiltersModule } from 'src/app/components/filters/filters.module';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { MatAutocompleteModule, MatButtonModule, MatCardModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatDividerModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatPaginatorModule, MatRadioModule, MatSelectModule, MatSlideToggleModule, MatSnackBarModule, MatStepperModule, MatTabsModule, MatToolbarModule, MatTooltipModule } from '@angular/material';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { DropzoneConfigInterface, DropzoneModule, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { TrendModule } from 'ngx-trend';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ViewComponent } from './executive/view/view.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CalendarModule } from 'angular-calendar';
import { AgmCoreModule } from '@agm/core';
import {
  PerfectScrollbarConfigInterface,
  PERFECT_SCROLLBAR_CONFIG,
} from "ngx-perfect-scrollbar";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";


import { CommonModule as Common } from '../common/common.module';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: "https://httpbin.org/post",
  maxFilesize: 50,
  acceptedFiles: "image/*",
};

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [
    ExecutiveComponent,
    AddComponent,
    ViewComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    AngularFontAwesomeModule,
    CommonModule,
    ExecutiveRoutingModule,
    PageTitleModule,
    FormsModule,
    ReactiveFormsModule,
    FiltersModule,
    NgxDaterangepickerMd.forRoot(),
    CommonModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    // NgImageSliderModule,
    MatSnackBarModule,
    NgBootstrapFormValidationModule,
    MatSelectModule,
    MatRadioModule,
    SlickCarouselModule,
    MatPaginatorModule,
    MatStepperModule,
    MatIconModule,
    DropzoneModule,
    TrendModule,
    RoundProgressModule,
    NgApexchartsModule,
    AngularFontAwesomeModule,
    MatTooltipModule,
    MatTabsModule,
    MatInputModule,
    NgbModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatIconModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatRadioModule,
    AngularFontAwesomeModule,
    MatStepperModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    DropzoneModule,
    NgApexchartsModule,
    AngularEditorModule,
    CalendarModule,
    MatTooltipModule,
    MatCardModule,
    MatSnackBarModule,
    SlickCarouselModule,
    AgmCoreModule,
    MatExpansionModule,
    PerfectScrollbarModule,
    Common
  ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG,
    },
  ],
})
export class ExecutiveModule {}
