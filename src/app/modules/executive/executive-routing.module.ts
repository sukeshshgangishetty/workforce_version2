import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExecutiveComponent } from './executive/executive.component';
import { AddComponent as AddExecutive  } from './executive/add/add.component';
import { ViewComponent as ViewExecutive  } from './executive/view/view.component';


  const routes: Routes = [
    {
      path: "",

      children: [
        {
          path: "view/:id",
          component: ViewExecutive,
        },
        {
          path: ":id",
          component: AddExecutive,
        },
        {
          path: "",
          component: ExecutiveComponent,
        },
      ],
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExecutiveRoutingModule { }
