import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TargetComponent } from '../sales/targets/target.component';
import { AddleadsComponent } from './addleads/addleads.component';
import { AddschemesComponent } from './addschemes/addschemes.component';
import { AddtargetComponent } from './addtarget/addtarget.component';
import { AreawiseComponent } from './areawise/areawise.component';
import { ConverttoclientComponent } from './converttoclient/converttoclient.component';
import { CustomformComponent } from './customform/customform.component';
import { NavigatetargetComponent } from './navigatetarget/navigatetarget.component';
import { ViewleadsComponent } from './viewleads/viewleads.component';
import { ViewschemesComponent } from './viewschemes/viewschemes.component';
import { ViewtargetComponent } from './viewtarget/viewtarget.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'addleads',
        component: AddleadsComponent,
      },
      {
        path: 'viewleads',
        component: ViewleadsComponent,
      },
      {
        path: 'converttoclient',
        component: ConverttoclientComponent,
      },
      {
        path: 'customform',
        component: CustomformComponent,
      },
      {
        path: 'addtarget',
        component: AddtargetComponent,
      },
      {
        path: 'viewtarget',
        component: TargetComponent,
      },
      {
        path: 'navigatetarget',
        component: NavigatetargetComponent,
      },
      {
        path: 'addschemes',
        component: AddschemesComponent,
      },
      {
        path: 'viewschemes',
        component: ViewschemesComponent,
      },
      {
        path: 'areawise',
        component: AreawiseComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketingRoutingModule { }
