import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesModule } from '../sales/sales.module'
import { MarketingRoutingModule } from './marketing-routing.module';
import { AddleadsComponent } from './addleads/addleads.component';
import { ViewleadsComponent } from './viewleads/viewleads.component';
import { ConverttoclientComponent } from './converttoclient/converttoclient.component';
import { CustomformComponent } from './customform/customform.component';
import { AddtargetComponent } from './addtarget/addtarget.component';
import { ViewtargetComponent } from './viewtarget/viewtarget.component';
import { NavigatetargetComponent } from './navigatetarget/navigatetarget.component';
import { AddschemesComponent } from './addschemes/addschemes.component';
import { ViewschemesComponent } from './viewschemes/viewschemes.component';
import { AreawiseComponent } from './areawise/areawise.component';


@NgModule({
  declarations: [AddleadsComponent, ViewleadsComponent, ConverttoclientComponent, CustomformComponent, AddtargetComponent, ViewtargetComponent, NavigatetargetComponent, AddschemesComponent, ViewschemesComponent, AreawiseComponent],
  imports: [
    CommonModule,
    MarketingRoutingModule,
    SalesModule
  ]
})
export class MarketingModule { }
