import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportRoutingModule } from './report-routing.module';
import { SalesComponent } from './sales/sales.component';
// import { ApexchartsModule } from '../../DemoPages/Charts/apexcharts/apexcharts.module';
import { NgApexchartsModule } from 'ng-apexcharts';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

import {MatCheckboxModule} from '@angular/material/checkbox'; 
import { TaskComponent } from './task/task.component';
import { PaymentComponent } from './payment/payment.component';
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule, MatTabsModule} from '@angular/material';
import { MatChipsModule,MatAutocompleteModule} from '@angular/material'
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio'; 
import { MatTooltipModule } from '@angular/material/tooltip';
import { CalendarModule } from 'angular-calendar';
import { MatStepperModule} from '@angular/material/stepper';
import { MatProgressBarModule} from '@angular/material/progress-bar';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MatSlideToggleModule} from '@angular/material/slide-toggle'; 
@NgModule({
  declarations: [SalesComponent, TaskComponent, PaymentComponent],
  imports: [
    MatChipsModule,MatAutocompleteModule, MatPaginatorModule, MatTooltipModule, MatRadioModule,
MatFormFieldModule,
ReactiveFormsModule,
 MatButtonModule,
  MatNativeDateModule,
  MatIconModule,
  MatDividerModule,
   MatTabsModule,
    MatSlideToggleModule,
     AngularEditorModule,
      MatProgressBarModule,
       MatStepperModule,
    CalendarModule,
    MatSelectModule,
    CommonModule,
    ReportRoutingModule,
    PageTitleModule,
    NgbModule,
    NgApexchartsModule,
    FormsModule,
    MatCheckboxModule,
    AngularFontAwesomeModule,
    RoundProgressModule
  ]
})
export class ReportModule { }
