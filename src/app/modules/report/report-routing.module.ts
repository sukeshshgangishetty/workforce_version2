import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentComponent } from './payment/payment.component';
import { SalesComponent } from './sales/sales.component';
import { TaskComponent } from './task/task.component';



const routes: Routes = [
  {
    
    path: '',

    children:[
      
     
      {
        path:'sales',
        component:SalesComponent,
      },
      {
        path:'payment',
        component:PaymentComponent,
      },
      {
        path:'task',
        component:TaskComponent,
      },
      {
        path:'',
        redirectTo:'sales',
        pathMatch:'full',
      }
    ]

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
