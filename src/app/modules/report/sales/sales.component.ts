import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.sass']
})
export class SalesComponent implements OnInit {
  heading = 'Sales Reports';
  subheading = 'Welcome to Reports Section';
  icon = 'fa fa-line-chart';
  btnstatus = false;

  constructor() { }

  ngOnInit() {
  }

}
