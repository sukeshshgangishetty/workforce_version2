import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  constructor(private _http: HttpClient) { }

  get() {
    return this._http.get("/workforce/api/v1/offer");
  }
  view(id: number) {
    return this._http.get("/workforce/api/v1/offer/" + id);
  }
  save(data: any) {
    if (data.id  != 0 && data.id ) {
      return this._http.put("/workforce/api/v1/offer/" + data.id , data);
    } else {
      return this._http.post("/workforce/api/v1/offer", data);
    }
  }

  saveClient(data: any) {
    return this._http.post("/workforce/api/v1/offer/" + data.id , data);



  }


}
