import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfferComponent } from './offer/offer.component';
import { AddComponent as AddOffer } from './offer/add/add.component';
import { ViewComponent as ViewOffer } from './offer/view/view.component';


const routes: Routes = [
  {
    path: "",

    children: [
      {
        path: "view/:id",
        component: ViewOffer,
      },
      {
        path: ":id",
        component: AddOffer,
      },
      {
        path: "",
        component: OfferComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
