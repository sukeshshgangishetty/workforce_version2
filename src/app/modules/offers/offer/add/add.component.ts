import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { OffersService } from '../../offers.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ClientService } from 'src/app/modules/client/client.service';
import { ProductService } from 'src/app/modules/product/product.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  req: Params;
  heading = "Add Offer";
  subheading = "";
  icon = "pe-7s-plus icon-gradient bg-info";
  btntext = "Back to Offers";
  btnLink = "offers";
  btnIcon = "angle-left";
  name: string;
  msg: string;

  offer:any = {
    
    detail:{},
    startDate1: { startDate: moment(), endDate: moment() },
    startDate: moment().format("DD-MM-yyyy HH:mm:ss"),
    endDate1: { startDate: moment(), endDate: moment() },
    endDate: moment().format("DD-MM-yyyy HH:mm:ss"),
  };
  
  offerclient:any = {};

  // chips
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  groupCtrl = new FormControl();
  filteredgroups: Observable<string[]>;
  
  groups: string[] = [];
  allgroups: string[] = ['Apple', 'Lemon', 'Lime', 'Orange', 'Strawberry'];

  clientCategories:any = [];
  clients:any = [];

  @ViewChild('groupInput', {static: false}) groupInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
 

  constructor(
    private _offersService: OffersService,
    private _route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _client: ClientService,
    private _product: ProductService
    ) {
      this.req = this._route.snapshot.params;
    this.filteredgroups = this.groupCtrl.valueChanges.pipe(
        startWith(null),
        map((group: string | null) => group ? this._filter(group) : this.allgroups.slice()));
  }

  add(event: MatChipInputEvent): void {
    // Add group only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our group
      if ((value || '').trim()) {
        this.groups.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.groupCtrl.setValue(null);
    }
  }

  remove(group: string): void {
    const index = this.groups.indexOf(group);

    if (index >= 0) {
      this.groups.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.groups.push(event.option.viewValue);
    this.groupInput.nativeElement.value = '';
    this.groupCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allgroups.filter(group => group.toLowerCase().indexOf(filterValue) === 0);
  }
  // chips


  offerTypes=["ORDER_VALUE", "PRODUCT"];
  products=["PRODUCT 1","PRODUCT 2","PRODUCT 3","PRODUCT 4","PRODUCT 5","PRODUCT 6"];
  offerCategorys = [ "I", "II", "III", "IV", "V", "VI"];
  
  ngOnInit() {
     // GetById
     if (this.req.id) {
      this.heading = "View/Edit offer ";
      this._offersService.view(this.req.id).subscribe((res: any) => {
        this.offer = res.body;
        console.log("StartDate",this.offer.startDate);
        this.offer.startDate1 = moment(this.offer.startDate).format("DD-MM-yyyy HH:mm:ss");
        console.log("StartDate 1",this.offer.startDate1);

        this.offer.endDate1 = moment(this.offer.endDate).format("DD-MM-yyyy HH:mm:ss");
        console.log("endDate 1",this.offer.endDate1);

      });
  }

  //GET ALL CLIENTS
  this._client.get().subscribe((res: any)=>{
    this.clients = res.body;
    console.log("Clients:",this.clients);
});

  //GET ALL CLIENTS CATEGORIES

   this._client.getcat().subscribe((res: any) => {
    this.clientCategories = res.body;
    console.log(this.clientCategories);
  });

   //GET ALL PRODUCTS

   this._product.get().subscribe((res: any) => {
    this.products = res.body;
  });


  } //ngOnInit




// End Of Chips

dataClear(event){
  // console.log("data clear:",event);
  this.offer.detail={};
}

clientdataClear1(){
  this.offerclient.clientIds=[];
  this.offerclient.clientCatIds=[];
  this.offerclient.proDetIds= [];
}

clientdataClear2(){
  this.offerclient.clientIds=[];
  this.offerclient.proDetIds= [];
  this.offerclient.isAllClients = false;

}

clientdataClear3(){
  this.offerclient.clientCatIds=[];
  this.offerclient.proDetIds= [];
  this.offerclient.isAllClients = false;

}



clientdataClear4(){
  this.offerclient.clientIds=[];
  this.offerclient.clientCatIds=[];
  this.offerclient.isAllClients = false;



}

  addDiscounts(f: any){
    console.log("Offers Data :",f.value);

    // console.log("UNIX : " , f.value.startDate = moment(f.value.startDate).unix());


    if (f.valid) {
      this._offersService.save(this.offer).subscribe(
        (res) => {
         

          this.msg = "Offer Updated";
          if (this.req.id < 1) {
            this.offer.detail={};
            this.msg = "Offer Added";
          }
          this.notify(this.msg);
        },
        (error) => {
          this.snackBar.open(error.error.message, "", {
            duration: 5000,
          });
        }
      );
    }
  }


  clientDiscounts(f:any){
    console.log("clientDiscounts:",this.offerclient);
    if(this.offerclient.isAllClients == 2 || this.offerclient.isAllClients == 3 || this.offerclient.isAllClients == 4 ){
      this.offerclient.isAllClients = false;
    }
    if (f.valid) {
      console.log("clientDiscounts:",this.offerclient);
      this.offerclient.id = this.req.id;

      this._offersService.saveClient(this.offerclient).subscribe(
        (res) => {
            this.msg = "Offer Assigned to Client";
          this.notify(this.msg);
        },
        (error) => {
          this.snackBar.open(error.error.message, "", {
            duration: 5000,
          });
      }
      );
    }

  }




  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

  
}
