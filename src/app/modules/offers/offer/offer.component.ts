import { Component, OnInit } from '@angular/core';
import { OffersService } from '../offers.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.sass']
})
export class OfferComponent implements OnInit {
  heading = 'Offers';
  subheading = 'Welcome to Offers Section';
  icon = 'pe-7s-ticket icon-gradient bg-info';
  btntext = "Add Offer";
  btnLink = "offers/0";
  offers:any = [];
  constructor(private _offersService: OffersService) { }

  ngOnInit() {
    this._offersService.get().subscribe((res: any) => {
      this.offers = res.body;

     this.offers.forEach(offer => {
       let date = new Date(offer.endDate);
       let currentDate = new Date();
       let days = Math.floor((currentDate.getTime() - date.getTime()) / 1000 / 60 / 60 / 24);
      offer.days = days;
       
     });

    });
  }
}

