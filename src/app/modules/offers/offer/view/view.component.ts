import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { OffersService } from '../../offers.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.sass']
})
export class ViewComponent implements OnInit {
  req: Params;
  heading = "Offer";
  subheading = "Welcome to Offer Section";
  icon = "pe-7s-ticket icon-gradient bg-info";
  btntext = "Back to Offers";
  btnLink = "offers";
  btnIcon = "angle-left";
  offerById:any = [];
  constructor(
    private _offersService: OffersService,
    private _route: ActivatedRoute,
    ) { 
    this.req = this._route.snapshot.params;

  }

  ngOnInit() {
    // GetById
    if (this.req.id) {
     console.log(this.req.id);

      this.heading = "View offer ID#";
      this._offersService.view(this.req.id).subscribe((res: any) => {
        this.offerById = res.body;
      });
  }

}


}
