import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffersRoutingModule } from './offers-routing.module';
import { OfferComponent } from './offer/offer.component';
import { AddComponent } from './offer/add/add.component';
import { ViewComponent } from './offer/view/view.component';
import { MatAutocompleteModule, MatButtonModule, MatCheckboxModule, MatChipsModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatRadioModule, MatSelectModule, MatSlideToggleModule, MatStepperModule, MatTabsModule, MatTooltipModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CalendarModule } from 'angular-calendar';
import { PageTitleModule } from 'src/app/Layout/Components/page-title/page-title.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';


@NgModule({
  declarations: [OfferComponent, AddComponent, ViewComponent],
  imports: [
    NgxDaterangepickerMd.forRoot(),
    CommonModule,
    OffersRoutingModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatNativeDateModule,
    MatIconModule,
    MatDividerModule,
    MatTabsModule,
    MatSlideToggleModule,
    AngularEditorModule,
    MatProgressBarModule,
    MatStepperModule,
    CalendarModule,
    MatSelectModule,
    MatCheckboxModule,
    PageTitleModule,
    AngularFontAwesomeModule,
    FormsModule
  ]
})
export class OffersModule { }
