import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AttendanceComponent } from './attendance/attendance.component';
import { ExpenditureComponent } from './expenditure/expenditure.component';
import { HrafullaccessComponent } from './hrafullaccess/hrafullaccess.component';
import { LeaveComponent } from './leave/leave.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'attendance',
        component: AttendanceComponent,
      },
      {
        path: 'expenditure',
        component: ExpenditureComponent,
      },
      {
        path: 'leave',
        component: LeaveComponent,
      },
      {
        path: 'hrafullaccess',
        component: HrafullaccessComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HraRoutingModule { }
