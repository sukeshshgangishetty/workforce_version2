import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HraRoutingModule } from './hra-routing.module';
import { AttendanceComponent } from './attendance/attendance.component';
import { ExpenditureComponent } from './expenditure/expenditure.component';
import { LeaveComponent } from './leave/leave.component';
import { HrafullaccessComponent } from './hrafullaccess/hrafullaccess.component';


@NgModule({
  declarations: [AttendanceComponent, ExpenditureComponent, LeaveComponent, HrafullaccessComponent],
  imports: [
    CommonModule,
    HraRoutingModule
  ]
})
export class HraModule { }
