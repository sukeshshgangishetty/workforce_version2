import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ForgetPasswordService {
  constructor(private http: HttpClient) {}

  otp(data) {
    const body = { phone: data.contactphone, prps: 2 };
    return this.http.post<any>(
      "/workforce/otp?phone=" + body.phone + "&prps=2",
      {}
    );
  }
  validateOTP(data) {
    return this.http.post<any>(
      "/workforce/validateOtp?phone=" + data.contactphone + "&otp=" + data.otp,
      {}
    );
  }
  reset(data) {
    return this.http.post<any>("/workforce/reset", data);
  }
}
