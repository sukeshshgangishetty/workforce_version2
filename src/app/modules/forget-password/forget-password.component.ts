import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatStepper } from "@angular/material/stepper";
import { MAT_STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { ForgetPasswordService } from "./forget-password.service";

@Component({
  selector: "app-forget-password",
  templateUrl: "./forget-password.component.html",
  styleUrls: ["./forget-password.component.sass"],
})
export class ForgetPasswordComponent implements OnInit {
  isLinear = true;
  forget = { otp: "" };
  slideConfig2 = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "0",
    slidesToShow: 1,
    speed: 500,
    dots: false,
  };

  otpedt = null;

  constructor(
    private _formBuilder: FormBuilder,
    private _reset: ForgetPasswordService
  ) {}

  ngOnInit() {}

  getOtp(f: any, stepper: MatStepper) {
    if (f.valid) {
      let formData = this.forget;
      this._reset.otp(formData).subscribe((data) => {
        // stepper.next();
        // if(data.status==200){
        //   stepper.next();
        // }
      });
      stepper.next();
    }
  }

  goBack(stepper: MatStepper) {
    this.forget.otp = "";
    stepper.previous();
  }

  validateOTP(f: any, stepper: MatStepper) {
    if (f.valid) {
      let formData = this.forget;
      this._reset.validateOTP(formData).subscribe((res) => {
        if (res.status === 200) {
          this._reset.reset(formData).subscribe((resp) => {
            stepper.next();
          });
        }
      });
    }
  }
}
