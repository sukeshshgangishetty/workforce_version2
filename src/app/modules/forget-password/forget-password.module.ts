import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgetPasswordRoutingModule } from './forget-password-routing.module';
import { CountdownModule } from '@ciri/ngx-countdown';
import { ForgetPasswordComponent } from './forget-password.component';


@NgModule({
  declarations: [ForgetPasswordComponent ],
  imports: [
    CommonModule,
    ForgetPasswordRoutingModule,
    
    CountdownModule
  ]
})
export class ForgetPasswordModule { }
