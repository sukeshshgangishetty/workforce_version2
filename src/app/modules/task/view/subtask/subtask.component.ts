import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { TaskService } from '../../task.service';
@Component({
  selector: 'app-subtask',
  templateUrl: './subtask.component.html',
  styleUrls: ['./subtask.component.sass']
})
export class SubtaskComponent implements OnInit {
  subTask = {attachments:[],client:{}};

  constructor(private snackBar : MatSnackBar,private _subtask:TaskService,
    public dialogRef: MatDialogRef<SubtaskComponent>,
         @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {    
                   // this.task = {data};
                   console.log(data)
  }

  ngOnInit() {

    this._subtask.getSubtask(this.data.tid,this.data.stid).subscribe(res=>{
      this.subTask = res.data;
      console.log(res)
    });
  }
  

}
