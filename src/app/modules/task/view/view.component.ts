import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { ViewsubtaskComponent } from './viewsubtask/viewsubtask.component';
import { AddsubtaskComponent } from './addsubtask/addsubtask.component';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.sass']
})
export class ViewComponent implements OnInit {

  heading = 'View Task';
  subheading = '';
  icon = 'pe-7s-user icon-gradient btn-primary';
  btntext = 'Back to Tasks';
  btnLink = 'task';
  btnIcon = 'angle-left';
  req: any;
  data = {
    tid: 0,
    client: {},
    attachments:[]
  };
  subtasks = {};
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  slides = [
    { img: '1' },
    { img: '2' },
    { img: '3' },
    

  ];
  slideConfig = {
    slidesToShow: 1,
    dots: true,
  };


  constructor( public dialog: MatDialog) {
  }
openDialog(){
  const dialogConfig= new MatDialogConfig();
  dialogConfig.autoFocus = true;
  dialogConfig.width="50%"; 
  dialogConfig.height="90%"; 
  this.dialog.open(ViewsubtaskComponent,dialogConfig);
}
openDialog1(){
  const dialogConfig= new MatDialogConfig();
  dialogConfig.autoFocus = true;
  dialogConfig.width="50%"; 
  this.dialog.open(AddsubtaskComponent,dialogConfig);
}

  ngOnInit() {
 
  }


}
