import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-addsubtask",
  templateUrl: "./addsubtask.component.html",
  styleUrls: ["./addsubtask.component.sass"],
})
export class AddsubtaskComponent implements OnInit {
  subtask: any = {};

  constructor() {}

  ngOnInit() {}
  add(f: any) {
    console.log("data:", this.subtask);
  }
  clear() {
    this.subtask = {};
  }

  //DropZone
  public onUploadInit(args: any): void {
    console.log("onUploadInit:", args);
  }

  public onUploadError(args: any): void {
    console.log("onUploadError:", args);
  }

  public onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
  }
}
