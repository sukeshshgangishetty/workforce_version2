import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-viewsubtask',
  templateUrl: './viewsubtask.component.html',
  styleUrls: ['./viewsubtask.component.sass']
})
export class ViewsubtaskComponent implements OnInit {

  constructor() { }
  swalConfirm() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-primary',
        cancelButton: 'btn btn-link'
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: 'Are you sure ? You want to Assign task id: NT00010 to Excutive: Abhinay , ID: #Niit3030',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Assign',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons.fire(
          'Assigned Task!',
          'Your Task has been Assigned! task id: NT00010 to Excutive: Abhinay , ID: #Niit3030',
          'success'
        );
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your Task is Canceled',
          'error'
        );
      }
    });
  }
  ngOnInit() {
  }

}
