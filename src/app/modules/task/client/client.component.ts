import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.sass']
})
export class ClientComponent implements OnInit {
  heading = 'Client';
  subheading = '';
  icon = 'pe-7s-user icon-gradient btn-primary';
  btntext = 'Back to Tasks';
  btnLink = 'task';
  btnIcon = 'angle-left';
  req: any;
  data = {
    tid: 0,
    client: {},
    attachments:[]
  };
  subtasks = {};
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  slides = [
    { img: '1' },
    { img: '2' },
    { img: '3' },
    

  ];
  slideConfig = {
    slidesToShow: 1,
    dots: true,
  };


  constructor() {
    
  }
  


  ngOnInit() {   
  }


}
