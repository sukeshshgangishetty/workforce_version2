import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }
  
  get(){
    return this.http.get<any>('/workforce/api/v1/tasks');
  }
  view(id){
    return this.http.get<any>('/workforce/api/v1/task/'+id);
  }

  save(data){
    return this.http.post<any>('/workforce/api/v1/task',data);
  }

  subtask(tid){
    return this.http.get<any>('/workforce/api/v1/task/'+tid+'/sub');
  }
  addsubtask(tid,data){
    return this.http.post<any>('/workforce/api/v1/task/'+tid+'/sub',data);
  }
  getSubtask(tid,stid){
    return this.http.get<any>('/workforce/api/v1/task/'+tid+'/sub/'+stid);
  }

  getTaskExeId(tid){
    return this.http.get<any>('/workforce/api/v1/exe/'+tid+'/tasks');
  }

}
