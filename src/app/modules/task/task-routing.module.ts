import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './client/client.component';
import { AddComponent } from './add/add.component';
import { TasksComponent } from './tasks/tasks.component';
import { ViewComponent } from './view/view.component';
import { TaskstatusComponent } from './taskstatus/taskstatus.component';


const routes: Routes = [
  {
    path: "",

    children: [
      {
        path: "client",
        component: ClientComponent,
      },
      {
        path: "view",
        component: ViewComponent,
      },
      {
        path: "addtask",
        component: AddComponent,
      },
      {
        path: "taskstatus",
        component: TaskstatusComponent,
      },
      {
        path: "",
        component: TasksComponent,
      },
      {
        path: ":id",
        component: AddComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
