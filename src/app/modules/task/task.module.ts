import { NgModule, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NgImageSliderModule } from 'ng-image-slider';
import {MatCardModule, MatDatepickerModule,MatError,MatFormFieldModule, MatIconModule, MatPaginatorModule, MatStepperModule} from '@angular/material'; 
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import { TaskRoutingModule } from './task-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule,MatSnackBarModule} from '@angular/material'; //
import { MatRadioModule, MatInputModule, MatDialogModule} from '@angular/material';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { ViewerComponent } from 'src/app/components/viewer/viewer.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { DropzoneConfigInterface, DropzoneModule } from 'ngx-dropzone-wrapper';
import { TrendModule } from 'ngx-trend';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { TasksComponent } from './tasks/tasks.component';
import { ClientComponent } from './client/client.component';
import { AddsubtaskComponent } from './view/addsubtask/addsubtask.component';
import { ViewsubtaskComponent } from './view/viewsubtask/viewsubtask.component';
import { AddComponent } from './add/add.component';
import { TaskstatusComponent } from './taskstatus/taskstatus.component';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: "https://httpbin.org/post",
  maxFilesize: 50,
  acceptedFiles: "image/*",
};

@NgModule({
  declarations: [ViewsubtaskComponent,AddsubtaskComponent, ViewerComponent, ViewComponent, AddComponent, TasksComponent, ClientComponent, TaskstatusComponent],
  imports: [
    NgxDaterangepickerMd.forRoot(),
    CommonModule,
    TaskRoutingModule,
    PageTitleModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule,
    MatDialogModule,
   AngularFontAwesomeModule,
    // NgImageSliderModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
    NgBootstrapFormValidationModule,
    MatSelectModule,
    MatRadioModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    MatDialogModule,
    SlickCarouselModule,
    MatPaginatorModule,
    MatStepperModule,
    MatIconModule,
    MatCardModule,
    DropzoneModule,
    TrendModule,
    RoundProgressModule,
    NgApexchartsModule,
     AngularFontAwesomeModule,
   
  ],
  entryComponents: [
    ViewsubtaskComponent,
    AddsubtaskComponent
  ]
})
export class TaskModule { 
  constructor(){
  }
}
