import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TaskService } from '../task.service'; 
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {
  heading = 'Tasks';
  subheading = '';
  icon = 'fa fa-archive';
  btntext = 'Add Task';
  btnIcon = 'plus';
  btnLink = 'task/0';
  tasks = [] ;
  constructor(private _task: TaskService,private _route: Router) { }

  ngOnInit() {
    this._task.get().subscribe(data =>{
      this.tasks = data.data;
    });
  }

  view(id){
    this._route.navigate(['task/'+id+'/view']);
  }

}
