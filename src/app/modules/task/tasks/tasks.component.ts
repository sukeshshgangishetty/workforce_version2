import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.sass']
})
export class TasksComponent implements OnInit {  
  heading = 'Tasks';
  subheading = 'Welcome To Task Section';
  icon = 'pe-7s-user icon-gradient btn-primary';
  btntext = 'Add Task';
  btnIcon = 'plus';
  btnLink = 'task/0';
  viewType: string = 'list';
  tasks = [] ;
  clients : any;
  task: {};


  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  slides = [
    { img: '1' },
    { img: '2' },
    { img: '3' },
    
];
  slideConfig = {
    slidesToShow: 1,
    dots: true,
    'arrows': false,
  };
// headcards


  slidecards = [
    { 
      "title": "All Task",
      "count": "30"
    },
    { 
      "title": "Assigned",
      "count": "09"
    },
    { 
      "title": "Scheduled",
      "count": "10"
    },
    { 
      "title": "Inprogress",
      "count": "05"
    },
    { 
      "title": "On Hold",
      "count": "04"
    },
    { 
      "title": "Rejected",
      "count": "03"
    }
    

  ];
  slideConfigs = {
    slidesToShow: 3,
    dots: true,
    'arrows': false,
  };
  constructor(private _task:TaskService) { }
  ngOnInit() {
      this._task.get().subscribe((res: any) => {
        this.task = res.body;

        console.log(this.task);
      });
  }

 
  foods=[
    {
      "id":1,
      "name":"demo 1"
    },
    {
      "id":2,
      "name":"demo 2"
    },
    {
      "id":3,
      "name":"demo 3"
    }
  ]

  client = [
    {
     "id":"NT00010",
     "date":"28 aug, 2020",
     "storename":"Collection of Cheque",
     "description":"Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis quis, libero quam",
       "contactname":"Hindustan Oil",
       "contactaddress":"19-28, malakpet, Secunderabad ",
       "profit":"GAIN",
       "class":"text-success",
     "outstanding":"+1,23,50.00",
     "status":"Assigned",
     "phone":8458745874
    },
    {
     "id":"NT00011",
     "date":"25 dec, 2020",
     "storename":" Collection of Cheque",
     "description":"Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis quis, libero quam",
     
       "contactname":"Hindustan Oil",
       "contactaddress":"19-28, malakpet, Secunderabad ",
     
     "executive":"Abhinay+2",
     "priority":"HIGH",
     "class":"text-danger",
     "status":"Approved",
     "phone":8458745874
    },
    {
     "id":"NT00012",
     "date":"12 nov, 2020",
     "storename":"Collection of Cheque",
     "description":"Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis quis, libero quam",
    
       "contactname":"Hindustan Oil",
       "contactaddress":"19-28, malakpet, Secunderabad ",
       "profit":"GAIN",
       "class":"text-success",
     "outstanding":"+1,23,50.00",
     "status":"Rejected",
     "phone":8458745874
     
    }
  ]

  
	
	
	
	
	
	viewTypeChange() {
    if( this.viewType == 'list'){
      this.viewType = 'card';
    }else{
      this.viewType = 'list';
    }
    
	}


}
