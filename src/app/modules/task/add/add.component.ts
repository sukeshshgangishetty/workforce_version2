import { Component, OnInit ,Inject, Injectable} from '@angular/core';
import Swal from 'sweetalert2';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import {FormControl,Validator} from '@angular/forms';
import { ClientService } from '../../client/client.service';
import { ExecutiveService } from '../../executive/executive.service';
import { TaskService } from '../task.service';
import { Router,ActivatedRoute,Params } from '@angular/router';
import * as moment from 'moment';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SubtaskComponent } from './subtask/subtask.component';
@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.sass"],
})
@Injectable()
export class AddComponent implements OnInit {
  heading = "Add Task";
  subheading = "";
  icon = "fa fa-plus";
  btntext = "Back to Task";
  btnLink = "task";
  btnIcon = "angle-left";
  msg: string;
  task = {
    tid: 0,
    clientId: 0,
    taskTo: "clients",
    exeIds: [],
    reminderTime: moment().format("DD-MM-yyyy HH:mm:ss"),
    reminderTime1: { startDate: moment(), endDate: moment() },
    expstartDate1: { startDate: moment(), endDate: moment() },
    expstartDate: moment().format("DD-MM-yyyy HH:mm:ss"),
    expEndDate1: { startDate: moment(), endDate: moment() },
    expEndDate: moment().format("DD-MM-yyyy HH:mm:ss"),
  };
  // clients: any = [
  //   {
  //     accStatus: "NO_ACCOUNT",
  //     addedBy: {
  //       email: "string",
  //       entityKey: 0,
  //       langKey: "string",
  //       lastName: "string",
  //       phone: "string",
  //       reportingManager: true,
  //       role: "USER_PARENT",
  //       userid: 0,
  //       username: "string",
  //     },
  //     assignedTo: 0,
  //     clientname: "string",
  //     companyName: "Dhanush",
  //     gstNo: "string",
  //     id: 1,
  //     image: "string",
  //     incorporatedAt: "2021-01-13T07:11:43.211Z",
  //     location: {
  //       address: "string",
  //       lat: 0,
  //       lng: 0,
  //     },
  //     mail: "string",
  //     officeNumber: "string",
  //     openingBalance: 0,
  //     phone: "string",
  //     remarks: "string",
  //     updateState: 0,
  //   },
  //   {
  //     accStatus: "NO_ACCOUNT",
  //     addedBy: {
  //       email: "string",
  //       entityKey: 0,
  //       langKey: "string",
  //       lastName: "string",
  //       phone: "string",
  //       reportingManager: true,
  //       role: "USER_PARENT",
  //       userid: 0,
  //       username: "string",
  //     },
  //     assignedTo: 0,
  //     clientname: "VeraBabu",
  //     companyName: "string",
  //     gstNo: "string",
  //     id: 2,
  //     image: "string",
  //     incorporatedAt: "2021-01-13T07:11:43.211Z",
  //     location: {
  //       address: "string",
  //       lat: 0,
  //       lng: 0,
  //     },
  //     mail: "string",
  //     officeNumber: "string",
  //     openingBalance: 0,
  //     phone: "string",
  //     remarks: "string",
  //     updateState: 0,
  //   }
  // ];
  req: any;
 
  // accessibility = 0;
  executives = [];
  clients = [];
  subtasks: any;

  constructor(
    private snackBar: MatSnackBar,
    private _task: TaskService,
    private _client: ClientService,
    private _route: ActivatedRoute,
    public dialog: MatDialog,
    private _exec: ExecutiveService,
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {

    this._exec.get().subscribe((res: any) => {
      this.executives = res.body;
      console.log("Executives:", this.executives);
    });

    this._client.get().subscribe((res: any)=>{
      this.clients = res.body;
      console.log("Clients:",this.clients);
  });

    // this._task.get().subscribe(res=>{
    //     this.task = res;
    // });
    // this._executive.sam().subscribe((res) => {
    //   this.clients = res;
    // });
    // this._executive.sam().subscribe((res) => {
    //   this.executives = res;
    // });
    if (this.req.id > 0) {
      this.get(this.req.id);
    }
  }
  get(id) {
    this.heading = "Edit Task ID#" + id;
    this._task.view(id).subscribe((res) => {
      this.task = res.data;
      this.task.expstartDate1 = { startDate: moment(), endDate: moment() };
      this.task.expEndDate1 = { startDate: moment(), endDate: moment() };
      this.task.reminderTime1 = { startDate: moment(), endDate: moment() };
      this.task.expstartDate1.startDate = moment(
        this.task.expstartDate,
        "DD-MM-yyyy HH:mm:ss"
      );
      this.task.expstartDate1.endDate = moment(
        this.task.expstartDate,
        "DD-MM-yyyy HH:mm:ss"
      );
      this.task.expEndDate1.startDate = moment(
        this.task.expEndDate,
        "DD-MM-yyyy HH:mm:ss"
      );
      this.task.expEndDate1.endDate = moment(
        this.task.expEndDate,
        "DD-MM-yyyy HH:mm:ss"
      );
      this.task.reminderTime1.startDate = moment(
        this.task.reminderTime,
        "DD-MM-yyyy HH:mm:ss"
      );
      this.task.reminderTime1.endDate = moment(
        this.task.reminderTime,
        "DD-MM-yyyy HH:mm:ss"
      );
      console.log(this.task);
      if (this.task.clientId != null) {
        this.task.taskTo = "clients";
      } else {
        this.task.taskTo = "others";
      }
    });
    this._task.subtask(id).subscribe((res) => {
      this.subtasks = res.data;
    });
  }

  add(f) {
    if (f.valid) {
      let data = f.value;
      console.log(data);
      if (this.task.tid > 0) {
        data.tid = this.req.id;
      }
      let temp = this.task.expstartDate1.startDate.format("HH:mm:ss");
      data.expstartDate =
        this.task.expstartDate1.startDate.format("DD-MM-yyyy") + " " + temp;
      temp = this.task.expEndDate1.startDate.format("HH:mm:ss");
      data.expEndDate =
        this.task.expEndDate1.startDate.format("DD-MM-yyyy") + " " + temp;
      temp = this.task.reminderTime1.startDate.format("HH:mm:ss");
      data.reminderTime =
        this.task.reminderTime1.startDate.format("DD-MM-yyyy") + " " + temp;
      data.client = { id: data.client };

      if (this.task.taskTo != "clients") {
        data.client = null;
      }
      if (data.needReminder == false) {
        data.reminderTime = null;
      }
      data.accessibility == "PRIVATE"
        ? (data.executives = [data.executives])
        : "";
      this._task.save(data).subscribe(
        (res) => {
          this.msg = "Task Updated";
          if (this.req.id < 1) {
            // this.task = {address:{}};

            this.msg = "Task Added";
          }
          this.notify(this.msg);
          this.get(res.data.tid);
        },
        (error) => {
          console.log(error);
          this.snackBar.open(error.error.message, "", {
            duration: 5000,
          });
        }
      );
    }
  }
  clientSelected() {}

  subtaskModel(id: number, i: number): void {
    const dialogRef = this.dialog.open(SubtaskComponent, {
      width: "350px",
      data: { tid: this.task.tid, stid: id, index: i },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.event == "success") {
        if (result.index > -1) {
          this.subtasks[result.index] = result.data;
        } else {
          this.subtasks.push(result.data);
        }
      }
    });
  }

  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
