import { Component, Inject, Optional,OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {TaskService} from '../../task.service';
import * as moment from 'moment';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
@Component({
  selector: 'app-subtask',
  templateUrl: './subtask.component.html',
  styleUrls: ['./subtask.component.sass']
})
export class SubtaskComponent implements OnInit {
  mytitle = "Add Subtask";
  response: any;
  task = {subTaskId:0,expectedStartTime:moment().format('DD-MM-yyyy HH:mm:ss'),expectedendTime:moment().format('DD-MM-yyyy HH:mm:ss'),expectedStartTime1:{startDate:moment(),endDate:moment()},expectedendTime1:{startDate:moment(),endDate:moment()}};
  constructor(private snackBar : MatSnackBar,private _subtask:TaskService,
      public dialogRef: MatDialogRef<SubtaskComponent>,
      //@Optional() is used to prevent error if no data is passed
      @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
      
      // this.task = {data};

   }

  ngOnInit() {
    if(this.data.stid > 0){
      this.mytitle = "Edit Subtask ID#"+this.data.stid;
      this._subtask.getSubtask(this.data.tid,this.data.stid).subscribe(res=>{
        this.task = res.data;
        this.task.expectedStartTime1 = {startDate:moment(),endDate:moment()};
        this.task.expectedendTime1 = {startDate:moment(),endDate:moment()};
        this.task.expectedStartTime1.startDate = moment(this.task.expectedStartTime,'DD-MM-yyyy HH:mm:ss');
        this.task.expectedStartTime1.endDate = moment(this.task.expectedStartTime,'DD-MM-yyyy HH:mm:ss');
        this.task.expectedendTime1.startDate = moment(this.task.expectedendTime,'DD-MM-yyyy HH:mm:ss');
        this.task.expectedendTime1.endDate = moment(this.task.expectedendTime,'DD-MM-yyyy HH:mm:ss');
      });
    }
    this.data.event = "close";
  }
  addsub(f){
    if(f.valid){
      let subtask = f.value;
      if(this.task.subTaskId>0){subtask.subTaskId = this.task.subTaskId};
      let temp = this.task.expectedStartTime1.startDate.format('HH:mm:ss');
      subtask.expectedStartTime = this.task.expectedStartTime1.startDate.format('DD-MM-yyyy')+' '+temp;
      temp = this.task.expectedendTime1.startDate.format('HH:mm:ss');
      subtask.expectedendTime =  this.task.expectedendTime1.startDate.format('DD-MM-yyyy')+' '+temp;
      this._subtask.addsubtask(this.data.tid,subtask).subscribe(res => {
        this.response = res.data;
        this.dialogRef.close({index:this.data.index,data:this.response,event:"success"});
        if(this.data.stid > 0){
        this.snackBar.open('Sub Task Updated','',{duration:5000});
        }else{
          this.snackBar.open('Sub Task Added','',{duration:5000});
        }
      });
    }
    
  }
}
