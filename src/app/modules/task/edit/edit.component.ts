import { Component, OnInit ,Inject, Injectable} from '@angular/core';
import Swal from 'sweetalert2';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import {FormControl,Validator} from '@angular/forms';
import { ClientService } from '../../client/client.service';
import { ExecutiveService } from '../../executive/executive.service';
import { TaskService } from '../task.service';
import { Router,ActivatedRoute,Params } from '@angular/router';
import * as moment from 'moment';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {
  
  heading = 'Add Task';
  subheading = '';
  icon = 'fa fa-plus';
  btntext = 'Back to Task';
  btnLink = 'task';
  btnIcon = 'angle-left';
  msg: string;
  task = {tid:0,client:{},taskTo:"clients",executives:[],reminderTime:moment().format('DD-MM-yyyy HH:mm:ss'),reminderTime1:{startDate:moment(),endDate:moment()},expstartDate1:{startDate:moment(),endDate:moment()},expstartDate:moment().format('DD-MM-yyyy HH:mm:ss'),expEndDate1:{startDate:moment(),endDate:moment()},expEndDate:moment().format('DD-MM-yyyy HH:mm:ss')};
  clients:any;
  req:any;
  executives: any;
  // accessibility = 0;
  executive = [];
  subtasks:any;

  constructor(private snackBar:MatSnackBar,private _task:TaskService,private _client:ClientService,private _route:ActivatedRoute,private _executive:ExecutiveService,public dialog: MatDialog) { 
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    // this._task.get().subscribe(res=>{
    //     this.task = res;
    // });
    this._client.get().subscribe(res=>{
      this.clients = res;
  });
  this._executive.get().subscribe(res=>{
    this.executives = res;
});
    if(this.req.id>0){
     this.get(this.req.id);
    }
  }
  get(id){

    this.heading = 'Edit Task ID#'+id;
    this._task.view(id).subscribe(res=>{
      this.task = res.data;
      this.task.expstartDate1 = {startDate:moment(),endDate:moment()};
      this.task.expEndDate1 = {startDate:moment(),endDate:moment()};
      this.task.reminderTime1 = {startDate:moment(),endDate:moment()};
      this.task.expstartDate1.startDate = moment(this.task.expstartDate,'DD-MM-yyyy HH:mm:ss');
      this.task.expstartDate1.endDate = moment(this.task.expstartDate,'DD-MM-yyyy HH:mm:ss');
      this.task.expEndDate1.startDate = moment(this.task.expEndDate,'DD-MM-yyyy HH:mm:ss');
      this.task.expEndDate1.endDate = moment(this.task.expEndDate,'DD-MM-yyyy HH:mm:ss');
      this.task.reminderTime1.startDate = moment(this.task.reminderTime,'DD-MM-yyyy HH:mm:ss');
      this.task.reminderTime1.endDate = moment(this.task.reminderTime,'DD-MM-yyyy HH:mm:ss');
      console.log(this.task);
      if(this.task.client != null){
        this.task.taskTo = "clients";
      }else{
        this.task.taskTo  = "others";
      }
    });
    this._task.subtask(id).subscribe(res=>{
      this.subtasks = res.data;
    });
  }

  add(f){
          
    if(f.valid){
    let data = f.value;
    if(this.task.tid>0){data.tid = this.req.id};
    let temp = this.task.expstartDate1.startDate.format('HH:mm:ss');
    data.expstartDate = this.task.expstartDate1.startDate.format('DD-MM-yyyy')+' '+temp;
    temp = this.task.expEndDate1.startDate.format('HH:mm:ss');
    data.expEndDate =  this.task.expEndDate1.startDate.format('DD-MM-yyyy')+' '+temp;
    temp = this.task.reminderTime1.startDate.format('HH:mm:ss');
    data.reminderTime =  this.task.reminderTime1.startDate.format('DD-MM-yyyy')+' '+temp;
    data.client = {id:data.client};
    data.accessibility=='INDIVIDUAL' ?
    data.executives = [ data.executives ] : '';
        this._task.save(data).subscribe(res=>{
         this.msg = "Task Updated";
         if(this.req.id < 1){ 
          // this.task = {address:{}};
            
          this.msg = "Task Added";
         }
         this.notify(this.msg);
         this.get(res.data.tid);
       },
       error=>{
        this.snackBar.open(this.msg,'',{ 
          duration : 5000 });
       });   
    }

  }
  clientSelected(){
    
  }


  notify(data : string){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: data,
      showConfirmButton: false,
      timer: 1500
    });
  }


}
