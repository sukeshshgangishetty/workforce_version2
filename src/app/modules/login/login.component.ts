import { Component, OnInit  } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from 'src/app/helpers/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
 errorMsg = undefined;
  firstFormGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder,private _auth: AuthService,private _route:Router) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      userEmail: ['', Validators.required],
      userPassword: ['', Validators.required]
    });
  }
  submit(f){
    const data = f.value;
    const user = {"username": data.userEmail,"password":data.userPassword};
    this._auth.login(user).subscribe(res=>{ 
        this.errorMsg = undefined;
        console.log("RES",res);
      if (res.statusCodeValue==200){
        
          localStorage.setItem("token",res.body.user.token);
        localStorage.setItem("user", JSON.stringify(res.body.user));
          this._route.navigate(['dashboard']);
        }
    },
    error=>{
    this.errorMsg=error.error.message;
    });  


  }

}
