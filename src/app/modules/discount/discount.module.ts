import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiscountRoutingModule } from './discount-routing.module';
import { DiscountComponent } from './discount/discount.component';
import { AddComponent } from './discount/add/add.component';
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule,MatInputModule } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
@NgModule({
  declarations: [DiscountComponent, AddComponent],
  imports: [
    CommonModule,
    DiscountRoutingModule,
    PageTitleModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatSelectModule
  ]
})
export class DiscountModule { }
