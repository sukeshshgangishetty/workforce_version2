import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DiscountService {

  constructor(private _http: HttpClient) { }
  save(data : any){
    return this._http.post('/workforce/discount',data);
  }
  get(){
    return this._http.get('/workforce/discount');
  }
  view(id : number){
    return this._http.get<any>('/workforce/discount/'+id);
  }
}
