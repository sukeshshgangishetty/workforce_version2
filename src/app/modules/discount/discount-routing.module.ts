import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiscountComponent } from './discount/discount.component';
import { AddComponent as AddDiscount } from './discount/add/add.component';

const routes: Routes = [
  {
    
    path: '',

    children:[
      {
        path:':id',
        component:AddDiscount,
      },
      {
        path:'',
        component:DiscountComponent,
      }
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscountRoutingModule { }
