import { Component, OnInit } from '@angular/core';
import {DiscountService} from '../discount.service'
@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.sass']
})
export class DiscountComponent implements OnInit {
  heading = 'Discounts';
  subheading = '';
  icon = 'fa fa-users';
  btntext = 'Add Discount';
  btnLink = 'discount/0';
  discounts = [];
  temp : any;

  constructor(private _disc: DiscountService) { }

  ngOnInit() {
    this._disc.get().subscribe(res=>{
      this.temp = res;
      this.discounts =this.temp.data;
      console.log(this.discounts);
    });
  }

}
