import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../product/product.service';
import { CategoryService as PcatService  } from '../../../product/category.service';
import {DiscountService} from '../../discount.service'
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  heading = 'Add Discount';
  subheading = '';
  icon = 'fa fa-users';
  btnIcon = 'angle-left';
  btntext = 'Back to Discount';
  btnLink = 'discount';
  discount = {discountType:2,disType:"PRODUCT"};
  products = [];
  pcats = [];
  req:any;
  
  discountTypes = [{"name":"Payment Mode/Order Value","value":1},{"name":"Product Based","value":2}];
  productBasedTypes = [{"name":"Categoty","value":"PRODUCT_CATGORY"},{"name":"Product","value":"PRODUCT"}];
  paymentType = [{"name":"Cash","value":false},{"name":"Percentage","value":true}];
  storeType = [{"name":"Order Value","value":"ORDER_VALUE"},{"name":"Payment Mode","value":"PAYMENT_MODE"}];
  msg: string;
  client: { location: {}; };
  constructor(private _product:ProductService,private _pcat:PcatService,private _disc: DiscountService,private _route:ActivatedRoute,private snackBar:MatSnackBar) {
    this.req = this._route.snapshot.params;
   }

  ngOnInit() {
    this._product.get().subscribe(res=>{
      this.products =res;
    });
    this._pcat.get().subscribe(res=>{
      this.pcats =res;
    });
    if(this.req.id > 0){
      this.heading = 'Edit Discount ID#'+this.req.id;
      this._disc.view(this.req.id).subscribe(res=>{
        this.discount = res.data;
        this.discount.discountType = ["PRODUCT","PRODUCT_CATGORY"].indexOf(res.data.disType)>-1?2:1;
        console.log(this.discount);
      });
    }

  }

  add(f){
    // console.log(f.value);
    // console.log(this.discount);
    this._disc.save(this.discount).subscribe(res=>{
      console.log(res);
      this.msg = "Discount Updated";
         if(this.req.id < 1){ 
          this.client = {location:{}};
          this.msg = "Discount Added";
         }
         this.notify(this.msg);
    },
    error=>{
      this.snackBar.open(this.msg,'',{ 
        duration : 5000 });
     });
  }
  notify(data : string){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: data,
      showConfirmButton: false,
      timer: 1500
    });
  }
}
