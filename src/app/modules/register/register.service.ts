import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  otp(data){
    const body = {"phone":data.contactphone,"prps":1};
    return this.http.post<any>('/workforce/otp?phone='+body.phone+'&prps=1',{});
  }

  validateOTP(data){
    return this.http.post<any>('/workforce/validateOtp?phone='+data.contactphone+'&otp='+data.otp,{});
  }

  register(otp,data){
    return this.http.post<any>('/workforce/register?otp='+otp,data);
  }
}
