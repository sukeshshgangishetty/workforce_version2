import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountdownModule } from '@ciri/ngx-countdown';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
   CountdownModule,
  AngularFontAwesomeModule,
    
  ]
})
export class RegisterModule { }
