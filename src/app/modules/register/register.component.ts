import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { RegisterService } from './register.service';
import { MAT_STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass'],
  providers: [{
    provide: MAT_STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class RegisterComponent implements OnInit {

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  errorMsg = undefined;
  slideConfig2 = {
    className: 'center',
    centerMode: true,
    infinite: true,
    centerPadding: '0',
    slidesToShow: 1,
    speed: 500,
    dots: false,
  };
  // submit(f){
  //   f.valid
  // }
  constructor(private _formBuilder: FormBuilder,private _register: RegisterService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      orgname: ['', Validators.required],
      contactname: ['', Validators.required],
      contactphone: ['', Validators.required],
      contactemail: ['', Validators.required],
      location: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      otp: ['', Validators.required]
    });
  }

  getOtp(stepper: MatStepper){
    if(this.firstFormGroup.valid){
    let formData = this.firstFormGroup.value;
    this._register.otp(formData).subscribe((data)=>{
      if(data.status==200 && stepper){
        stepper.next();
      }
      this.errorMsg = undefined;
    },error => {
      this.errorMsg = error.error.message;
    });
    }
  }

  goBack(stepper: MatStepper){
    stepper.previous();
  }

  validateOTP(stepper: MatStepper){
    if(this.secondFormGroup.valid){
      let formData = this.firstFormGroup.value;
      let formdata2 = this.secondFormGroup.value;
      formdata2.contactphone = formData.contactphone;
      this._register.validateOTP(formdata2).subscribe((res)=>{
        this.errorMsg = undefined;
        if(res.status===200){
          this._register.register(formdata2.otp,formData).subscribe((res)=>{
            this.errorMsg = undefined;
            if(res.status===200){
              stepper.next();
              window.location.href = "login";
            }
          },error => {
            this.errorMsg = error.error.message;
          });
        }
      },error => {
        this.errorMsg = error.error.message;
      });
    }

  }
}
