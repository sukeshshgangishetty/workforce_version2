import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasegoodsComponent } from './purchasegoods.component';

describe('PurchasegoodsComponent', () => {
  let component: PurchasegoodsComponent;
  let fixture: ComponentFixture<PurchasegoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasegoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasegoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
