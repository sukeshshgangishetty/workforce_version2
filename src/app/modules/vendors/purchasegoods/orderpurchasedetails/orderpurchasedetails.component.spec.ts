import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderpurchasedetailsComponent } from './orderpurchasedetails.component';

describe('OrderpurchasedetailsComponent', () => {
  let component: OrderpurchasedetailsComponent;
  let fixture: ComponentFixture<OrderpurchasedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderpurchasedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderpurchasedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
