import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VendorsService } from '../../vendors.service';

@Component({
  selector: 'app-orderpurchasedetails',
  templateUrl: './orderpurchasedetails.component.html',
  styleUrls: ['./orderpurchasedetails.component.sass']
})
export class OrderpurchasedetailsComponent implements OnInit {
  selectedCity: any;
  invoice = [];
  gst = [
    { id: 1, name: 'GST- 5%' },
    { id: 2, name: 'GST- 8%' },
    { id: 3, name: 'GST- 10%', disabled: true },
    { id: 4, name: 'GST- 12%' },
    { id: 5, name: 'GST- 18%' }
  ];
  invoicedata = [
    {
     "id":"1",
     "sku":"43467",
     "productName":"Venilla Ice Cream",
     "category":"Men",
       "hsn":"Sw1234",
       "type":"500 ml",
       "quantity":"2.0",
       "perRate":"30,000.00",
       "tax":"18%",
       "netAmount":"35,1254.00",
    },
      {
        "id":"2",
        "color":"Blue",
        "size":"XXl",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       },
       {
        "id":"3",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "color":"Red",
        "size":"XXl",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       },
       {
        "id":"4",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "color":"Black",
        "size":"XXl",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       },
       {
        "id":"5",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "color":"Pink",
        "size":"XXl",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       }
  ]
  foods=[
    {
      "id":1,
      "name":"demo 1"
    },
    {
      "id":2,
      "name":"demo 2"
    },
    {
      "id":3,
      "name":"demo 3"
    }
  ]
  vendorById:any = [];
  req: any;

  constructor(private _vendorService : VendorsService, private _route: ActivatedRoute) {
    this.req = this._route.snapshot.params;
   }

  ngOnInit() {
      // this.heading = "View Vendor ";
      this._vendorService.get().subscribe((res: any) => {
        if(res){
          this.vendorById = res.body;
          console.log("vendor : ",this.vendorById);
        }
      });
  }

}
