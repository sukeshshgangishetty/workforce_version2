import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderpurchaseinvoiceComponent } from './orderpurchaseinvoice.component';

describe('OrderpurchaseinvoiceComponent', () => {
  let component: OrderpurchaseinvoiceComponent;
  let fixture: ComponentFixture<OrderpurchaseinvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderpurchaseinvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderpurchaseinvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
