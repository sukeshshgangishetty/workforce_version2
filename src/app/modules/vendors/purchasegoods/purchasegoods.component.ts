import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VendorsService } from '../vendors.service';

@Component({
  selector: 'app-purchasegoods',
  templateUrl: './purchasegoods.component.html',
  styleUrls: ['./purchasegoods.component.sass']
})
export class PurchasegoodsComponent implements OnInit {

  heading = 'Vendors Purchase Goods';
  subheading = 'Welcome to Vendors Section';
  icon = 'pe-7s-ticket icon-gradient bg-info';
  btntext = "Back to Vendors";
  btnLink = "vendors";
  btnIcon = "angle-left";

  Mobileinfo:any = [];
  attendance:any =[];
  req: any;
  vendor: any ={};
  
  constructor(private _vendorService : VendorsService, private _route: ActivatedRoute) {
    this.req = this._route.snapshot.params;
   }

  ngOnInit() {
      this.heading = "View Vendor ";
      this._vendorService.get().subscribe((res: any) => {
        if(res){
          this.vendor = res.body;
          console.log("vendor : ",this.vendor);
        }
      });
  }
}
