import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderpurchaseComponent } from './orderpurchase.component';

describe('OrderpurchaseComponent', () => {
  let component: OrderpurchaseComponent;
  let fixture: ComponentFixture<OrderpurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderpurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderpurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
