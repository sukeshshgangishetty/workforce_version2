import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VendorsService } from '../../vendors.service';

@Component({
  selector: 'app-orderpurchase',
  templateUrl: './orderpurchase.component.html',
  styleUrls: ['./orderpurchase.component.sass']
})
export class OrderpurchaseComponent implements OnInit {
  heading = 'Vendors Purchase Details';
  subheading = 'Welcome to Purchase Details Section';
  icon = 'pe-7s-ticket icon-gradient bg-info';
  btntext = "Back to Vendors";
  btnLink = "vendors";
  btnIcon = "angle-left";
  req: any;
  vendor: any =[];
  
  constructor(private _vendorService : VendorsService, private _route: ActivatedRoute) {
    this.req = this._route.snapshot.params;
   }

  ngOnInit() {
    this.heading = "View Vendor ";
    this._vendorService.get().subscribe((res: any) => {
      if(res){
        this.vendor = res.body;
        console.log("vendor : ",this.vendor);
      }
    });
}

}
