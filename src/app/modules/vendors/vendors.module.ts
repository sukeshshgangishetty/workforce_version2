import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import { NgApexchartsModule } from 'ng-apexcharts';

import { VendorsRoutingModule } from './vendors-routing.module';
import { VendorComponent } from './vendor/vendor.component';
import { AddComponent } from './vendor/add/add.component';
import { ViewComponent } from './vendor/view/view.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { MatAutocompleteModule, MatButtonModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatRadioModule, MatSelectModule, MatSlideToggleModule, MatStepperModule, MatTabsModule, MatTooltipModule } from '@angular/material';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CalendarModule ,DateAdapter} from 'angular-calendar';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TrendModule } from 'ngx-trend';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DropzoneConfigInterface, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { BusinessComponent } from './vendor/business/business.component';
import { AddComponent as AddBusiness}  from './vendor/business/add/add.component';
import { PurchasegoodsComponent } from './purchasegoods/purchasegoods.component';
import { PurchasepaymentsComponent } from './purchasepayments/purchasepayments.component';
import { QuotationComponent } from './purchasegoods/quotation/quotation.component';
import { OrderpurchaseComponent } from './purchasegoods/orderpurchase/orderpurchase.component';
import { OrderpurchasedetailsComponent } from './purchasegoods/orderpurchasedetails/orderpurchasedetails.component';
import { OrderpurchaseinvoiceComponent } from './purchasegoods/orderpurchaseinvoice/orderpurchaseinvoice.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { VendorportalComponent } from './vendorportal/vendorportal.component';


const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

@NgModule({
  declarations: [VendorComponent, AddComponent, ViewComponent, BusinessComponent,AddBusiness, PurchasegoodsComponent, PurchasepaymentsComponent, QuotationComponent, OrderpurchaseComponent, OrderpurchasedetailsComponent, OrderpurchaseinvoiceComponent, VendorportalComponent],
  imports: [
    FormsModule ,
    CommonModule,
    NgbModule,

    MatDatepickerModule,
    AngularFontAwesomeModule,
    VendorsRoutingModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatNativeDateModule,
    MatIconModule,
    MatDividerModule,
    MatTabsModule,
    NgbModule,
    MatSlideToggleModule,
    AngularEditorModule,
    TrendModule,
    MatProgressBarModule,
    MatStepperModule,
    CalendarModule,
    MatSelectModule,
    MatCheckboxModule,
    PageTitleModule,
    AngularFontAwesomeModule,
    NgBootstrapFormValidationModule,
    NgApexchartsModule,
    SlickCarouselModule,
    MatCardModule,NgSelectModule,
    NgxDaterangepickerMd.forRoot(),
    MatDatepickerModule,
  ],
  exports: [ MatFormFieldModule, MatInputModule ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG,
    },
    
  ],
 
})
export class VendorsModule { }
