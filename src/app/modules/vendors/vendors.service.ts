import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VendorsService {

  constructor(private _http: HttpClient) { }

  get() {
    return this._http.get("/workforce/api/v1/vendor");
  }
  view(id: number) {
    return this._http.get("/workforce/api/v1/vendor/" + id);
  }
  save(data: any) {
    if (data.id  != 0 && data.vendorId ) {
      return this._http.put("/workforce/api/v1/vendor/" + data.vendorId , data);
    } else {
      return this._http.post("/workforce/api/v1/vendor", data);
    }
  }

  saveAddress(id: string,data: any) {
    if (data.id) {
      return this._http.put("/workforce/api/v1/vendor/address/" + data.id, data);
    } else {
      return this._http.post("/workforce/api/v1/vendor/" + id + "/address", data);
    }
  }
  saveAccount(id:string,data: any) {
    if (data.id) {
      return this._http.put("/workforce/api/v1/vendor/accDetail/" + data.id, data);
    } else {
      return this._http.post("/workforce/api/v1/vendor/" + id + "/accDetail", data);
    }
  }

  statusVendor(data: any,id: string){
    return this._http.patch("/workforce/api/v1/vendor/"+id+"?status="+data,{}); 
  }

  deleteVendor(id: string){
    return this._http.delete("/workforce/api/v1/vendor/" + id);
  }

  // business

  getBusiness() {
    return this._http.get("/workforce/api/v1/vendor/business");
  }
  viewBusiness(id: number) {
    return this._http.get("/workforce/api/v1/vendor/business/" + id);
  }

  saveBusiness(data: any) {
    if (data.id  != 0 && data.id ) {
      return this._http.put("/workforce/api/v1/vendor/business/" + data.id , data);
    } else {
      return this._http.post("/workforce/api/v1/vendor/business", data);
    }
  }

  deleteBusiness(id: string){
    return this._http.delete("/workforce/api/v1/vendor/business/" + id);
  }


   // Delete
   delete(id: any) {
    return this._http.delete("/workforce/api/v1/vendor/address/" + id);
  }
  
  updatePrimaryStatus(key:string,id:string){
    return this._http.patch("/workforce/api/v1/vendor/"+key+"/"+id,{});
  }

}
