import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorComponent } from './vendor/vendor.component';
import { AddComponent as AddVendor } from './vendor/add/add.component';
import { ViewComponent as ViewVendor } from './vendor/view/view.component';
import { BusinessComponent } from './vendor/business/business.component';
import { AddComponent as AddBusiness } from './vendor/business/add/add.component';
import { PurchasegoodsComponent } from './purchasegoods/purchasegoods.component';
import { PurchasepaymentsComponent } from './purchasepayments/purchasepayments.component';
import { OrderpurchaseComponent } from './purchasegoods/orderpurchase/orderpurchase.component';
import { QuotationComponent } from './purchasegoods/quotation/quotation.component';
import { OrderpurchasedetailsComponent } from './purchasegoods/orderpurchasedetails/orderpurchasedetails.component';
import { VendorportalComponent } from './vendorportal/vendorportal.component';


const routes: Routes = [
  {
    path: "",

    children: [
      {
        path: "purchasegoods",
        component: PurchasegoodsComponent,
      },

      {
        path: "orderpurchase",
        component: OrderpurchaseComponent,
      },
      {
        path: "orderpurchasedetails",
        component: OrderpurchasedetailsComponent,
      },

      {
        path: "quotation",
        component: QuotationComponent,
      },


      {
        path: "purchasepayments",
        component: PurchasepaymentsComponent,
      },
      {
        path: "business",
        component: BusinessComponent,
      },
      {
        path: "business/:id",
        component: AddBusiness,
      },
      {
        path: "view/:id",
        component: ViewVendor,
      },
      {
        path: ":id",
        component: AddVendor,
      },
      {
        path: "addvendor",
        component: AddVendor,
      },
      {
        path: "vendorportal",
        component: VendorportalComponent,
      },
      {
        path: "",
        component: VendorComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorsRoutingModule { }
