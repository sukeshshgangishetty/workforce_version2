import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientService } from 'src/app/modules/client/client.service';
import { DateAdapter, MatSnackBar, MAT_DATE_FORMATS } from '@angular/material';
import { ExecutiveService } from 'src/app/modules/executive/executive.service';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { VendorsService } from '../../vendors.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/date.adapter';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass'],
 providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
  ],
})
export class AddComponent implements OnInit {
  req: Params;
  heading = "Add Vendor";
  subheading = "";
  icon = "pe-7s-plus icon-gradient bg-info";
  btntext = "Back to Vendors";
  btnLink = "vendors";
  btnIcon = "angle-left";
  clients: any;
  vendors: any={};
  today = moment().add(1, "days").toDate();
  
  vendor: any = {
    business:{},
    addresses: [{"line1": "","line2": "", "city": "", "state": "","zipcode": ""}],
    accountDetails: [{"name": "","accNo": "","accType": "","bankName": "","branch": "","ifsc": ""}],
  };
  
  vendorCopy: any = {
    business:{},

    addresses: [{"line1": "","line2": "", "city": "", "state": "","zipcode": ""}],
    accountDetails:[{"name": "","accNo": "","accType": "","bankName": "","branch": "","ifsc": ""}],
   };
   CCs: any = {
    business:{},

  addresses :[{ "line1": "", "line2": "", "city": "", "state": "", "zipcode": "" }],
  accountDetails :[{ "name": "", "accNo": "", "accType": "", "bankName": "", "branch": ""}]
};

   vendorstatus = ["Active","Inactive"];
   Vendortype = ["Mobile","Mobile 2"];

  msg: string;
  secondFormGroup: FormGroup;
  cat: any = [];
  exe: any = [];
  business: any = [];
  

  constructor(
    private formBuilder: FormBuilder,
    private _vendorService : VendorsService,
    private _route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _executive: ExecutiveService
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    if (this.req.id !=0 ) {
      this.heading = "Edit Vendor ";
      this._vendorService.view(this.req.id).subscribe((res: any) => {
        if(res){
          // res.body.incorporateDate =  moment().format("yyyy-MM-DD ");
          this.vendor = res.body;
          // console.log("Date :",this.vendor.incorporateDate);
        }
      });
    }
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ["", Validators.required],
    });

    this._vendorService.getBusiness().subscribe((res: any) => {
      this.business = res.body;
     
    });
   
  }


  save(f:any) {
    console.log(this.vendor);
    console.log("Indate",this.vendor.incorporateDate);
    this.vendor.Date = new Date(this.vendor.incorporateDate);
    console.log("Date",this.vendor.Date);
    this.vendor.incorporateDate = this.vendor.Date.getTime();

    if (f.valid) {
      this._vendorService.save(this.vendor).subscribe(
        (res: any) => {
          this.msg = "Vendor Updated";
          if (!this.vendor.vendorId) {
            this.msg = "Vendor Added";
          }
          this.notify(this.msg);
          this.vendor = res.body;
          
          this.checkObjs();
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }

  saveAddress(f: any,i:any,aid:string) {
    if (f.valid) {
      this._vendorService.saveAddress(this.vendor.vendorId,this.vendor.addresses[i]).subscribe(
        (res: any) => {
          this.msg = "Vendor Address Updated";
          if (!this.vendor.addresses[i].id) {
            this.msg = "Vendor Address Added";
          }
          this.notify(this.msg);
          this.vendor.addresses[i] = res.body;
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }

  saveAccount(f: any, i: any) {
    if (f.valid) {
      this._vendorService.saveAccount(this.vendor.vendorId, this.vendor.accountDetails[i]).subscribe(
        (res: any) => {
          this.msg = "Vendor Account Details Updated";
          if (!this.vendor.accountDetails[i].id) {
            this.msg = "Vendor Account Details Added";
          }
          this.notify(this.msg);
          this.vendor.accountDetails[i] = res.body;
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }
  addCC(ref:string){
    this.vendor[ref].push(JSON.parse(JSON.stringify(this.CCs[ref])));
  }
  removeCC(ref:string,i:number,id:string){
    this.vendor[ref].splice(i,1);
    //deleting
    this._vendorService.delete(id).subscribe(
      (res: any) => {
        this.snackBar.open("Address Deleted Successful", "", {
          duration: 5000,
        });

      });
  }

  changeStatus(ref:string,attr:string,i:number){
    this.vendor[ref].map((d: any,index: number) => {
      if(i != index){
        this.vendor[ref][index][attr] = false;
      }
    });
  }


  updateStatus(obj:string,key:string,i:number){
    var keys = { "addresses": 'address', "accountDetails":'accDetail'};
    var id=this.vendor[obj][i].id;
    this._vendorService.updatePrimaryStatus(keys[obj],id).subscribe(res => {
      this.vendor[obj][i][key] = true;
      this.changeStatus(obj,key,i);
      this.snackBar.open("Primary status Update Successful", "", {
        duration: 5000,
      });
    },error=>{
        this.snackBar.open(this.msg, "", {
          duration: 5000,
        });
    });
  }

































  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

  checkObjs(){
    if(this.vendor.addresses.length < 1){
      this.vendor.addresses = JSON.parse(JSON.stringify(this.vendorCopy.addresses));
    }
    if (this.vendor.accountDetails.length < 1) {
      this.vendor.accountDetails = JSON.parse(JSON.stringify(this.vendorCopy.accountDetails));
    }
  }

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
  };
  public onUploadInit(args: any): void {
    // console.log("onUploadInit:", args);
  }

  public onUploadError(args: any): void {
    console.log("onUploadError:", args);
  }

  public onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
  }

}
