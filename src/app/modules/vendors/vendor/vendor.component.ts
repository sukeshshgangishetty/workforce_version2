import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { OffersService } from '../../offers/offers.service';
import { VendorsService } from '../vendors.service';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.sass']
})
export class VendorComponent implements OnInit {
  heading = 'Vendors';
  subheading = 'Welcome to Vendors Section';
  icon = 'pe-7s-ticket icon-gradient bg-info';
  btntext = "Add Vendor";
  btnLink = "vendors/0";
  vendor:any = [];
  msg: string;
  constructor(private _vendorService: VendorsService,private _offersService: OffersService,private snackBar: MatSnackBar,) { }

  ngOnInit() {
    this._vendorService.get().subscribe((res: any) => {
      this.vendor = res.body;
    });

   
}


VenderStatus(data: any,id:string) {
  console.log("status :",this.vendor.isActive);

  this.vendor.isActive = !this.vendor.isActive; 
  console.log("status After:",this.vendor.isActive);
  this._vendorService.statusVendor(data,id).subscribe((res: any) => {
    this.msg ="Vendor status Updated   " + this.vendor.isActive
    this.snackBar.open(this.msg, "", {
      duration: 5000,
    });
  });
}

deleteVendor(id) {
  console.log("deleteBusiness :",id);

this._vendorService.deleteVendor(id).subscribe((res: any) => {
  if(res){
    this.msg = "Vendor  Deleted Sucessfully";
    this.notify(this.msg);
  }
});
}






notify(data: string) {
  Swal.fire({
    position: "center",
    icon: "success",
    title: data,
    showConfirmButton: false,
    timer: 1500,
  });
}

}
