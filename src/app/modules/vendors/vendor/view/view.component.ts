import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { VendorsService } from '../../vendors.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.sass']
})
export class ViewComponent implements OnInit {
  req: Params;
  heading = "View Vendor";
  subheading = "Welcome to Vendor Section";
  icon = "pe-7s-ticket icon-gradient bg-info";
  btntext = "Back to Vendors";
  btnLink = "vendors";
  btnIcon = "angle-left";
  vendorById:any = [];

  constructor(private _vendorService : VendorsService, private _route: ActivatedRoute) {
    this.req = this._route.snapshot.params;
   }

  ngOnInit() {
    if (this.req.id ) {
      this.heading = "View Vendor ";
      this._vendorService.view(this.req.id).subscribe((res: any) => {
        if(res){
          this.vendorById = res.body;
          console.log("vendor : ",this.vendorById);
        }
      });
    }
  }

}
