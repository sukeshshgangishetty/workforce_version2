import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { VendorsService } from '../../vendors.service';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.sass']
})
export class BusinessComponent implements OnInit {
  heading = 'Business';
  subheading = 'Welcome to Business Section';
  icon = 'pe-7s-ticket icon-gradient bg-info';
  btntext = "Add Vendor";
  btnLink = "vendors/business/0";
  business:any = [];
  req: any;
  msg: any;

  constructor(private _vendorService: VendorsService,private _route: ActivatedRoute) { 
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    this._vendorService.getBusiness().subscribe((res: any) => {
      this.business = res.body;
     
    });
  }
 

  deleteBusiness(id) {
    console.log("deleteBusiness :",id);

  this._vendorService.deleteBusiness(id).subscribe((res: any) => {
    if(res){
      this.msg = "Business  Deleted Sucessfully";
      this.notify(this.msg);
    }
  });
}



notify(data: string) {
  Swal.fire({
    position: "center",
    icon: "success",
    title: data,
    showConfirmButton: false,
    timer: 1500,
  });
}

}
