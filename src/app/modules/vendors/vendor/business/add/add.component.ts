import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Params } from '@angular/router';
import { ExecutiveService } from 'src/app/modules/executive/executive.service';
import Swal from 'sweetalert2';
import { VendorsService } from '../../../vendors.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  req: Params;
  heading = "Add Business";
  subheading = "";
  icon = "pe-7s-plus icon-gradient bg-info";
  btntext = "Back to Business";
  btnLink = "vendors/business";
  btnIcon = "angle-left";

  business:any = {};
  msg: string;

  constructor(
    private formBuilder: FormBuilder,
    private _vendorService : VendorsService,
    private _route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _executive: ExecutiveService
  ) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    if (this.req.id != 0) {
      this.heading = "Edit Business ";
      this._vendorService.viewBusiness(this.req.id).subscribe((res: any) => {
        if(res){
          this.business = res.body;
          console.log("business : ",this.business);
        }
      });
    }
  }

  save(f:any) {
    console.log(this.business);

    if (f.valid) {
      this._vendorService.saveBusiness(this.business).subscribe(
        (res: any) => {
          this.msg = "Business Updated";
          if (!this.business.id) {
            this.business = {};
            this.msg = "Business Added";
          }
          this.notify(this.msg);
          this.business = res.body;
        },
        (error) => {
          this.snackBar.open(this.msg, "", {
            duration: 5000,
          });
        }
      );
    }
  }









  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }


}
