import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-purchasepayments',
  templateUrl: './purchasepayments.component.html',
  styleUrls: ['./purchasepayments.component.sass']
})
export class PurchasepaymentsComponent implements OnInit {

  heading = 'Vendors Purchase Payments';
  subheading = 'Welcome to Purchase Payments Section';
  icon = 'pe-7s-ticket icon-gradient bg-info';
  btntext = "Back to Vendors";
  btnLink = "vendors";
  btnIcon = "angle-left";

  constructor() { }

  ngOnInit() {
  }

}
