import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchordersComponent } from './dispatchorders.component';

describe('DispatchordersComponent', () => {
  let component: DispatchordersComponent;
  let fixture: ComponentFixture<DispatchordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
