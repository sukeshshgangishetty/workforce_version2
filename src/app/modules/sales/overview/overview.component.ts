import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.sass']
})
export class OverviewComponent implements OnInit {
  heading = 'Sales Overview Section';
  subheading = 'Welcome to Sales Overview Section';
  icon = 'pe-7s-light icon-gradient bg-info';
  constructor() { }

  ngOnInit() {
  }

}
