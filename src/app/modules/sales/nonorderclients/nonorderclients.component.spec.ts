import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonorderclientsComponent } from './nonorderclients.component';

describe('NonorderclientsComponent', () => {
  let component: NonorderclientsComponent;
  let fixture: ComponentFixture<NonorderclientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonorderclientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonorderclientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
