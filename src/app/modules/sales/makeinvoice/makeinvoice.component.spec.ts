import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeinvoiceComponent } from './makeinvoice.component';

describe('MakeinvoiceComponent', () => {
  let component: MakeinvoiceComponent;
  let fixture: ComponentFixture<MakeinvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeinvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeinvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
