import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesRoutingModule } from './sales-routing.module';
import { OverviewComponent } from './overview/overview.component';
import { TargetComponent } from './targets/target.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { MakeinvoiceComponent } from './makeinvoice/makeinvoice.component';
import { InvoicepdfComponent } from './invoicepdf/invoicepdf.component';
// import { ApexchartsModule } from '../../DemoPages/Charts/apexcharts/apexcharts.module';
import { NgApexchartsModule } from 'ng-apexcharts';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

import {MatCheckboxModule} from '@angular/material/checkbox'; 
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule, MatTabsModule} from '@angular/material';
import { MatChipsModule,MatAutocompleteModule} from '@angular/material'
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio'; 
import { MatTooltipModule } from '@angular/material/tooltip';
import { CalendarModule } from 'angular-calendar';
import { MatStepperModule} from '@angular/material/stepper';
import { MatProgressBarModule} from '@angular/material/progress-bar';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MatSlideToggleModule} from '@angular/material/slide-toggle'; 

import { MatCardModule,MatInputModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { DraftorderComponent } from './draftorder/draftorder.component';
import { VieworderComponent } from './vieworder/vieworder.component';
import { DispatchordersComponent } from './dispatchorders/dispatchorders.component';
import { ViewquotationsComponent } from './viewquotations/viewquotations.component';
import { NonorderclientsComponent } from './nonorderclients/nonorderclients.component';
import { OrderstatusComponent } from './orderstatus/orderstatus.component';
import { ViewbillsComponent } from './viewbills/viewbills.component';
import { OrderformatComponent } from './orderformat/orderformat.component';
import { OrdersModule } from '../orders/orders.module';

@NgModule({
  declarations: [
    OverviewComponent,
     TargetComponent,
      InvoiceComponent,
       MakeinvoiceComponent,
        InvoicepdfComponent,
        DraftorderComponent,
        VieworderComponent,
        DispatchordersComponent,
        ViewquotationsComponent,
        NonorderclientsComponent,
        OrderstatusComponent,
        ViewbillsComponent,
        OrderformatComponent],
  imports: [
    CommonModule,
    SalesRoutingModule,
    MatChipsModule,MatAutocompleteModule, MatPaginatorModule, MatTooltipModule, MatRadioModule,
    MatFormFieldModule,
    ReactiveFormsModule,
     MatButtonModule,
      MatNativeDateModule,
      MatIconModule,
      MatDividerModule,
       MatTabsModule,
        MatSlideToggleModule,
         AngularEditorModule,
          MatProgressBarModule,
        MatStepperModule,
        CalendarModule,
        MatSelectModule,
        CommonModule,
        PageTitleModule,
        NgbModule,
        NgApexchartsModule,
        FormsModule,
        MatCheckboxModule,
        AngularFontAwesomeModule,
        RoundProgressModule,
        MatCardModule,MatInputModule,NgSelectModule,OrdersModule
  ]
})
export class SalesModule { }
