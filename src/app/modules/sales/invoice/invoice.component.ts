import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.sass']
})
export class InvoiceComponent implements OnInit {

  invoicedata = [
    {
     "id":"1",
     "sku":"43467",
     "productName":"Venilla Ice Cream",
     "category":"Men",
       "hsn":"Sw1234",
       "type":"500 ml",
       "quantity":"2.0",
       "perRate":"30,000.00",
       "tax":"18%",
       "netAmount":"35,1254.00",
    },
      {
        "id":"2",
        "color":"Blue",
        "size":"XXl",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       },
       {
        "id":"3",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "color":"Red",
        "size":"XXl",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       },
       {
        "id":"4",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "color":"Black",
        "size":"XXl",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       },
       {
        "id":"5",
        "productName":"Venilla Ice Cream",
        "category":"Men",
          "hsn":"Sw1234",
          "color":"Pink",
        "size":"XXl",
          "quantity":"2.0",
          "perRate":"30,000.00",
          "tax":"18%",
          "netAmount":"35,1254.00",
       }
  ]



  constructor() { }

  ngOnInit() {
  }

}
