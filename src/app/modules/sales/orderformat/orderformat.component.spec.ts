import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderformatComponent } from './orderformat.component';

describe('OrderformatComponent', () => {
  let component: OrderformatComponent;
  let fixture: ComponentFixture<OrderformatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderformatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderformatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
