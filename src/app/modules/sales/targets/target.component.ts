import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-target',
  templateUrl: './target.component.html',
  styleUrls: ['./target.component.sass']
})
export class TargetComponent implements OnInit {
  heading = 'Target Overview Section';
  subheading = 'Welcome to Target Overview Section';
  icon = 'pe-7s-light icon-gradient bg-info';
  constructor() { }

  ngOnInit() {
  }

}
