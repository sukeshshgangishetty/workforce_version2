import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DraftorderComponent } from './draftorder.component';

describe('DraftorderComponent', () => {
  let component: DraftorderComponent;
  let fixture: ComponentFixture<DraftorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DraftorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraftorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
