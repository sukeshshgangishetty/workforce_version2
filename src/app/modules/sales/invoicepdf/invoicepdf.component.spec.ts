import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicepdfComponent } from './invoicepdf.component';

describe('InvoicepdfComponent', () => {
  let component: InvoicepdfComponent;
  let fixture: ComponentFixture<InvoicepdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicepdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicepdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
