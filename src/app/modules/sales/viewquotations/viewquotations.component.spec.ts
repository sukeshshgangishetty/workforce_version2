import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewquotationsComponent } from './viewquotations.component';

describe('ViewquotationsComponent', () => {
  let component: ViewquotationsComponent;
  let fixture: ComponentFixture<ViewquotationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewquotationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewquotationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
