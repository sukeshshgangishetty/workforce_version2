import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from '../orders/orders.component';
import { DispatchordersComponent } from './dispatchorders/dispatchorders.component';
import { DraftorderComponent } from './draftorder/draftorder.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicepdfComponent } from './invoicepdf/invoicepdf.component';
import { MakeinvoiceComponent } from './makeinvoice/makeinvoice.component';
import { NonorderclientsComponent } from './nonorderclients/nonorderclients.component';
import { OrderformatComponent } from './orderformat/orderformat.component';
import { OrderstatusComponent } from './orderstatus/orderstatus.component';
import { OverviewComponent } from './overview/overview.component';
import { TargetComponent } from './targets/target.component';
import { ViewbillsComponent } from './viewbills/viewbills.component';
import { VieworderComponent } from './vieworder/vieworder.component';
import { ViewquotationsComponent } from './viewquotations/viewquotations.component';


const routes: Routes = [
  {

    path: '',

    children: [


      {
        path: 'overview',
        component: OverviewComponent,
      },
      {
        path: 'target',
        component: TargetComponent,
      },
      {
        path: 'addorder',
        component: MakeinvoiceComponent,
      },
      {
        path: 'invoicepdf',
        component: InvoicepdfComponent,
      },
      {
        path: 'invoice',
        component: InvoiceComponent,
      },
      {
        path: 'draftorder',
        component: DraftorderComponent,
      },
      {
        path: 'vieworder',
        component: OrdersComponent,
      },
      {
        path: 'orderstatus',
        component: OrderstatusComponent,
      },
      {
        path: 'dispatchorders',
        component: DispatchordersComponent,
      },
      {
        path: 'viewquotations',
        component: ViewquotationsComponent,
      },
      {
        path: 'nonorderclients',
        component: NonorderclientsComponent,
      },
      {
        path: 'viewbills',
        component: ViewbillsComponent,
      },
      {
        path: 'orderformat',
        component: OrderformatComponent,
      },
    ]

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
