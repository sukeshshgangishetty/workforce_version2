import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountingsRoutingModule } from './accountings-routing.module';
import { NongstbillComponent } from './nongstbill/nongstbill.component';
import { AddvouchersComponent } from './addvouchers/addvouchers.component';
import { ViewvouchersComponent } from './viewvouchers/viewvouchers.component';
import { TypevouchersComponent } from './typevouchers/typevouchers.component';
import { VendorspaymentsComponent } from './vendorspayments/vendorspayments.component';
import { BankaccountsComponent } from './bankaccounts/bankaccounts.component';
import { CreditnotesComponent } from './creditnotes/creditnotes.component';
import { DebitnotesComponent } from './debitnotes/debitnotes.component';
import { ClientsledgersComponent } from './clientsledgers/clientsledgers.component';
import { VendorsledgersComponent } from './vendorsledgers/vendorsledgers.component';
import { TaxesComponent } from './taxes/taxes.component';
import { AccbankaccountsComponent } from './accbankaccounts/accbankaccounts.component';
import { VendorsModule } from '../vendors/vendors.module';

@NgModule({
  declarations: [NongstbillComponent, AddvouchersComponent, ViewvouchersComponent, TypevouchersComponent, VendorspaymentsComponent, BankaccountsComponent, CreditnotesComponent, DebitnotesComponent, ClientsledgersComponent, VendorsledgersComponent, TaxesComponent, AccbankaccountsComponent],
  imports: [
    CommonModule,
    AccountingsRoutingModule,
    VendorsModule
  ]
})
export class AccountingsModule { }
