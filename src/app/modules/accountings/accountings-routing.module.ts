import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaxComponent } from '../product/tax/tax.component';
import { PurchasepaymentsComponent } from '../vendors/purchasepayments/purchasepayments.component';
import { AccbankaccountsComponent } from './accbankaccounts/accbankaccounts.component';
import { AddvouchersComponent } from './addvouchers/addvouchers.component';
import { ClientsledgersComponent } from './clientsledgers/clientsledgers.component';
import { CreditnotesComponent } from './creditnotes/creditnotes.component';
import { DebitnotesComponent } from './debitnotes/debitnotes.component';
import { NongstbillComponent } from './nongstbill/nongstbill.component';
import { TaxesComponent } from './taxes/taxes.component';
import { TypevouchersComponent } from './typevouchers/typevouchers.component';
import { VendorsledgersComponent } from './vendorsledgers/vendorsledgers.component';
import { VendorspaymentsComponent } from './vendorspayments/vendorspayments.component';
import { ViewvouchersComponent } from './viewvouchers/viewvouchers.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'nongstbill',
        component: NongstbillComponent,
      },
      {
        path: 'addvouchers',
        component: AddvouchersComponent,
      },
      {
        path: 'viewvouchers',
        component: ViewvouchersComponent,
      },
      {
        path: 'typevouchers',
        component: TypevouchersComponent,
      },
      {
        path: 'vendorpayments',
        component: VendorspaymentsComponent,
      },
      {
        path: 'bankaccounts',
        component: AccbankaccountsComponent,
      },
      {
        path: 'creditnotes',
        component: CreditnotesComponent,
      },
      {
        path: 'debitnotes',
        component: DebitnotesComponent,
      },
      {
        path: 'clientsledgers',
        component: ClientsledgersComponent,
      },
      {
        path: 'vendorsledgers',
        component: VendorsledgersComponent,
      },
      {
        path: 'taxes',
        component: TaxComponent,
      },
      {
        path: "purchasepayments",
        component: PurchasepaymentsComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingsRoutingModule { }
