import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Params } from '@angular/router';
import Swal from 'sweetalert2';
import { DeviceService } from '../device.service';

@Component({
  selector: "app-adddevice",
  templateUrl: "./adddevice.component.html",
  styleUrls: ["./adddevice.component.sass"],
})
export class AdddeviceComponent implements OnInit {
  req: Params;
  heading = " Device Details";
  subheading = "Welcome To Add Device Section";
  icon = "pe-7s-user icon-gradient btn-primary";
  btntext = "Back to Device";
  btnLink = "/device/device";
  btnIcon = "angle-left";
  msg: string;
  deviceTypes = ["TRUCK", "BUS", "CAR", "BIKE", "OTHER"];

  constructor(
    private _device: DeviceService,
    private _route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.req = this._route.snapshot.params;
  }

  device: any = {};

  ngOnInit() {
    this._device.view(this.req.id).subscribe((res) => {
      this.device = res;
      console.log(this.device);
    });
  }

  // add Device function
  add(f: any) {
    if (f.valid) {
      this.device.id = this.req.id;
      this._device.save(this.device).subscribe(
        (res) => {
          this.msg = "Devices Details Updated";
          if (this.req.id < 1) {
            this.device = {};
            this.msg = "Devices Details Added";
          }
          this.notify(this.msg);
        },
        (error) => {
          this.snackBar.open(error.error.message, "", {
            duration: 5000,
          });
        }
      );
      // console.log("Executive:", this.executive);
    }
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}