import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: "root",
})
export class DeviceService {
  constructor(private _http: HttpClient) {}

  get() {
    return this._http.get("/workforce/api/v1/device/org");
  }
  view(id: string) {
    return this._http.get("/workforce/api/v1/device/detail/" + id);
  }

  save(data: any) {
    
    return this._http.post("/workforce/api/v1/device/detail/" + data.id, data);
    
  }

  raiseComm(data:any){
    if(data.id){
      return this._http.put(`/workforce/api/v1/device/complaint/${data.id}`, data);
    }else{
      return this._http.post(`/workforce/api/v1/device/detail/${data.devDetId}/complaint`, data);
    }
  }
  getCommByID(id:any){
    return this._http.get("/workforce/api/v1/device/complaint/" + id);
  }
  getAllComm(){
    return this._http.get("/workforce/api/v1/device/org/complaint");
  }
  getCommByDeviceID(deviceID:any) {
    return this._http.get(`/workforce/api/v1/device/${deviceID}/complaint`);
  }
  cancelDeviceComm(commID: any) {
    return this._http.get(`/workforce/api/v1/device/complaint/${commID}`);
  }

  // request

  deviceRequest(data: any) {
    if (data.id) {
      return this._http.put(`/workforce/api/v1/device/request/${data.id}`, data);
    } else {
      return this._http.post(`/workforce/api/v1/device/request`, data);
    }
  }
  getReqByID(id: any) {
    return this._http.get("/workforce/api/v1/device/request/" + id);
  }
  getAllReq() {
    return this._http.get("/workforce/api/v1/device/org/request");
  }
  cancelDeviceReq(reqID: any) {
    return this._http.get(`/workforce/api/v1/device/request/${reqID}`);
  }
 
}
