import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../device.service';

@Component({
  selector: "app-device",
  templateUrl: "./device.component.html",
  styleUrls: ["./device.component.sass"],
})
export class DeviceComponent implements OnInit {
  heading = "Device";
  subheading = "Welcome to Device Section";
  icon = "pe-7s-settings icon-gradient btn-primary";

  constructor(private _device: DeviceService) {}

  devices:any = [
    // {
    //   Sno: "1",
    //   Titles: "Device 1",
    //   Issued_To: "Abhinay(Ex)",
    //   phone: "9845784578",
    //   Licence_No: "854",
    //   Vehical_No: "AP 12 CD 4312",
    //   Client: "FedEX",
    //   Last_Active: "02-02-2021 09:05 am",
    //   status: "Online",
    // },
    // {
    //   Sno: "2",
    //   Titles: "Device 2",
    //   Issued_To: "Abhinay(Ex)",
    //   phone: "8236784578",
    //   Licence_No: "541",
    //   Vehical_No: "TS 08 CD 2145",
    //   Client: "Dart",
    //   Last_Active: "09-01-2021 10:05 pm",
    //   status: "Offline",
    // },
  ];

  ngOnInit() {
    this._device.get().subscribe(res=>{
      this.devices = res;
      console.log(this.devices);
    });
  }
}
