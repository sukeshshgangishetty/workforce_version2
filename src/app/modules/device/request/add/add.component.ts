import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import Swal from 'sweetalert2';
import { DeviceService } from '../../device.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  request: any = { quotationRequired:true};
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ]
  };
  req: Params;
  constructor(private _device: DeviceService, private _route: ActivatedRoute) {
    this.req = this._route.snapshot.params;
   }
  

  ngOnInit() {
    if (this.req.id != 0 && this.req.id) {
      this._device.getReqByID(this.req.id).subscribe((res: any) => {
        this.request = res;
      })
    }
  }

  add(f:any){
    this._device.deviceRequest(this.request).subscribe((res:any)=>{
      this.notify("Device Request Successful");
      this.request = { quotationRequired: true };
    });
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

}
