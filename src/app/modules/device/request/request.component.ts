import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { DeviceService } from '../device.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.sass']
})
export class RequestComponent implements OnInit {

  requests:any = [];
  constructor(private _device: DeviceService) { }

  ngOnInit() {
    this._device.getAllReq().subscribe((res: any) => {
      this.requests = res;
    });
  }
  cancelReq(id: any, i: any) {
    this._device.cancelDeviceReq(id).subscribe((res: any) => {
      this.notify('Device Request Cancelled Successful .');
      this.requests[i] = res;
    });
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
