import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import Swal from 'sweetalert2';
import { DeviceService } from '../../device.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  devices:any = [];
  comm:any = {};
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ]
  };
  req:Params;
  constructor(private _device: DeviceService, private _route: ActivatedRoute) { 
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    this._device.get().subscribe(res => {
      this.devices = res;
      console.log(this.devices);
    });
    if(this.req.id != 0 && this.req.id){
        this._device.getCommByID(this.req.id).subscribe((res:any)=>{
         this.comm = res;
          this.comm.devDetId = res.device.id;
        })
    }
  }

  add(f:any){
    this._device.raiseComm(this.comm).subscribe((res:any)=>{
      this.notify("Complaint Raised Successful");
      this.comm = {};
    });
  }
  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

}
