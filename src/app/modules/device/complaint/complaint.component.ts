import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { DeviceService } from '../device.service';

@Component({
  selector: 'app-complaint',
  templateUrl: './complaint.component.html',
  styleUrls: ['./complaint.component.sass']
})
export class ComplaintComponent implements OnInit {

  complaints:any = [];
  constructor(private _device: DeviceService) { }

  ngOnInit() {
    this._device.getAllComm().subscribe((res:any)=>{
      this.complaints = res;
    });
  }

  cancelReq(id:any,i:any){
    this._device.cancelDeviceComm(id).subscribe((res: any) => {
      this.notify('Complaint withdraw Successful .');
      this.complaints[i] = res;
    });
  }

  notify(data: string) {
    Swal.fire({
      position: "center",
      icon: "success",
      title: data,
      showConfirmButton: false,
      timer: 1500,
    });
  }

}
