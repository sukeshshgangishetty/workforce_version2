import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdddeviceComponent } from './adddevice/adddevice.component';
import { DeviceComponent } from './device/device.component';
import { TrackComponent } from './track/track.component';
import { RequestComponent } from './request/request.component';
import { AddComponent as RADD } from './request/add/add.component';
import { ComplaintComponent } from './complaint/complaint.component';
import { AddComponent as CADD } from './complaint/add/add.component';


const routes: Routes = [
  {
    path: "",

    children: [
      {
        path: "device",
        component: DeviceComponent,
      },
      {
        path: "track",
        component: TrackComponent,
      },
      {
        path: "track/:id",
        component: TrackComponent,
      },
      {
        path: "complaints",
        component: ComplaintComponent,
      },
      {
        path: "complaint/:id",
        component: CADD,
      },
      {
        path: "requests",
        component: RequestComponent,
      },
      {
        path: "request/:id",
        component: RADD,
      },
      {
        path: ":id",
        component: AdddeviceComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeviceRoutingModule { }
