import {
  Component,
  OnInit,
  AfterViewInit,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import * as L from "leaflet";
import "mapbox-gl-leaflet";
import * as moment from "moment";


@Component({
  selector: "app-track",
  templateUrl: "./track.component.html",
  styleUrls: ["./track.component.sass"],
})
export class TrackComponent implements OnInit, AfterViewInit {
  // pagetitle Code
  @Output() public callback: EventEmitter<any> = new EventEmitter();

  heading = "Device Tracking";
  subheading = "Welcome To Device Tracking Section";
  icon = "pe-7s-map icon-gradient btn-primary";
  private map: L.Map;

  markers = [];
  // track
  track = {
    Tracks: [
      {
        name: "rajesh(AP 12 be 1234)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1533306044000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: { lat: 17.254684, lng: 78.25412547, address: "" },
          ttype: 0,
          currbat: -1.0,
          trackUpdatedAt: null,
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 867,
          exeid: 2,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "sachin(AP 12ub 1234)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1533448519000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.43316,
            lng: 78.446512,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 31.0,
          trackUpdatedAt: "02-11-2019 12:07:37",
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 883,
          exeid: 20,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "NeemiiT(TS 00 AB 1234)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1565238095000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.35655,
            lng: 78.59119,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 16.0,
          trackUpdatedAt: "21-08-2019 08:51:33",
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 4321,
          exeid: 63,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "NEEMiiT 2(TS 01 CD 4321)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1565238130000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.355923 + 1,
            lng: 78.593928,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 10.0,
          trackUpdatedAt: "21-08-2019 09:03:16",
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 4322,
          exeid: 118,
          distanceTravelled: 0.0,
        },
        track: null,
      },
    ],
    Error: "No",
  };

  date = moment();
  end = moment().endOf("month").format("D");

  ngAfterViewInit() {
    this.map = L.map("map").setView([17.39062, 78.55883], 11);
    this.map.options.minZoom = 3;
    this.map.options.maxZoom = 20;

    L.tileLayer("http://49.156.148.124/map/{z}/{x}/{y}.png").addTo(this.map);
    setInterval(() => {
      this.locationupdate();
    }, 500);
  
     
 
  // L.marker([ 17.35655,78.59119], { icon: greenIcon })
  //   .bindPopup("I am a green leaf.")
  //   .addTo(this.map);
  }
  constructor() {}

  ngOnInit() {}

  devicetrack = [
    {
      id: 1,
      time: "7:13 am",
      battery: "10",
      address:
        "Block 1, DivyaSree Omega Survey No 13, Kohtaguda, Telangana 500084",
      distance: "15",
    },
    {
      id: 1,
      time: "2:54 am",
      battery: "20",
      address:
        "Jayabheri Silicon Towers, 118/1/14/C, 2nd, Kondapur, Telangana 500084",
      distance: "30",
    },
    {
      id: 1,
      time: "8:09 am",
      battery: "50",
      address:
        "Kukatpally Housing Board Colony, Kukatpally Hyderabad, Telangana 500085",
      distance: "58",
    },
    {
      id: 1,
      time: "6:02 am",
      battery: "95",
      address: "Jubilee Enclave, HITEC City, Hyderabad, Telangana 500081",
      distance: "62",
    },
    {
      id: 1,
      time: "9:15 am",
      battery: "15",
      address:
        "G3, Sri Mytri Square Opp.Sarath City Mall, near Laxmi Cyber City, Kohtaguda, Telangana 500084",
      distance: "71",
    },
    {
      id: 1,
      time: "4:53 am",
      battery: "34",
      address: "Gitam university, Rudraram, Telangana 502329",
      distance: "79",
    },
  ];
  exedetails = [
    {
      id: 1,
      Vehical_No: "TS 10 ED 8547",
      Vehical_Model: "Bajaj",
      name: "Sai",
      phone: "8547854785",
      signal: "31",
      date: "02-02-2020",
      start_time: "12:05",
      end_time: "2:50",
      speed: "50",
      distance: "50",
      status: "online",
    },
    {
      id: 2,
      Vehical_No: "RJ 01 LN 2154",
      Vehical_Model: "BMW",
      name: "Raj",
      phone: "985658965",
      signal: "25",
      date: "25-01-2021",
      start_time: "03:50",
      end_time: "01:02",
      speed: "25",
      distance: "100",
      status: "offline",
    },
  ];

  changeMonth(m) {
    if (m == "add") {
      this.date.add(1, "month");
    } else {
      this.date.subtract(1, "month");
    }
    this.end = this.date.endOf("month").format("D");
  }

  // function for api call for location updates
  locupdate() {
    //api
  }
  i = 0;
  locationupdate() {

   
    const trukIcon = L.icon({
    iconUrl: '/assets/images/markers/truk.png',

    iconSize:     [20, 35], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    // iconAnchor:   [30, 20], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
});;

    // markarIcon

    this.i += 0.00001;
    this.track.Tracks.forEach(function (value) {
      var i = this.markers.findIndex(
        (m) => m.fsexeid == value.currentTrack.exeid
      );
      if (i == -1) {
        var marker = L.marker(
          [
            value.currentTrack.currentLoc.lat,
            value.currentTrack.currentLoc.lng,
          ],
          { icon: trukIcon }
        );
        marker.bindTooltip(value.name, {
          permanent: true,
          className: "markerLable",
          offset: [0, 0],
        });
        marker.addTo(this.map);

        this.markers.push({
          fsexeid: value.currentTrack.exeid,
          marker: marker,
          data: value,
        });
      } else {
        //updating marker
        this.markers[i].marker.setLatLng(
          [
            value.currentTrack.currentLoc.lat + this.i,
            value.currentTrack.currentLoc.lng,
          ],
          { icon: trukIcon }
        );
      }

      //Animation controll on zoom

      // this.map.on(
      //   "zoomstart",
      //   function (e) {
      //     // $('.busMarker').removeClass("leaflet-zoom-animated");
      //     $(".awesome-marker-shadow").css("transition", "");
      //     $(".busMarker").css("transition", "");
      //   },
        
      // );
      // this.map.on(
      //   "zoomend",
      //   function (e) {
      //     // console.log("ZOOMEND", e);
      //     // $('.busMarker').css("animation-play-state","running");
      //     $(".awesome-marker-shadow").css("transition", "all 9s linear");
      //     $(".busMarker").css("transition", "all 9s linear");
      //   },
       
      // );
    }, this);
  }

  // Executive Function
  // open() {
  //   console.log("Hi Executive...!");
  // }
  scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: "smooth" });
  }


}
