import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeviceRoutingModule } from './device-routing.module';
import { DeviceComponent } from './device/device.component';
import { PageTitleModule } from 'src/app/Layout/Components/page-title/page-title.module';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatAutocompleteModule, MatButtonModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatPaginatorModule, MatRadioModule, MatSelectModule, MatSlideToggleModule, MatTabsModule, MatTooltipModule, MatTreeModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AdddeviceComponent } from './adddevice/adddevice.component';
import { TrackComponent } from './track/track.component';
import { DateDirective } from 'src/app/directives/date.directive';
import { RequestComponent } from './request/request.component';
import { ComplaintComponent } from './complaint/complaint.component';
import { AddComponent as RADD } from './request/add/add.component';
import { AddComponent as CADD } from './complaint/add/add.component';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  declarations: [
    DeviceComponent,
    AdddeviceComponent,
    TrackComponent,
    DateDirective,
    RequestComponent,
    ComplaintComponent,
    RADD,
    CADD,
  ],
  imports: [
    CommonModule,
    DeviceRoutingModule,
    PageTitleModule,
    AgmCoreModule,
    NgbModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatIconModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatIconModule,
    MatTabsModule,
    MatTooltipModule,
    MatTreeModule,
    MatRadioModule,
    AngularFontAwesomeModule,
    NgxDaterangepickerMd.forRoot(),
    MatSlideToggleModule,
    PerfectScrollbarModule,
    AngularEditorModule
  ],
})
export class DeviceModule {}
