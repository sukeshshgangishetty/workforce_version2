import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OrdersComponent} from './orders.component';
import { EditOrdersComponent } from './edit-orders/edit-orders.component';

const routes: Routes = [
  {
    path:'',

    children:[

{

  path:':id',
  component: EditOrdersComponent

},

{

  path:'',
  component: OrdersComponent
}
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
