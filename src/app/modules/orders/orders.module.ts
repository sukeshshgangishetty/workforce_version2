import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component' 
import { MatDividerModule,MatNativeDateModule,MatDatepickerModule,MatCheckboxModule,MatSelectModule,MatStepperModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule, MatExpansionModule, MatSortModule, MatTableModule, MatDialogModule, } from '@angular/material';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { EditOrdersComponent } from './edit-orders/edit-orders.component';
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import {FilterService} from '@syncfusion/ej2-angular-grids';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    OrdersComponent,
    OrdersListComponent,
    EditOrdersComponent,
    
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatSelectModule,
    MatStepperModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    AngularFontAwesomeModule,
    MatDividerModule,
    MatSortModule,
    MatTableModule,
    MatDialogModule,
    MatCheckboxModule,
    FormsModule,
    PageTitleModule,
    HttpClientModule,
   NgbModule,
  ],

  providers:[FilterService]
})
export class OrdersModule { }
