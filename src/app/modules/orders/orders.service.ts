import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class OrdersService {
 

  constructor(private http:HttpClient) { }

  get(){
    return this.http.get<any>('/workforce/order');
  }

  place(){
    return this.http.post<any>('/workforce/order',{});
  }

  sampleorder(){
    return this.http.get<any>('/workforce/order/sam');
  }
  update(id : number, body : any){
    return this.http.post<any>('/workforce/order/'+id,body);
  }
  status(id : any, status : any){
    return this.http.post<any>('/workforce/order/'+id+'/status/'+status,{});
  }
  getOrderBycid(id:any){
    return this.http.get<any>('/workforce/api/v1/client/'+id+'/orders');
  }

  view(id){

  return this.http.get<any>('/workforce/order/'+id);

  }
}
