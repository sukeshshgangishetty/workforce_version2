import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { OrdersService } from '../orders.service';
@Component({
  selector: 'app-edit-orders',
  templateUrl: './edit-orders.component.html',
  styleUrls: ['./edit-orders.component.sass']
})
export class EditOrdersComponent implements OnInit {
  

  heading = 'View orders';
  subheading = '';
  icon = 'fa fa-user';
  btntext = '';
  btnLink = 'order';
  btnIcon = 'angle-left';
  orders = {order:{orderto:{}, orderCollected:{},orderid:0}, particulars:[]};
  req: Params;
  msg: string;
  editMode = false;
  constructor(private _orders : OrdersService,private _route:ActivatedRoute) { 
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    if(this.req.id > 0){
      this.heading = 'Edit Order ID#'+this.req.id;
      this._orders.view(this.req.id).subscribe(res=>{
        this.orders = res;
        console.log(res);
      });
    }
  }

edit(){
  this.editMode = !this.editMode;
}
 save(){
   let oid = this.orders.order.orderid;
   let body = this.orders.particulars;
   this.edit();
   this._orders.update(oid,body).subscribe(res=>{
     console.log(res);
   },error=>{});
  // console.log(this.orders);
  
 }


  statusChange(orderid,status,statusText) {
    Swal.fire({
      title: 'You wanna '+statusText+' the order with ID#'+orderid+'?',
      text: 'You wont be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
       this._orders.status(orderid,status).subscribe(res=>{
        Swal.fire(
          status,
          'Order has been '+statusText+'ed.',
          'success'
        );
       },error=>{ Swal.fire(
        'Order.!',
        'Order not '+statusText+'ed.',
        'error'
      );})
        
      }
    });
  }




  swalWarning() {
    Swal.fire({
      title: 'You wanna reject the order?',
      text: 'You wont be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Rejected!',
          'Order has been Rejected.',
          'success'
        );
      }
    });
  }


}
