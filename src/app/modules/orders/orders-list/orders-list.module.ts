import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersListRoutingModule } from './orders-list-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OrdersListRoutingModule
  ]
})
export class OrdersListModule { }
