import { Component, OnInit, ViewChild } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {OrdersService} from './orders.service' 
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

// export interface OrderElement {
//   id:number;
//   name: string;
//   orderType:string;
//   phone: number;
//   email: string;
//   totalAmount: number;
//   taxableAmount: number;
//   netAmount: number;
//   orderGeneratedBy: string;
// }

// const ELEMENT_DATA = [
//   {id: 1, orderType:'Food', name: 'Ratnadeep', phone: 9876543210, email: 'xyz@gmail.com',totalAmount:5000,taxableAmount:100,netAmount:5100,orderGeneratedBy:'Cash'},
//   {id: 2, orderType:'Ice ',  name: 'Kwality Walls',phone: 9876543210, email: 'xyz@gmail.com',totalAmount:4000,taxableAmount:200,netAmount:4200,orderGeneratedBy:'Card'},
//   {id: 3, orderType:'Fruits', name: 'Ghanshyam',phone: 9876543210, email: 'xyz@gmail.com',totalAmount:3000,taxableAmount:300,netAmount:3300,orderGeneratedBy:'Cash'},
//   {id: 4, orderType:'Goods', name: 'Dmart', phone: 9876543210, email: 'xyz@gmail.com',totalAmount:2000,taxableAmount:400,netAmount:2400,orderGeneratedBy:'Card'},
//   {id: 5, orderType:'Clothes',name: 'Brand Bazaar',phone: 9876543210, email: 'xyz@gmail.com', totalAmount:1000,taxableAmount:500,netAmount:1500,orderGeneratedBy:'Cash' }
// ];




@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.sass']
})
export class OrdersComponent implements OnInit {
  heading = 'Orders';
  subheading = '';
  icon = 'fa fa-shopping-cart';
  btntext = 'Add to dispatch list';
  btnLink = 'addToDispatch()';
  orders = [];
  dispatchList = [];
  searchValue:string;

  
  constructor(private http: HttpClient, private _order:OrdersService,private _Activatedroute:ActivatedRoute ){ }  
  ngOnInit() {
    this._order.get().subscribe(res=>{  // Order Service
     this.orders = res;
      console.log(res);
    });
   }  

  toggleSelect(event,id){
   event.checked?
      this.dispatchList.push(id)
      :
      this.dispatchList.indexOf(id)<0?'':this.dispatchList.splice(this.dispatchList.indexOf(id),1);
    
    console.log(this.dispatchList);
  }

  addToDispatch(){

    this.http.post<any>('/workforce/dispatchlist',this.dispatchList).subscribe(res=>{
      console.log(res);
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Dispatch List is Generated',
        showConfirmButton: false,
        timer: 1500
      });
    });
  }

  
 
}