import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddserviceComponent } from './addservice/addservice.component';
import { ServicecategoryComponent } from './servicecategory/servicecategory.component';
import { ServicesubcategoryComponent } from './servicesubcategory/servicesubcategory.component';
import { ViewserviceComponent } from './viewservice/viewservice.component';


const routes: Routes = [
  {    
    path: '',
    children:[          
      {
        path:'addservice',
        component:AddserviceComponent,
      },
      {
        path:'viewservice',
        component:ViewserviceComponent,
      },
      {
        path:'servicecategory',
        component:ServicecategoryComponent,
      },
      {
        path:'servicesubcategory',
        component:ServicesubcategoryComponent,
      },
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
