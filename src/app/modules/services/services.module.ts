import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';
import { AddserviceComponent } from './addservice/addservice.component';
import { ViewserviceComponent } from './viewservice/viewservice.component';
import { ServicecategoryComponent } from './servicecategory/servicecategory.component';
import { ServicesubcategoryComponent } from './servicesubcategory/servicesubcategory.component';


@NgModule({
  declarations: [AddserviceComponent, ViewserviceComponent, ServicecategoryComponent, ServicesubcategoryComponent],
  imports: [
    CommonModule,
    ServicesRoutingModule
  ]
})
export class ServicesModule { }
