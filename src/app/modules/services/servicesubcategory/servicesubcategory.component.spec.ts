import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesubcategoryComponent } from './servicesubcategory.component';

describe('ServicesubcategoryComponent', () => {
  let component: ServicesubcategoryComponent;
  let fixture: ComponentFixture<ServicesubcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesubcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesubcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
