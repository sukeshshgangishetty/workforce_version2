import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrackRoutingModule } from './track-routing.module';
import { TrackComponent } from './track/track.component';
import { PageTitleModule } from "../../Layout/Components/page-title/page-title.module";
import { AgmCoreModule } from "@agm/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MatAutocompleteModule, MatButtonModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatPaginatorModule, MatRadioModule, MatSelectModule, MatTabsModule, MatTooltipModule, MatTreeModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { TrackinghistoryComponent } from './trackinghistory/trackinghistory.component';



@NgModule({
  declarations: [TrackComponent, TrackinghistoryComponent],
  imports: [
    CommonModule,
    TrackRoutingModule,
    PageTitleModule,
    AgmCoreModule,
    NgbModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatIconModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatIconModule,
    MatTabsModule,
    MatTooltipModule,
    MatTreeModule,
    MatRadioModule,
    AngularFontAwesomeModule
  ],
})
export class TrackModule {}
