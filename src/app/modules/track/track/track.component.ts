import {
  Component,
  OnInit,
  AfterViewInit,
  EventEmitter,
  Input,
  Output
} from "@angular/core";
import * as L from "leaflet";
import "mapbox-gl-leaflet";
import * as moment from "moment";
import { TrackService } from "../track.service";

@Component({
  selector: "app-track",
  templateUrl: "./track.component.html",
  styleUrls: ["./track.component.sass"],
})
export class TrackComponent implements OnInit, AfterViewInit {
  // pagetitle Code
  @Output() public callback: EventEmitter<any> = new EventEmitter();

  heading = "Maps";
  subheading = "Welcome To Maps Section";
  icon = "pe-7s-map icon-gradient btn-primary";
  private map: L.Map;
  markers = [];
  // track
  track = {
    Tracks: [
      {
        name: "Venkat",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 1,
          startedat: 1612238886000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.4552859,
            lng: 78.3719631,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 93.0,
          trackUpdatedAt: "02-02-2021 10:33:49",
          imei: "e7b2918ca699e9be",
          manufacturer: "Xiaomi",
          osversion: "10",
          model: "POCO X2",
          speed: 0.0,
          trackkey: 7454,
          exeid: 9,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "Abhinay",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 1,
          startedat: 1613138504000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.455261,
            lng: 78.3721999,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 8.0,
          trackUpdatedAt: "13-02-2021 19:49:09",
          imei: "97b8dcbbd0edaa97",
          manufacturer: "OnePlus",
          osversion: "10",
          model: "AC2001",
          speed: 0.0,
          trackkey: 7501,
          exeid: 10,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "testcase1",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 1,
          startedat: 1583301337000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.42028391454369,
            lng: 78.65512154996395,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 55.0,
          trackUpdatedAt: "04-03-2020 11:53:22",
          imei: "358461095971091",
          manufacturer: "samsung",
          osversion: "9",
          model: "SM-J600G",
          speed: 0.0,
          trackkey: 5935,
          exeid: 40,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "Eorb test2",
        currentTrack: {
          tstat: "TRACK_STOPPED",
          cttype: 1,
          startedat: 1573731173000,
          kmtr: 0.0,
          endedat: 1573731187000,
          currentLoc: {
            lat: 17.44661260396242,
            lng: 78.4853803832084,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 72.0,
          trackUpdatedAt: "14-11-2019 17:03:04",
          imei: "358461095971091",
          manufacturer: "samsung",
          osversion: "9",
          model: "SM-J600G",
          speed: 0.0,
          trackkey: 5186,
          exeid: 42,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "test5",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 1,
          startedat: 1572852320000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: { lat: 0.0, lng: 0.0, address: "" },
          ttype: 0,
          currbat: -1.0,
          trackUpdatedAt: null,
          imei: "864537038658684",
          manufacturer: "LENOVO",
          osversion: "7.0",
          model: "Lenovo K33a42",
          speed: 0.0,
          trackkey: 5092,
          exeid: 45,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "K KIRAN REDDY",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 1,
          startedat: 1573001210000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.476223333333333,
            lng: 78.62563333333333,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 62.0,
          trackUpdatedAt: "06-11-2019 06:22:52",
          imei: "860643040244358",
          manufacturer: "OPPO",
          osversion: "9",
          model: "CPH1969",
          speed: 0.0,
          trackkey: 5109,
          exeid: 63,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "rajesh(AP 12 be 1234)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1533306044000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: { lat: 0.0, lng: 0.0, address: "" },
          ttype: 0,
          currbat: -1.0,
          trackUpdatedAt: null,
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 867,
          exeid: 2,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "sachin(AP 12ub 1234)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1533448519000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.43316,
            lng: 78.446512,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 31.0,
          trackUpdatedAt: "02-11-2019 12:07:37",
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 883,
          exeid: 20,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "NeemiiT(TS 00 AB 1234)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1565238095000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.35655,
            lng: 78.59119,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 16.0,
          trackUpdatedAt: "21-08-2019 08:51:33",
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 4321,
          exeid: 63,
          distanceTravelled: 0.0,
        },
        track: null,
      },
      {
        name: "NEEMiiT 2(TS 01 CD 4321)",
        currentTrack: {
          tstat: "TRACK_STARTED",
          cttype: 2,
          startedat: 1565238130000,
          kmtr: 0.0,
          endedat: null,
          currentLoc: {
            lat: 17.355923 + 1,
            lng: 78.593928,
            address: "Location Error",
          },
          ttype: 0,
          currbat: 10.0,
          trackUpdatedAt: "21-08-2019 09:03:16",
          imei: null,
          manufacturer: null,
          osversion: null,
          model: null,
          speed: 0.0,
          trackkey: 4322,
          exeid: 118,
          distanceTravelled: 0.0,
        },
        track: null,
      },
    ],
    Error: "No",
  };

  date = moment();
  end = moment().endOf("month").format("D");

  ngAfterViewInit() {
    this.map = L.map("map").setView([17.39062, 78.55883], 11);
    this.map.options.minZoom = 3;
    this.map.options.maxZoom = 20;

    L.tileLayer("http://49.156.148.124/map/{z}/{x}/{y}.png").addTo(this.map);
    setInterval(() => {
      this.locationupdate();
    }, 500);
  }

  constructor(private _track: TrackService) {}

  ngOnInit() {
    // console.log("HI From Track..!",this.track);
    //track
    // this._track.getTrack().subscribe((res) => {
    //   this.track.Tracks = res;
    //   console.log("Track Data:", this.track.Tracks);
    // });
    //Tracking Logic
  }

  // function for api call for location updates
    locupdate(){
      //api
      
    }
    i = 0;
  locationupdate() {
    this.i += 0.00001000;
    this.track.Tracks.forEach(function (value) {
      var i = this.markers.findIndex(
        (m) => m.fsexeid == value.currentTrack.exeid
      );
      if (i == -1) {
        var marker = L.marker([
          value.currentTrack.currentLoc.lat,
          value.currentTrack.currentLoc.lng,
        ]);
        marker.bindTooltip(value.name, {
          permanent: true,
          className: "markerLable",
          offset: [0, 0],
        });
        marker.addTo(this.map);

        this.markers.push({
          fsexeid: value.currentTrack.exeid,
          marker: marker,
          data: value,
        });
      } else {
        //updating marker
        this.markers[i].marker.setLatLng([
          value.currentTrack.currentLoc.lat + this.i,
          value.currentTrack.currentLoc.lng ,
        ]);
      }

      //  console.log("Markers:",this.markers);

      //  secound Forloop

      //  marker
      //    .bindPopup(value.name,{
      //      maxWidth: 500,
      //    })
      //  .openPopup();
    }, this);
  }

 

  exedetails = [
    {
      id: 1,
      name: "Abhinay",
      phone: "8547854785",
      aadhar: "**85124",
      type: "Executive",
      Currentlocation: "In Local Area",
      HoldingTaskonHand: "5",
      status: "Active",
      LastLocationUpdatedAt: "5-2-2021 12:23:12",
    },
    {
      id: 2,
      name: "Sai",
      phone: "8547854785",
      aadhar: "**8547",
      type: "Executive",
      Currentlocation: "In Local Area",
      HoldingTaskonHand: "10",
      status: "Active",
      LastLocationUpdatedAt: "5-2-2021 12:23:12",
    },
    {
      id: 3,
      name: "Deepak",
      phone: "8547854785",
      aadhar: "**4251",
      type: "Executive",
      Currentlocation: "In Local Area",
      HoldingTaskonHand: "2",
      status: "Active",
      LastLocationUpdatedAt: "5-2-2021 12:23:12",
    },
  ];

  changeMonth(m) {
    if (m == "add") {
      this.date.add(1, "month");
    } else {
      this.date.subtract(1, "month");
    }
    this.end = this.date.endOf("month").format("D");
  }

  // Executive Function
  // open() {
  //   console.log("Hi Executive...!");
  // }
  scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: "smooth" });
  }
}
