import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: "root",
})
export class TrackService {
  constructor(private _http: HttpClient) {}

  //track
  getTrack() {
    return this._http.get("/workforce/api/v1/track/allLoc");
  }

  //getTrackExeId
  getTrackExeId(id: number){
    return this._http.get("/workforce/api/v1/track/trackHistory?entityKey="+id+"&trackType=EXECUTIVE_DEVICE_MOBILE");
  }
}
