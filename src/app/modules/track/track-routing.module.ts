import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrackComponent } from './track/track.component';
import { TrackinghistoryComponent } from './trackinghistory/trackinghistory.component';


const routes: Routes = [
  {
    path: "",

    children: [
      {
        path: "livemap",
        component: TrackComponent,
      },
      {
        path: "trackinghistory",
        component: TrackinghistoryComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrackRoutingModule { }
