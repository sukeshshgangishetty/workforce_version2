import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackinghistoryComponent } from './trackinghistory.component';

describe('TrackinghistoryComponent', () => {
  let component: TrackinghistoryComponent;
  let fixture: ComponentFixture<TrackinghistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackinghistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackinghistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
