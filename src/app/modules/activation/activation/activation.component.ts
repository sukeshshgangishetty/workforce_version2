import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute,Params} from '@angular/router';
import {ActivationService} from '../activation.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import { MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.sass']
})
export class ActivationComponent implements OnInit {

  activationFormGroup: FormGroup;
  req: any;
 

  constructor(private snackbar:MatSnackBar, private _route : ActivatedRoute, private _activate : ActivationService,private _formBuilder : FormBuilder) {

      this.req   = this._route.snapshot.params;
   }

  ngOnInit() {
    this.activationFormGroup = this._formBuilder.group({
      userPasswordpass: ['', Validators.required],
      userRePassword: ['', Validators.required]
    });
   }

submit(f){
  let data = f.value;
  const body = {"key":this.req.key,"pass":data.userPassword};
  this._activate.activation(body).subscribe(res=>{
    Swal.fire({
      title: "<strong>Your Account Is Successfully Activated</strong>",
      icon: "success",
      html:
        "",
      showCloseButton: true,
      focusConfirm: true,
      confirmButtonText: '<i class="fa fa-angleleft"></i> Back To Login!',
      confirmButtonAriaLabel: "Confirm",
    }).then((res) => {
      if (res.isConfirmed) {
        window.location.href='/login';
      }
    });
  },error=>{
     this.snackbar.open("Need to add from server", "", {
       duration: 5000,
     });
  });
}


}
