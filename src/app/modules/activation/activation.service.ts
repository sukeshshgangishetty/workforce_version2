import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
  
})
export class ActivationService {

  activation(data){
   return this.http.get<any>('/workforce/activate?activationKey='+data.key+'&pass='+data.pass,{headers:{"X-AUTH-TOKEN":"NONE"}});
  }
  constructor(private http: HttpClient) {



   }
}
