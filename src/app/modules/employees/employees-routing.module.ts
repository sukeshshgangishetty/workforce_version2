import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdddepartmentComponent } from './adddepartment/adddepartment.component';
import { AddemployeesComponent } from './addemployees/addemployees.component';
import { AppstatusComponent } from './appstatus/appstatus.component';
import { DailyreportsComponent } from './dailyreports/dailyreports.component';
import { LaboursComponent } from './labours/labours.component';
import { ViewdepartmentComponent } from './viewdepartment/viewdepartment.component';
import { ViewemployeesComponent } from './viewemployees/viewemployees.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'addemployee',
        component: AddemployeesComponent,
      },
      {
        path: 'viewemployees',
        component: ViewemployeesComponent,
      },
      {
        path: 'labours',
        component: LaboursComponent,
      },
      {
        path: 'adddepartment',
        component: AdddepartmentComponent,
      },
      {
        path: 'viewdepartment',
        component: ViewdepartmentComponent,
      },
      {
        path: 'appstatus',
        component: AppstatusComponent,
      },
      {
        path: 'dailyreports',
        component: DailyreportsComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
