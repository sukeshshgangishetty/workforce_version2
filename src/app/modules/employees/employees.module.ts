import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesRoutingModule } from './employees-routing.module';
import { AddemployeesComponent } from './addemployees/addemployees.component';
import { ViewemployeesComponent } from './viewemployees/viewemployees.component';
import { LaboursComponent } from './labours/labours.component';
import { AdddepartmentComponent } from './adddepartment/adddepartment.component';
import { ViewdepartmentComponent } from './viewdepartment/viewdepartment.component';
import { AppstatusComponent } from './appstatus/appstatus.component';
import { DailyreportsComponent } from './dailyreports/dailyreports.component';


@NgModule({
  declarations: [AddemployeesComponent, ViewemployeesComponent, LaboursComponent, AdddepartmentComponent, ViewdepartmentComponent, AppstatusComponent, DailyreportsComponent],
  imports: [
    CommonModule,
    EmployeesRoutingModule
  ]
})
export class EmployeesModule { }
