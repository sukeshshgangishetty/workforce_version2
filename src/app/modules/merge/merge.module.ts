import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MergeRoutingModule } from './merge-routing.module';
import { AddclientmergesComponent } from './addclientmerges/addclientmerges.component';
import { ViewclientsmergesComponent } from './viewclientsmerges/viewclientsmerges.component';
import { AddgroupclientComponent } from './addgroupclient/addgroupclient.component';
import { ViewgroupclientsComponent } from './viewgroupclients/viewgroupclients.component';


@NgModule({
  declarations: [AddclientmergesComponent, ViewclientsmergesComponent, AddgroupclientComponent, ViewgroupclientsComponent],
  imports: [
    CommonModule,
    MergeRoutingModule
  ]
})
export class MergeModule { }
