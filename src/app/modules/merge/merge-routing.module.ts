import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddclientmergesComponent } from './addclientmerges/addclientmerges.component';
import { AddgroupclientComponent } from './addgroupclient/addgroupclient.component';
import { ViewclientsmergesComponent } from './viewclientsmerges/viewclientsmerges.component';
import { ViewgroupclientsComponent } from './viewgroupclients/viewgroupclients.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'addclientmerges',
        component: AddclientmergesComponent,
      },
      {
        path: 'viewclientsmerges',
        component: ViewclientsmergesComponent,
      },
      {
        path: 'addgroupclient',
        component: AddgroupclientComponent,
      },
      {
        path: 'viewgroupclients',
        component: ViewgroupclientsComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MergeRoutingModule { }
