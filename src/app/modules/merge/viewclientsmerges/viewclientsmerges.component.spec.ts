import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewclientsmergesComponent } from './viewclientsmerges.component';

describe('ViewclientsmergesComponent', () => {
  let component: ViewclientsmergesComponent;
  let fixture: ComponentFixture<ViewclientsmergesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewclientsmergesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewclientsmergesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
