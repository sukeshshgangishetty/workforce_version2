import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdddiscountComponent } from './adddiscount/adddiscount.component';
import { AddpaymentsComponent } from './addpayments/addpayments.component';
import { InvoicebalanceComponent } from './invoicebalance/invoicebalance.component';
import { PaymentgatewaysapisComponent } from './paymentgatewaysapis/paymentgatewaysapis.component';
import { SalesbalanceComponent } from './salesbalance/salesbalance.component';
import { TypesofdiscountsComponent } from './typesofdiscounts/typesofdiscounts.component';
import { ViewdiscountComponent } from './viewdiscount/viewdiscount.component';
import { ViewsalespaymentComponent } from './viewsalespayment/viewsalespayment.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'addpayments',
        component: AddpaymentsComponent,
      },
      {
        path: 'viewsalespayment',
        component: ViewsalespaymentComponent,
      },
      {
        path: 'salesbalance',
        component: SalesbalanceComponent,
      },
      {
        path: 'invoicebalance',
        component: InvoicebalanceComponent,
      },
      {
        path: 'adddiscount',
        component: AdddiscountComponent,
      },
      {
        path: 'viewdiscount',
        component: ViewdiscountComponent,
      },
      {
        path: 'typesofdiscounts',
        component: TypesofdiscountsComponent,
      },
      {
        path: 'paymentgatewaysapis',
        component: PaymentgatewaysapisComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
