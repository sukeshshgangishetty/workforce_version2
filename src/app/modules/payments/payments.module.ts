import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentsRoutingModule } from './payments-routing.module';
import { AddpaymentsComponent } from './addpayments/addpayments.component';
import { ViewsalespaymentComponent } from './viewsalespayment/viewsalespayment.component';
import { SalesbalanceComponent } from './salesbalance/salesbalance.component';
import { InvoicebalanceComponent } from './invoicebalance/invoicebalance.component';
import { AdddiscountComponent } from './adddiscount/adddiscount.component';
import { ViewdiscountComponent } from './viewdiscount/viewdiscount.component';
import { TypesofdiscountsComponent } from './typesofdiscounts/typesofdiscounts.component';
import { PaymentgatewaysapisComponent } from './paymentgatewaysapis/paymentgatewaysapis.component';


@NgModule({
  declarations: [AddpaymentsComponent, ViewsalespaymentComponent, SalesbalanceComponent, InvoicebalanceComponent, AdddiscountComponent, ViewdiscountComponent, TypesofdiscountsComponent, PaymentgatewaysapisComponent],
  imports: [
    CommonModule,
    PaymentsRoutingModule
  ]
})
export class PaymentsModule { }
