import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.sass"],
})
export class AddComponent implements OnInit {
  tax = {};
  constructor(
    public dialogRef: MatDialogRef<AddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}
  add(f) {
    if (f.valid) {
      console.log("Tax:", this.tax);
      //  this._cat.save(this.cat).subscribe(res=>{
      //    this.msg = "Product Category Updated";
      //    if(this.cat.catid < 0){
      //      this.categories.push(res);
      //      this.msg = "Product Category Added";
      //    }
      //    this.cat ={catid:0};
      //    this.notify(this.msg);
      //  },
      //  error=>{
      //   this.snackBar.open(this.msg,'',{
      //     duration : 5000 });
      //  });
      // TODO Need To Add API Call
    }
  }

  close() {
    this.dialogRef.close();
  }
}
