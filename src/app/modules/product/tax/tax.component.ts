import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { from } from 'rxjs';
import Swal from 'sweetalert2';
import { CategoryService } from '../category.service';
import {AddComponent} from './add/add.component';


@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.sass']
})
export class TaxComponent implements OnInit {
  heading = 'Tax ';
  subheading = 'Welcome to Tax ';
  icon = 'pe-7s-light icon-gradient bg-info';
  btntext = 'Add Tax';
  btnIcon = 'plus';
  btntype='callback';

  cat = {catid:0};

  categories = [];

  msg: string;
  constructor(private _cat : CategoryService,private snackBar:MatSnackBar,public dialog: MatDialog) { }

  ngOnInit() {
    this._cat.get().subscribe(res=>{
      this.categories = res;
      console.log(this.categories);
    });
  }
 
 edit(index){
   this.cat = this.categories[index];
 }
 notify(data : string){
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 1500
  });
}

openDialog(){
  const dialogConfig= new MatDialogConfig();
  dialogConfig.autoFocus = true;
  dialogConfig.width="50%"; 
  dialogConfig.height="80%"; 
  this.dialog.open(AddComponent,dialogConfig);
}

}
