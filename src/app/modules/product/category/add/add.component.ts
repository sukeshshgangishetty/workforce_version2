import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { CategoryService } from '../../category.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  heading = 'Add Category ';
  subheading = 'Welcome to Category ';
  icon = 'pe-7s-light icon-gradient bg-info';
  btntext = 'Back Category';
  btnIcon = 'angle-left';
  btnLink = 'product/category';
  cat = {catid:0};
  

  categories = [];
  msg: string;
  constructor(private _cat : CategoryService,private snackBar:MatSnackBar) { }

  ngOnInit() {
     this._cat.get().subscribe((res) => {
       this.categories = res.body;
     });
  }
  add(f){
    if(f.valid){
      console.log("Cat Data:",this.cat);
       this._cat.save(this.cat).subscribe(res=>{
         this.msg = "Product Category Updated";
         if(this.cat.catid < 0){
           this.categories.push(res);
           this.msg = "Product Category Added";
         }
         this.cat ={catid:0};
         this.notify(this.msg);
       },
       error=>{
        this.snackBar.open(this.msg,'',{ 
          duration : 5000 });
       });  
    }
 
  }

  notify(data : string){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: data,
      showConfirmButton: false,
      timer: 1500
    });
  }
}
