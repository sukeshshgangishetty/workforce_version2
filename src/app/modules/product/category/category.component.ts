import { Component, OnInit } from '@angular/core';
import { CategoryService} from '../category.service';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.sass']
})
export class CategoryComponent implements OnInit {

  heading = 'Category ';
  subheading = 'Welcome to Category ';
  icon = 'pe-7s-light icon-gradient bg-info';
  btntext = 'Add Category';
  btnIcon = 'plus';
  btnLink = 'product/category/0';
  cat = {catid:0};

  categories = [];
 
  msg: string;

  constructor(private _cat : CategoryService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this._cat.get().subscribe(res=>{
      this.categories = res.body;
      console.log(this.categories);
    });
  }
 
 edit(index){
   this.cat = this.categories[index];
 }
 notify(data : string){
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 1500
  });
}
}
