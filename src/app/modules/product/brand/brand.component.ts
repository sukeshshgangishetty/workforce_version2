import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';
import { ProductService } from '../product.service';
@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.sass']
})
export class BrandComponent implements OnInit {

  heading = 'Category ';
  subheading = 'Welcome to Category ';
  icon = 'pe-7s-light icon-gradient bg-info';
  btntext = 'Add Category';
  btnIcon = 'plus';
  btnLink = 'product/brand/0';
  brand = {brandId:0};

  brands = [];
 
  msg: string;

  constructor(private _prod: ProductService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this._prod.getBrands().subscribe(res=>{
      this.brands = res.body;
      console.log(this.brands);
    });
  }
 
 edit(index){
   this.brand = this.brands[index];
 }
 notify(data : string){
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: data,
    showConfirmButton: false,
    timer: 1500
  });
}
}
