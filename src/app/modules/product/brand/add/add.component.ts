import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { ProductService } from '../../product.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  heading = 'Add Product Brand ';
  subheading = 'Welcome to Product Brands ';
  icon = 'pe-7s-light icon-gradient bg-info';
  btntext = 'Back to Brands';
  btnIcon = 'angle-left';
  btnLink = 'product/brand';
  brand = {brandId:0};
  

  msg: string;
  constructor(private _prod : ProductService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    //  this._prod.get().subscribe((res) => {
    //    this.categories = res.body;
    //  });
  }
  add(f){
    if(f.valid){
      console.log("Cat Data:",this.brand);
       this._prod.saveBrands(this.brand).subscribe(res=>{
         this.msg = "Product Brand Updated";
         if(this.brand.brandId < 0){
           this.msg = "Product Brand Added";
         }
         this.brand ={brandId:0};
         this.notify(this.msg);
       },
       error=>{
        this.snackBar.open(this.msg,'',{ 
          duration : 5000 });
       });  
    }
 
  }

  notify(data : string){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: data,
      showConfirmButton: false,
      timer: 1500
    });
  }
}
