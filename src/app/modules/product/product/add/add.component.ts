import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../category.service';
import { ProductService } from '../../product.service';
import {Router, ActivatedRoute,Params} from '@angular/router';
import {FormControl,Validator} from '@angular/forms'
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {





  
  heading = 'Add Product';
  subheading = '';
  icon = 'fa fa-plus';
  btntext = 'Back to Products';
  btnLink = 'product';
  btnIcon = 'angle-left';
  req : any; 
  cats = [];
  product = {pcat:{}};
  msg: any;
  constructor(private _categories: CategoryService,private _product: ProductService,private _route: ActivatedRoute,private snackBar:MatSnackBar) { 
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    this._categories.get().subscribe(res=>{
        this.cats = res;
        console.log(this.cats);
    });
    if(this.req.id>0){
      this.heading = 'Edit Product ID#'+this.req.id;
        this._product.view(this.req.id).subscribe(res=>{
          this.product = res;
        });
    }
  }

  add(f){
    if(f.valid){
      this._product.save(this.product).subscribe(res=>{
          this.msg = "Product Updated";
          if(this.req.id<1){
            this.product = {pcat:{}};
            this.msg = "Product Added";
          }
          this.notify(this.msg);
      },
      error=>{
       this.snackBar.open(this.msg,'',{ 
         duration : 5000 });
      });  
    }
  }
  notify(data : string){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: data,
      showConfirmButton: false,
      timer: 1500
    });
  }

  

}
