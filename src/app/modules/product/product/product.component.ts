import { Component, OnInit } from "@angular/core";
import { ProductService } from "../product.service";
@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.sass"],
})
export class ProductComponent implements OnInit {
  heading = "Products";
  subheading = "";
  icon = "fa fa-archive";
  btntext = "Add Product";
  btnLink = "product/0";
  products = [];

  constructor(private _product: ProductService) {}

  ngOnInit() {
    this._product.get().subscribe((res) => {
      this.products = res;

      console.log(res);
    });
  }
}
