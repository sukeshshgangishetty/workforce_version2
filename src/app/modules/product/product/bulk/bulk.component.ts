import { Component, OnInit } from '@angular/core';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

@Component({
  selector: 'app-bulk',
  templateUrl: './bulk.component.html',
  styleUrls: ['./bulk.component.sass']
})
export class BulkComponent implements OnInit {
  heading = 'Product Upload';
  subheading = 'Welcome to Product Upload Section';
  icon = 'pe-7s-user icon-gradient btn-primary';
  btntext = 'Back to Products';
  btnLink = 'product';
  btnIcon = 'angle-left';
  

  constructor() { }

  ngOnInit() {
  }

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };
  public onUploadInit(args: any): void {
    console.log('onUploadInit:', args);
  }

  public onUploadError(args: any): void {
    console.log('onUploadError:', args);
  }

  public onUploadSuccess(args: any): void {
    console.log('onUploadSuccess:', args);
  }

  bulk = [
    {
     "date":"21 Aug, 2020",
     "totalrecord":"0",
     "status":"Uploaded",
       "result":"0 Imported Successfully",
       "importedBy":"User1",
    },
    {
      "date":"21 Aug, 2020",
      "totalrecord":"20",
      "status":"Uploaded",
        "result":"20 Imported Successfully",
        "importedBy":"User2",
     },
     {
      "date":"21 Aug, 2020",
      "totalrecord":"201",
      "status":"Uploaded",
        "result":"201 Imported Successfully",
        "importedBy":"User4",
     },
     {
      "date":"21 Aug, 2020",
      "totalrecord":"451",
      "status":"Uploaded",
        "result":"451 Imported Successfully",
        "importedBy":"User3",
     },
     {
      "date":"21 Aug, 2020",
      "totalrecord":"89",
      "status":"Uploaded",
        "result":"89 Imported Successfully",
        "importedBy":"User2",
     }
  ]


}
