import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.sass']
})
export class DetailsComponent implements OnInit {

  heading = 'Product Details';
  subheading = 'Welcome to Product Details Section';
  icon = 'pe-7s-user icon-gradient btn-primary';
  btntext = 'Back to Products';
  btnLink = 'product';
  btnIcon = 'angle-left';
  product = [];
  selectedCity: any;

  htmlContent = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ]
  };

  foods=[
    {
      "id":1,
      "name":"demo 1"
    },
    {
      "id":2,
      "name":"demo 2"
    },
    {
      "id":3,
      "name":"demo 3"
    }
  ]
  gst = [
    { id: 1, name: 'GST- 5%' },
    { id: 2, name: 'GST- 8%' },
    { id: 3, name: 'GST- 10%', disabled: true },
    { id: 4, name: 'GST- 12%' },
    { id: 5, name: 'GST- 18%' }
  ];
  exproduct = [
    { id: 1, name: 'Vilnius', avatar: '//www.gravatar.com/avatar/b0d8c6e5ea589e6fc3d3e08afb1873bb?d=retro&r=g&s=30 2x' },
    { id: 2, name: 'Kaunas', avatar: '//www.gravatar.com/avatar/ddac2aa63ce82315b513be9dc93336e5?d=retro&r=g&s=15' },
    { id: 3, name: 'Pavilnys', avatar: '//www.gravatar.com/avatar/6acb7abf486516ab7fb0a6efa372042b?d=retro&r=g&s=15' }
  ];
  public myModel: string;
  public modelWithValue: string;
  public formControlInput: FormControl = new FormControl();
  public mask: Array<string | RegExp>;

  constructor() {
   
  }

  ngOnInit() {
  }

}
