import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product/product.component';
import { StockComponent } from './stock/stock.component';
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import { CategoryComponent } from './category/category.component';
import { PricelistComponent } from './pricelist/pricelist.component';
import { AddComponent as AddPrice } from './pricelist/add/add.component';
import { AddComponent as AddProduct } from './product/add/add.component';
import { AddComponent as AddStock } from './stock/add/add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { OverviewComponent } from './overview/overview.component';
import { DetailsComponent } from './details/details.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule, MatTabsModule, MatDialogModule } from '@angular/material';
import { MatChipsModule, MatAutocompleteModule } from '@angular/material'
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CalendarModule } from 'angular-calendar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { TrendModule } from 'ngx-trend';
import { ProductoverviewComponent } from './productoverview/productoverview.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { BulkComponent } from './product/bulk/bulk.component';
import { DropzoneConfigInterface, DropzoneModule, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DetailsComponent as ProdDetails } from './product/details/details.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { TextMaskModule } from 'angular2-text-mask';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddComponent } from './category/add/add.component';
import { TaxComponent } from './tax/tax.component';
import { AddComponent as AddtaxComponent } from './tax/add/add.component';
import { BrandComponent } from './brand/brand.component';
import { AddComponent as AddBrandComponent } from './brand/add/add.component';
import { ProductsubcategoryComponent } from './productsubcategory/productsubcategory.component';
import { BarcodeComponent } from './barcode/barcode.component';
import { BatchproductsComponent } from './batchproducts/batchproducts.component';
import { ActiveinactiveproductComponent } from './activeinactiveproduct/activeinactiveproduct.component';
import { PricingModule } from '../pricing/pricing.module';
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

// import { PageTitleComponent } from '../../../Layout/Components/page-title/page-title.component';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [AddBrandComponent, AddtaxComponent, ProductComponent, StockComponent, CategoryComponent, PricelistComponent, AddPrice, AddProduct, AddStock, OverviewComponent, DetailsComponent, ProductoverviewComponent, BulkComponent, ProdDetails, AddComponent, TaxComponent, BrandComponent, ProductsubcategoryComponent, BarcodeComponent, BatchproductsComponent, ActiveinactiveproductComponent],
  imports: [
    MatChipsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatRadioModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatNativeDateModule,
    MatIconModule,
    MatDividerModule,
    MatTabsModule,
    MatSlideToggleModule,
    AngularEditorModule,
    MatProgressBarModule,
    MatStepperModule,
    CalendarModule,
    MatSelectModule,
    MatCheckboxModule,
    CommonModule,
    ProductRoutingModule,
    PageTitleModule,
    NgbModule,
    FormsModule,
    NgBootstrapFormValidationModule,
    MatInputModule,
    AngularFontAwesomeModule,
    RoundProgressModule,
    TrendModule,
    PerfectScrollbarModule,
    MatDatepickerModule,
    DropzoneModule,
    MatExpansionModule,
    TextMaskModule,
    NgSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    PricingModule
  ],
  entryComponents: [
    AddtaxComponent
  ],

  exports: [

  ],
  providers: [
    {
      provide:
        PERFECT_SCROLLBAR_CONFIG,
      // DROPZONE_CONFIG,
      useValue:
        DEFAULT_PERFECT_SCROLLBAR_CONFIG,
      // DEFAULT_DROPZONE_CONFIG,
    }, {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG

    }
  ],

})
export class ProductModule { }
