import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productoverview',
  templateUrl: './productoverview.component.html',
  styleUrls: ['./productoverview.component.sass']
})
export class ProductoverviewComponent implements OnInit {
  heading = 'Product Overview';
  subheading = '';
  icon = 'fa fa-dropbox';
  constructor() { }

  ngOnInit() {
  }

}
