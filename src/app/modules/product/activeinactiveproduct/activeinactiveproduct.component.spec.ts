import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveinactiveproductComponent } from './activeinactiveproduct.component';

describe('ActiveinactiveproductComponent', () => {
  let component: ActiveinactiveproductComponent;
  let fixture: ComponentFixture<ActiveinactiveproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveinactiveproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveinactiveproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
