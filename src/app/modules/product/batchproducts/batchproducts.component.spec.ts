import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchproductsComponent } from './batchproducts.component';

describe('BatchproductsComponent', () => {
  let component: BatchproductsComponent;
  let fixture: ComponentFixture<BatchproductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchproductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
