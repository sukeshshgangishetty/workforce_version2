import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { PricelistService } from '../../pricelist.service';
import { ProductService } from '../../product.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  heading = 'Add Price List';
  subheading = '';
  icon = 'fa fa-plus';
  btntext = 'Back to Pricelist';
  btnLink = 'product/pricelist';
  btnIcon = 'angle-left';
  products : any;
  pricelist : any = {product:{}};
  req: any;
  res : any;
  msg: string;

  constructor(private _price: PricelistService,private _product: ProductService,private _route:ActivatedRoute,private snackBar:MatSnackBar) {
    this.req = this._route.snapshot.params;
  }

  ngOnInit() {
    this._product.get().subscribe(res=>{
      this.products = res;
    });
    if(this.req.id > 0){
      this.heading = 'Edit Price List ID#'+this.req.id;
      this._price.view(this.req.id).subscribe(res=>{
        this.res = res;
        this.pricelist = this.res.data;
      });
    }


  }
  add(f){
    if(f.valid){
      this._price.save(this.pricelist).subscribe(res=>{
        this.msg = "Price List Updated";
        if(this.req.id < 1){
          this.pricelist = {product:{}};
          this.msg = "Price List Added";
        }
        this.notify(this.msg);
      },
      error=>{
       this.snackBar.open(this.msg,'',{ 
         duration : 5000 });
      });  
    }

  }
  notify(data : string){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: data,
      showConfirmButton: false,
      timer: 1500
    });
  }
}
