import { Component, OnInit } from '@angular/core';
import { PricelistService } from '../pricelist.service';

@Component({
  selector: 'app-pricelist',
  templateUrl: './pricelist.component.html',
  styleUrls: ['./pricelist.component.sass']
})
export class PricelistComponent implements OnInit {

  heading = 'Price List';
  subheading = '';
  icon = 'fa fa-doller';
  btntext = 'Add Pricelist';
  btnLink = 'product/pricelist/0';
  pricelists : any;
  constructor(private _price : PricelistService) { }

  ngOnInit() {
    this._price.get().subscribe(res=>{
      this.pricelists = res;
      console.log("Pricelist",res);
    });
  }

  add(){
    console.log("add click");
  }
}
