import { Injectable } from '@angular/core';
import { HttpClient }  from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  constructor(private http:HttpClient) { }
  
  get(){
    return this.http.get<any>('/workforce/api/v1/product/brand');
  }
  view(id){
    return this.http.get<any>('/workforce/api/v1/product/'+id);
  }

  save(data){
    return this.http.post<any>('/workforce/api/v1/product',data);
  }

  getBrands(){
    return this.http.get<any>('/workforce/api/v1/product/brand');
  }
  saveBrands(data){
    return this.http.post<any>('/workforce/api/v1/product/brand',data);
  }
  viewBrand(id){
    return this.http.get<any>('/workforce/api/v1/product/brand/'+id);
  }
}
