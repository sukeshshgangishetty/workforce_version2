import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.sass']
})
export class OverviewComponent implements OnInit {
  heading = 'Product Overview';
  subheading = '';
  icon = 'fa fa-dropbox';
  products = [];
  constructor() { }

  ngOnInit() {
  }

}
