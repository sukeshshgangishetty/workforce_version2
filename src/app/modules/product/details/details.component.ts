import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.sass']
})
export class DetailsComponent implements OnInit {
  heading = 'Product Details';
  subheading = '';
  icon = 'fa fa-archive';
  
  stars: number[] = [1, 2, 3, 4, 5];
  constructor() { }
  ngOnInit() {
  }
  countStar(star) {
    console.log('Value of star', star);
  }
}