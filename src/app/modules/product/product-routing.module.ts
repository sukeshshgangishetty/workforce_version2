import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { StockComponent } from './stock/stock.component';
import { CategoryComponent } from './category/category.component';
import { PricelistComponent } from './pricelist/pricelist.component';
import { AddComponent as AddPrice } from './pricelist/add/add.component';
import { AddComponent as AddProduct } from './product/add/add.component';
import { AddComponent as AddStock } from './stock/add/add.component';
import { DetailsComponent } from './details/details.component';
import { OverviewComponent } from './overview/overview.component';
import { ProductoverviewComponent } from './productoverview/productoverview.component';
import { BulkComponent } from './product/bulk/bulk.component';
import { DetailsComponent as ProdDetails } from './product/details/details.component';
import { AddComponent as AddCat } from './category/add/add.component';
import { TaxComponent } from './tax/tax.component';
import { BrandComponent } from './brand/brand.component';
import { AddComponent as AddBrand } from './brand/add/add.component';
import { ProductsubcategoryComponent } from './productsubcategory/productsubcategory.component';
import { BarcodeComponent } from './barcode/barcode.component';
import { BatchproductsComponent } from './batchproducts/batchproducts.component';
import { ActiveinactiveproductComponent } from './activeinactiveproduct/activeinactiveproduct.component';
import { CreateComponent } from '../pricing/create/create.component';
import { StrategyComponent } from '../pricing/strategy/strategy.component';


const routes: Routes = [
  {

    path: '',

    children: [
      {
        path: 'details',
        component: DetailsComponent,
      },
      {
        path: 'overview',
        component: OverviewComponent,
      },
      {
        path: 'productoverview',
        component: ProductoverviewComponent,
      },
      {
        path: 'pricelist',
        component: PricelistComponent,
      },
      {
        path: 'pricelist/:id',
        component: AddPrice,
      },
      {
        path: 'category',
        component: CategoryComponent,
      },
      {
        path: 'subcategory',
        component: ProductsubcategoryComponent,
      },
      {
        path: 'barcode',
        component: BarcodeComponent,
      },
      {
        path: 'batchproducts',
        component: BatchproductsComponent,
      },
      {
        path: 'activeinactiveproduct',
        component: ActiveinactiveproductComponent,
      },
      {
        path: 'createpricingproduct',
        component: CreateComponent,
      },
      {
        path: 'viewpricingproduct',
        component: StrategyComponent,
      },
      {
        path: 'activeinactiveproduct',
        component: ActiveinactiveproductComponent,
      },
      {
        path: 'stock',
        component: StockComponent,
      },

      {
        path: 'stock/:id',
        component: AddStock,
      },
      {
        path: 'bulk',
        component: BulkComponent
      },
      {
        path: 'tax',
        component: TaxComponent,
      },
      {
        path: 'productdetails',
        component: ProdDetails,
      },
      {
        path: 'category/:id',
        component: AddCat,
      },
      {
        path: 'brand',
        component: BrandComponent,
      },
      {
        path: 'brand/:id',
        component: AddBrand,
      },
      {
        path: ':id',
        component: ProdDetails,
      },

      {
        path: '',
        component: OverviewComponent,
      }
    ]

  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
