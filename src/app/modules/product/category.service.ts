import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private _http: HttpClient) { }
  get(){
    return this._http.get<any>('/workforce/api/v1/product/category');
  }
  save(data){
    return this._http.post<any>('/workforce/api/v1/product/category',data);
  }
}
