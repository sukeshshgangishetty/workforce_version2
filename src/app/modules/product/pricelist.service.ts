import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PricelistService {

  constructor(private _http: HttpClient) { }

  save(data){
    return this._http.post('/workforce/pricelist/',data);
  }

  get(){
    return this._http.get('/workforce/pricelist/');
  }
  view(id : number){
    return this._http.get('/workforce/pricelist/'+id);
  }

}
