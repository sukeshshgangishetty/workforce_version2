import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddstockComponent } from './addstock/addstock.component';
import { BatchstockComponent } from './batchstock/batchstock.component';
import { DamagestockComponent } from './damagestock/damagestock.component';
import { ExpirystockComponent } from './expirystock/expirystock.component';
import { GiftsstockComponent } from './giftsstock/giftsstock.component';
import { LowstockComponent } from './lowstock/lowstock.component';
import { OutofstockComponent } from './outofstock/outofstock.component';
import { PackingmaterialsComponent } from './packingmaterials/packingmaterials.component';
import { ProductstockComponent } from './productstock/productstock.component';
import { RawmaterialComponent } from './rawmaterial/rawmaterial.component';
import { ReturnstockComponent } from './returnstock/returnstock.component';
import { ViewstocksComponent } from './viewstocks/viewstocks.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'viewstocks',
        component: ViewstocksComponent,
      },
      {
        path: 'addstock',
        component: AddstockComponent,
      },
      {
        path: 'lowstock',
        component: LowstockComponent,
      },
      {
        path: 'rawmaterial',
        component: RawmaterialComponent,
      },
      {
        path: 'batchstock',
        component: BatchstockComponent,
      },
      {
        path: 'damagestock',
        component: DamagestockComponent,
      },
      {
        path: 'giftstock',
        component: GiftsstockComponent,
      },
      {
        path: 'productstocks',
        component: ProductstockComponent,
      },
      {
        path: 'packingmatrials',
        component: PackingmaterialsComponent,
      },
      {
        path: 'returnstock',
        component: ReturnstockComponent,
      },
      {
        path: 'expirystock',
        component: ExpirystockComponent,
      },
      {
        path: 'outofstock',
        component: OutofstockComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockRoutingModule { }
