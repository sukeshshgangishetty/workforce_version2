import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchstockComponent } from './batchstock.component';

describe('BatchstockComponent', () => {
  let component: BatchstockComponent;
  let fixture: ComponentFixture<BatchstockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchstockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
