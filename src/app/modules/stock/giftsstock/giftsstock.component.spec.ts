import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsstockComponent } from './giftsstock.component';

describe('GiftsstockComponent', () => {
  let component: GiftsstockComponent;
  let fixture: ComponentFixture<GiftsstockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsstockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
