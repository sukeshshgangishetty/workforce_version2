import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockRoutingModule } from './stock-routing.module';
import { ViewstocksComponent } from './viewstocks/viewstocks.component';
import { AddstockComponent } from './addstock/addstock.component';
import { LowstockComponent } from './lowstock/lowstock.component';
import { RawmaterialComponent } from './rawmaterial/rawmaterial.component';
import { BatchstockComponent } from './batchstock/batchstock.component';
import { DamagestockComponent } from './damagestock/damagestock.component';
import { GiftsstockComponent } from './giftsstock/giftsstock.component';
import { ProductstockComponent } from './productstock/productstock.component';
import { PackingmaterialsComponent } from './packingmaterials/packingmaterials.component';
import { ReturnstockComponent } from './returnstock/returnstock.component';
import { ExpirystockComponent } from './expirystock/expirystock.component';
import { OutofstockComponent } from './outofstock/outofstock.component';


@NgModule({
  declarations: [ViewstocksComponent, AddstockComponent, LowstockComponent, RawmaterialComponent, BatchstockComponent, DamagestockComponent, GiftsstockComponent, ProductstockComponent, PackingmaterialsComponent, ReturnstockComponent, ExpirystockComponent, OutofstockComponent],
  imports: [
    CommonModule,
    StockRoutingModule
  ]
})
export class StockModule { }
