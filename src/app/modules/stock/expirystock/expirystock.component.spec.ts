import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpirystockComponent } from './expirystock.component';

describe('ExpirystockComponent', () => {
  let component: ExpirystockComponent;
  let fixture: ComponentFixture<ExpirystockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpirystockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpirystockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
