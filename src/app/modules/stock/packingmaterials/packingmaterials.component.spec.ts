import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingmaterialsComponent } from './packingmaterials.component';

describe('PackingmaterialsComponent', () => {
  let component: PackingmaterialsComponent;
  let fixture: ComponentFixture<PackingmaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackingmaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackingmaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
