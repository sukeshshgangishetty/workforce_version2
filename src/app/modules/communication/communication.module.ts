import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunicationRoutingModule } from './communication-routing.module';
import { OverviewchatComponent } from './overviewchat/overviewchat.component';
import { ExeappchatComponent } from './exeappchat/exeappchat.component';
import { MerchantsappchatsComponent } from './merchantsappchats/merchantsappchats.component';
import { ProductreviewsComponent } from './productreviews/productreviews.component';
import { VendorchatComponent } from './vendorchat/vendorchat.component';
import { GroupchatComponent } from './groupchat/groupchat.component';
import { AttatchfilesComponent } from './attatchfiles/attatchfiles.component';
import { ChatsettingsComponent } from './chatsettings/chatsettings.component';


@NgModule({
  declarations: [OverviewchatComponent, ExeappchatComponent, MerchantsappchatsComponent, ProductreviewsComponent, VendorchatComponent, GroupchatComponent, AttatchfilesComponent, ChatsettingsComponent],
  imports: [
    CommonModule,
    CommunicationRoutingModule
  ]
})
export class CommunicationModule { }
