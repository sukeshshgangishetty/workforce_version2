import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExeappchatComponent } from './exeappchat.component';

describe('ExeappchatComponent', () => {
  let component: ExeappchatComponent;
  let fixture: ComponentFixture<ExeappchatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExeappchatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExeappchatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
