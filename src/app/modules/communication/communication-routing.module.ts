import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AttatchfilesComponent } from './attatchfiles/attatchfiles.component';
import { ChatsettingsComponent } from './chatsettings/chatsettings.component';
import { ExeappchatComponent } from './exeappchat/exeappchat.component';
import { GroupchatComponent } from './groupchat/groupchat.component';
import { MerchantsappchatsComponent } from './merchantsappchats/merchantsappchats.component';
import { OverviewchatComponent } from './overviewchat/overviewchat.component';
import { ProductreviewsComponent } from './productreviews/productreviews.component';
import { VendorchatComponent } from './vendorchat/vendorchat.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'overviewchat',
        component: OverviewchatComponent,
      },
      {
        path: 'exeappchat',
        component: ExeappchatComponent,
      },
      {
        path: 'merchantsappchat',
        component: MerchantsappchatsComponent,
      },
      {
        path: 'productreviews',
        component: ProductreviewsComponent,
      },
      {
        path: 'vendorchat',
        component: VendorchatComponent,
      },
      {
        path: 'groupchat',
        component: GroupchatComponent,
      },
      {
        path: 'attachmentfiles',
        component: AttatchfilesComponent,
      },
      {
        path: 'chatsetting',
        component: ChatsettingsComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationRoutingModule { }
