import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../orders/orders.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-view-dispatch',
  templateUrl: './view-dispatch.component.html',
  styleUrls: ['./view-dispatch.component.sass']
})
export class ViewDispatchComponent implements OnInit {

  heading = 'Generate Dispatch List';
  subheading = '';
  icon = 'fa fa-shopping-cart';
  btnIcon= 'angle-left';
  btntext = 'Dispatch list';
  btnLink = 'dispatch';
  orders: any;
  dispatchList = [];
  selectedIndexes = [];

  constructor(private http: HttpClient, private _order:OrdersService,private _Activatedroute:ActivatedRoute ){ }  
  ngOnInit() {
    this._order.get().subscribe(res=>{  // Order Service
     this.orders = res;
      console.log(res);
    });
   }  

  toggleSelect(event,id,i){
   if (event.checked){

    this.dispatchList.push(id);
    this.selectedIndexes.push(i);
   }
else{
  this.dispatchList.indexOf(id)<0?'':this.dispatchList.splice(this.dispatchList.indexOf(id),1);
  this.selectedIndexes.indexOf(i)<0?'':this.selectedIndexes.splice(this.selectedIndexes.indexOf(i),1);
}   
    console.log(this.dispatchList);
  }

  addToDispatch(){

    this.http.post<any>('/workforce/dispatchlist',this.dispatchList).subscribe(res=>{
      this.removeDispatchedOrders();
      console.log(res);
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Dispatch List is Generated',
        showConfirmButton: false,
        timer: 1500
      });
    },error=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Dispatch List is not Generated',
        showConfirmButton: false,
        timer: 1500
      });
    });
  }
removeDispatchedOrders(){
 this.selectedIndexes.forEach(element => {
  this.orders.splice(element,1);
 });

}


}