import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DispatchComponent} from './dispatch/dispatch.component'
import { DispatchListRoutingModule } from './dispatch-list-routing.module';
import { PageTitleModule } from '../../Layout/Components/page-title/page-title.module';
import {MatExpansionModule, MatTableModule, MatListModule, MatButtonModule,MatCheckboxModule} from '@angular/material'
import {HttpClientModule} from '@angular/common/http';
import {ViewDispatchComponent } from './view-dispatch/view-dispatch.component'
import {BasicComponent} from 'src/app/components/filters/basic/basic.component';
import { OverviewdispatchComponent } from './overviewdispatch/overviewdispatch.component';
import { PendingdispatchComponent } from './pendingdispatch/pendingdispatch.component';
import { CompleteddispatchComponent } from './completeddispatch/completeddispatch.component';
import { CancelleddispatchComponent } from './cancelleddispatch/cancelleddispatch.component'

@NgModule({
  declarations: [DispatchComponent, ViewDispatchComponent,BasicComponent, OverviewdispatchComponent, PendingdispatchComponent, CompleteddispatchComponent, CancelleddispatchComponent],
  imports: [
    CommonModule,
    DispatchListRoutingModule,
    PageTitleModule,
    MatExpansionModule,
    MatTableModule,
    MatListModule,
    MatButtonModule,
    HttpClientModule,
    MatCheckboxModule

  ]
})
export class DispatchListModule {

  


 }
