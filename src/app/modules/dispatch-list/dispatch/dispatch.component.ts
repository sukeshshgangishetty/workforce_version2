import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';
@Component({
  selector: 'app-dispatch',
  templateUrl: './dispatch.component.html',
  styleUrls: ['./dispatch.component.sass'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DispatchComponent implements OnInit {
  isTableExpanded = false;

  panelOpenState = false;
  
  heading = 'Dispatch List';
  subheading = '';
  icon = 'fa fa-archive';
  btntext = 'Generate dispatch list';
  btnLink = 'dispatch/0';
  
 
 
  STUDENTS_DATA = [
    {
      "id": 1,
      "name": "10/08/2020 ",
      "order": 8,
      
      "isExpanded": false,
      "subjects": [
        {
        "pid": 1,
         "name": 'Product 1',
         "qty": 10,
         "hsncode": 4504,
         "price": 180,
        },
        {
          "pid": 2,
         "name": 'Product 2 ',
         "qty": 12,
         "hsncode": 5412,
         "price": 200,
        }
      ]
    },
    {
      "id": 2,
      "name": "20/08/2020",
      "order": 4,
      
      "isExpanded": false,
      "subjects": [
        {
          "pid": 1,
           "name": 'Product 1',
           "qty": 18,
           "hsncode": 4554,
           "price": 120,
          },
          {
            "pid": 2,
           "name": 'Product 2',
           "qty": 15,
           "hsncode": 5059,
           "price": 4000,
          }
        ]
    },
    {
      "id": 3,
      "name": "25/08/2020",
      "order": 6,
     
      "isExpanded": false,
      "subjects": [
        {
          "pid": 1,
           "name": 'Product 1',
           "qty": 5,
           "hsncode": 1657,
           "price": 1800,
          },
          {
            "pid": 2,
           "name": 'Product 2',
           "qty": 8,
           "hsncode": 4404,
           "price": 2000,
          }
        ]
    }
  ];


  dataStudentsList = new MatTableDataSource();
  displayedStudentsColumnsList: string[] = ['id', 'name', 'order',  'actions'];

  constructor(private http:HttpClient) { }

  ngOnInit() {
  this.http.get<any>('/workforce/dispatchlist').subscribe(res=>{
    console.log(res);
  });

  this.dataStudentsList.data = this.STUDENTS_DATA;
  }
  toggleTableRows() {
    this.isTableExpanded = !this.isTableExpanded;

    this.dataStudentsList.data.forEach((row: any) => {
      row.isExpanded = this.isTableExpanded;
    })
  }
}
