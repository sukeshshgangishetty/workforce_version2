import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DispatchListService {

  constructor(private http:HttpClient) { }

  get(){
    return this.http.get<any>('/workforce/dispatchlist');
  }

  generate(){
    return this.http.post<any>('/workforce/dispatchlist',{});

  }

}
