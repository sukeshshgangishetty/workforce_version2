import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CancelleddispatchComponent } from './cancelleddispatch/cancelleddispatch.component';
import { CompleteddispatchComponent } from './completeddispatch/completeddispatch.component';
import { DispatchComponent } from './dispatch/dispatch.component';
import { OverviewdispatchComponent } from './overviewdispatch/overviewdispatch.component';
import { PendingdispatchComponent } from './pendingdispatch/pendingdispatch.component';
import { ViewDispatchComponent } from './view-dispatch/view-dispatch.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ':id',
        component: ViewDispatchComponent
      },
      {
        path: 'overviewdispatch',
        component: OverviewdispatchComponent
      },
      {
        path: 'pendingdispatch',
        component: PendingdispatchComponent
      },
      {
        path: 'completeddispatch',
        component: CompleteddispatchComponent
      },
      {
        path: 'cancelleddispatch',
        component: CancelleddispatchComponent
      },
      {
        path: '',
        component: DispatchComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DispatchListRoutingModule { }
