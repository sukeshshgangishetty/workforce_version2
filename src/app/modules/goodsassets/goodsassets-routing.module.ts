import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintComponent } from '../device/complaint/complaint.component';
import { DeviceComponent } from '../device/device/device.component';
import { RequestComponent } from '../device/request/request.component';
import { AdddeviceComponent } from './adddevice/adddevice.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { DevicestatesComponent } from './devicestates/devicestates.component';
import { DriverprofilesComponent } from './driverprofiles/driverprofiles.component';
import { GeofetchingComponent } from './geofetching/geofetching.component';
import { RequestdevicesComponent } from './requestdevices/requestdevices.component';
import { ViewdevicesComponent } from './viewdevices/viewdevices.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'viewdevices',
        component: DeviceComponent,
      },
      {
        path: 'driverprofiles',
        component: DriverprofilesComponent,
      },
      {
        path: 'requestdevices',
        component: RequestComponent,
      },
      {
        path: 'complaints',
        component: ComplaintComponent,
      },
      {
        path: 'adddevice',
        component: AdddeviceComponent,
      },
      {
        path: 'devicestates',
        component: DevicestatesComponent,
      },
      {
        path: 'geofetching',
        component: GeofetchingComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsassetsRoutingModule { }
