import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoodsassetsRoutingModule } from './goodsassets-routing.module';
import { ViewdevicesComponent } from './viewdevices/viewdevices.component';
import { DriverprofilesComponent } from './driverprofiles/driverprofiles.component';
import { RequestdevicesComponent } from './requestdevices/requestdevices.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { AdddeviceComponent } from './adddevice/adddevice.component';
import { DevicestatesComponent } from './devicestates/devicestates.component';
import { GeofetchingComponent } from './geofetching/geofetching.component';
import { DeviceModule } from '../device/device.module';


@NgModule({
  declarations: [ViewdevicesComponent, DriverprofilesComponent, RequestdevicesComponent, ComplaintsComponent, AdddeviceComponent, DevicestatesComponent, GeofetchingComponent],
  imports: [
    CommonModule,
    GoodsassetsRoutingModule,
    DeviceModule
  ]
})
export class GoodsassetsModule { }
